const text = {
  "header": {
    "linkCompany": "ΕΤΑΙΡΕΙΑ",
    "linkFaq": "ΣΥΧΝΕΣ ΕΡΩΤΗΣΕΙΣ",
    "emailLabel": "Επικοινωνήστε:",
    "phone": "+34-675-59-53-79 / +357-96-643041",
    "emailAddress": "info@esolnetespana.es",
    "registrationInfo": "Πληροφορίες Εγγραφών",
    "language": "Γλώσσα: ",
    "languages": ['Ελληνικά', 'Ισπανικά', 'Αγγλικά'],    
    "createAccount": "Δημιουργία Λογαριασμού / Είσοδος",
    "buttons": {
      "lrn": "LRN EXAMS",
      "material": "ΥΠΟΣΤΗΡΙΚΤΙΚΟ ΥΛΙΚΟ",
      "news": "ΝΕΑ",
      "contact": "ΕΠΙΚΟΙΝΩΝΙΑ"
    },
    "lrnButtons": ['LRN B1 Level', 'LRN B2 Level', 'LRN C1 Level', 'LRN C2 Level'],
    "materialButtons": ['LRN Past Papers', 'Mock Interview Videos']    
  },
  "homeText": {
    "description": "– LRN – is an awarding organisation that offers qualifications to candidates, educational institutes, training providers, schools and employers who can access qualifications through registered educational institutions. <br /> It was founded by a group of educators and business people and specialises in ESOL and management qualifications. LRN London head office is supported by a team of representatives around the world.",
    "title": "Γιατί Εξετάσεις LRN;",
    "more": "Περισσότερα...",
    "carousel": [
      "Learning Resource Network",
      "LRN examination on the 20th of January 2019.",
      "Registrations from the 5th of November until the 9th of December 2018."
    ], 
    "sections": [
      {
          "title": "ΕΞΥΠΝΕΣ",
          "bullets": [
            {
              "main": "Βάση σε όλα τα Levels 50%.",
              "sub1": null
            },
            {
              "main": "Συμψηφίζονται ΟΛΑ τα SKILLS χωρίς προϋποθέσεις.",
              "sub1": null
            },
            {
              "main": "ΔΩΡΕΑΝ ΕΠΑΝΕΞΕΤΑΣΗ.",
              "sub1": null
            }
          ]
      },
      {
        "title": "ΚΑΙΝΟΤΟΜΕΣ",
        "bullets": [
          {
            "main": "Το Listening είναι απλό και ακούγεται 2 φορές.",
            "sub1": "Καινοτομία του LRN: Το Section 3 του Listening ακούγεται και 3η φορά. Καινοτομία του LRN: Το θέμα του Section 3 του Listening είναι όμοιο με το θέμα του Task 1 του Writing Section 1."
          },
          {
            "main": "Καινοτομία του LRN: Το Syllabus του Speaking για τις Εξετάσεις Ιανουαρίου δίνεται από την αρχή της Σχολικής Χρονιάς."
          }
        ]
      },
      {
          "title": "ΦΙΛΙΚΕΣ",
          "bullets": [
            {
              "main": "Προσιτές Εξετάσεις: δίνουν ευκαιρίες, χωρίς παγίδες, χωρίς ιδιομορφίες.",
              "sub1": null
            },
            {
              "main": "Στόχος η μείωση του άγχους του Εξεταζόμενου και η υψηλότερη απόδοσή του.",
              "sub1": null
            },
            {
              "main": "Ευνοϊκές για μαθητές με μαθησιακές δυσκολίες.",
              "sub1": null
            }
          ]
      }
    ],
    recognition: 'Recognition',    
    bodies: [
      {
        title: '<b>ALTE</b> The Association of Language testers in Europe.',
        text1: 'ALTE: is an association of language test providers who work together to promote the fair and accurate assessment of linguistic ability across Europe and beyond.',
        text2: 'It was founded in 1989 by the University of Cambridge (UK) and the Universidad de Salamanca (Spain). Today, ALTE has 33 Full Members representing 25 European languages, as well as 58 Institutional Affiliates and over 500 Individual Affiliates from all around the world. ALTE is a non-profit organisation, and has Participatory Status as an NGO with the Council of Europe.'
      },
      {
        title: '<b>UCAS:</b> The Universities and Colleges Admissions Service.',
        text1: 'A UK-based organisation whose main role is to operate the application process for British universities.',
        text2: ''
      },
      {
        title: '<b>OFQUAL:</b> The Office of Qualifications and Examinations Regulation.',
        text1: 'A non-ministerial government department that regulates qualifications, exams and tests in England and, until May 2016, vocational qualifications in Northern Ireland. Colloquially and publicly, Ofqual is often referred to as the exam "watchdog"',
        text2: ''
      }
    ]
  },
  "contact": {
    "text": "Επικοινωνήστε μαζί μας για περισσότερες πληροφορίες.",
    "addressLabel": "Διεύθυνση",
    "address": "Thesallonikis 46, Flat 1, 6035, Larnaca, Cyprus",
    "scheduleLabel": "Ωράριο τηλεφωνικού κέντρου",
    "schedule": "Δευτέρα – Παρασκευή 9:00 – 20:00",
    "phoneLabel": "Τηλέφωνο",
    "phone": "ES: +34 675 595 379 <br /> CY: +357 96 643041 <br /> UK: +44 7446 069628",
    "emailLabel": "Email",
    "email": "info@esolnetespana.es"
  },
  "footer": {
    "button1": "ΕΤΑΙΡΕΙΑ",
    "button2": "ΕΡΩΤΗΣΕΙΣ",
    "button3": "ΕΓΓΡΑΦΕΣ",
    "button4": "ΠΟΛΙΤΙΚΗ ΑΠΟΡΡΗΤΟΥ",
    "phoneLabel": "Τηλ.",
    "telephone": "ES: +34 675 595 379 / CY: +357 96 643041 / UK: +44 7446 069628",
    "emailLabel": "Email",
    "emailAddress": "info@esolnetespana.es",
    "address": "Thesallonikis 46, Flat 1, 6035, Larnaca, Cyprus",
    "schedule": "Τηλ. Κέντρο: Δευτέρα – Παρασκευή 9:00 – 20:00",
    "copyright": "© 2017 Esolnet Hellas."
  },
  "cookieNotice": {
    "text": "We use third-party cookies to improve our services. You can change the configuration or obtain more information in our cookies policy here: ",
    "link": "cookie policy",
    "close": "Accept"
  },
  "lrnExams": {
    "title1": "ΓΙΑΤΙ LRN;",
    "title2": "ΔΙΟΤΙ ΟΙ ΕΞΕΤΑΣΕΙΣ LRN:",
    "subtitle1": "Σας ανησυχεί η επίδοση των μαθητών σας;",
    "subtitle2": "Στις εξετάσεις LRN, οι μαθητές σας έχουν το μέγιστο ποσοστό επιτυχίας. Δηλώστε τους με εμπιστοσύνη.",
    "bullets": [
      {
        "main": "Είναι αναγνωρισμένο από το Ofqual και το ΑΣΕΠ.",
        "sub1": null
      },
      {
        "main": "Είναι προσιτές, χωρίς παγίδες, χωρίς ιδιομορφίες, ΧΩΡΙΣ ΑΓΧΟΣ.",
        "sub1": null
      },
      {
        "main": "Έχουν βάση σε ΟΛΑ τα Levels 50%.",
        "sub1": null
      },
      {
        "main": "Συμψηφίζονται ΟΛΑ τα Skills χωρίς προϋποθέσεις.",
        "sub1": null
      },
      {
        "main": "Το Listening είναι απλό και ακούγεται 2 φορές.",
        "sub1": "Καινοτομία: Το Listening section 3 σε ΟΛΑ τα Levels ακούγεται και 3η φορά.",
        "sub2": "Καινοτομία: To Writing Section 1, Task 1 έχει το ίδιο θέμα με το Listening Section 3."
      },
      {
        "main": "Τα Tasks στο Writing συνοδεύονται από ιδέες.",
        "sub1": null
      },
      {
        "main": "Τα 2 Tasks του Writing, που γράφουν οι Υποψήφιοι ευνοούν την καλύτερη απόδοση και βαθμολογία.",
        "sub1": null
      },
      {
        "main": "Οι Υποψήφιοι προβάλλουν τις καλύτερες επιδόσεις τους, που συνήθως έχουν στο Listening, Writing, Speaking.",
        "sub1": null
      },
      {
        "main": "Τα Reading and Use συμψηφίζονται σε κοινό βαθμό.",
        "sub1": null
      },
      {
        "main": "Ευνοϊκές και για το Level C2.",
        "sub1": null
      },
      {
        "main": "Είναι απλές και οδηγούν τους μαθητές ευκολότερα στην επιτυχία.",
        "sub1": null
      },
      {
        "main": "Ο χρόνος για τις Εξετάσεις είναι ενιαίος χωρίς διαλείμματα και περισσότερο από επαρκής.",
        "sub1": null
      },
      {
        "main": "Οι Υποψήφιοι ΟΛΩΝ των Επιπέδων εξετάζονται σε όλες τις δεξιότητες του γραπτού και προφορικού λόγου.",
        "sub1": null
      },
      {
        "main": "Ευνοϊκές και για Υποψηφίους με μαθησιακές δυσκολίες.",
        "sub1": null
      },
      {
        "main": "Καινοτομία: Θεσπίσαμε τη ΔΩΡΕΑΝ ΕΠΑΝΕΞΕΤΑΣΗ.",
        "sub1": null
      },
      {
        "main": "Οι Εξετάσεις διεξάγονται Ιανουάριο και Ιούνιο.",
        "sub1": null
      },
      {
        "main": "ΔΩΡΕΑΝ Πλούσιο Υποστηρικτικό Υλικό.",
        "sub1": null
      },
      {
        "main": "Καινοτομία: Θεσπίσαμε τον Προσωπικό Σύμβουλο Εξετάσεων, για στήριξη και συνεχή Επικοινωνία.",
        "sub1": null
      },
      {
        "main": "Τα Κέντρα Ξένων Γλωσσών δεν χρειάζεται να γράφουν τις διευθύνσεις των μαθητών τους στις δηλώσεις συμμετοχής και να δίνουν προσωπικά και επαγγελματικά δεδομένα.",
        "sub1": null
      }
    ],
    "examsButtons": ['Listening','Writing','Reading and Use','Speaking'],
    "exams": [
      {
        "name": "LRN B1",
        "price": "70€",
        "text": "Το <b> LRN EXAM </b> είναι <b> ενιαίο </b>. Δίνεται όλο µαζί και οι υποψήφιοι έχουν τη δυνατότητα να διαχειριστούν το χρόνο τους, να επιστρέψουν σε ένα μέρος του Test, για να διορθώσουν ή να συµπληρώσουν ό,τι χρειαστεί.",
        "duration": {
          "title": "Διάρκεια εξέτασης: ",
          "text": "2 ώρες και 30 λεπτά."
        },
        "tableTitle": "Το <b> LRN EXAM </b> αποτελείται από: ",
        "table": {
          "rows": [
            [" ","Skills","Parts"],
            ["1.","Listening","3 Sections"],
            ["2.","Writing","2 Sections"],
            ["3.","Reading and Use","4 Sections"],
            ["4.","Speaking","4 Sections"]                            
          ]
        },
        "listening": {
          "title": "To <b> Listening </b> του <b> LRN EXAM </b> αποτελείται από :",
          "section1": {
            "title": "Listening Section 1: (9 marks)",
            "bullets": ["Ακούγεται 2 φορές","9 µικροί διάλογοι, 9 ερωτήσεις","1 βαθµό η κάθε ερώτηση"]
          },
          "section2": {
            "title": "Listening Section 2: (6 marks)",
            "bullets": ["Ακούγεται 2 φορές","3 λίγο μεγαλύτεροι διάλογοι","Δύο ερωτήσεις ο κάθε διάλογος","1 βαθµό η κάθε ερώτηση"]
          },
          "section3": {
            "title": "Listening Section 3: (5 marks)",
            "bullets": ["Ακούγεται 2 φορές","1 κείμενο με θέμα Friendly Letter","5 ερωτήσεις","1 βαθμό η κάθε ερώτηση"]
          },
          "hint": {
            "title": "<b>Καινοτομία</b>",
            "text": "σε όλα τα Levels το Listening Section 3 ακούγεται και 3η φορά σε συνδυασμό με το Writing, Section 1, Task 1."
          }
        },
        "writing": {
          "title": "Το <b>WRITING</b> του <b>LRN EXAM</b> αποτελείται σε ΟΛΑ τα Επίπεδα από 2 Sections.",
          "text1": "Στο <b>Writing Section 1, Task 1</b> οι υποψήφιοι γράφουν <b>υποχρεωτικά</b> το θέµα που άκουσαν στο  Listening Section 3.<br /> Το θέμα του <b>Listening Section 3</b> και το <b>Writing Section 1, Task 1</b> είναι ένα Friendly Letter.",
          "text2": "Στο <b>Writing Section 2</b> υπάρχουν <b>3 επιλογές.</b>",
          "bullets": ["Άλλο ένα Friendly Letter (συνοδεύεται από ιδέες)","Story","Essay (συνοδεύεται από ιδέες)"],
          "text3": "Τα 2 Writing του LRN EXAM προσφέρουν ευκαιρία στους υποψήφιους για καλύτερη βαθµολογία.",
          "table": {
            "title": "Τα 2 Writing του LRN EXAM προσφέρουν ευκαιρία στους υποψήφιους για καλύτερη βαθµολογία.",
            "subtitle": "Number of words of Writing EXAM",              
            "rows": [
              ["Levels","1o Task","2o Task"],
              ["B1","90-110","110-150"]
            ]
          }
        },
        "reading": {
          "title": "Το READING AND USE του LRN EXAM είναι µαζί και µοιράζονται τη βαθµολογία.",
          "table1": {
            "title": "Το Reading αποτελείται από:",
            "rows": [
              ["Section 1:","1 κείµενο, 8 ερωτήσεις ( ½ βαθµό η κάθε µια)"],
              ["Section 2:","2 µικρά κείµενα με κοινό θέμα"],
              [" ", "1ο  κείµενο, 3 – 4 ερωτήσεις. <br /> 2ο  κείµενο, 3 – 4 ερωτήσεις."],
              [" ", "Σύνολο ερωτήσεων 15, η κάθε ερώτηση παίρνει ½ βαθµό."]
            ]
          },
          "table2": {
            "title": "Tο USE  αποτελείται από:",
            "rows": [
              ["Section 3:","15 multiple choice ερωτήσεις γραµµατικής, 1/2 βαθµό η κάθε ερώτηση."],
              ["Section 4:","1 κείµενο fill in µε 10 multiple choice ερωτήσεις , ½ βαθµό η κάθε ερώτηση."],
              [" ", " "],
              [" ","Σύνολο  βαθµολογίας Reading and Use: 15 + 25 = 40 : 2 = 20 marks"]
            ]
          },
          "footer": "O συµψηφισµός του Reading and Use ευνοεί την καλύτερη βαθµολογική απόδοση των υποψηφίων."
        },
        "speaking": {
          "title": "Στο SPEAKING του LRN EXAM συµµετέχουν : 2 Candidates, 1 Examiner.",
          "duration": {
            "title": "Διάρκεια εξέτασης: ",
            "text": "14 – 16 λεπτά."
          },
          "bulletTitle": "Ανάλυση των 3 Sections: ",
          "bullets": [
            {
              "heading": "Section 1: ",
              "text": "Warm up questions"
            },
            {
              "heading": "Section 2: ",
              "text": "Candidates talk on 1 of the 5 topics they have chosen and prepared."
            },
            {
              "heading": "Section 3: ",
              "text": "Candidates respond to a question related to Section 2; points are given for them to use."
            }
          ],
          "text1": "To Speaking Section 2 του LRN EXAM αποστέλλεται για προετοιµασία 2 εβδοµάδες πριν από τις προφορικές εξετάσεις. Προϋποθέτει την προετοιµασία 1 από τα 5  θέµατα που αποστέλλονται.",
          "text2": "Οι 2 υποψήφιοι που εξετάζονται στο Speaking είναι δυνατόν να έχουν επιλέξει το ίδιο θέµα.",
          "text3": "Καινοτομία: Το Syllabus του Speaking για τις Εξετάσεις Ιανουαρίου δίνεται από την αρχή της Σχολικής Χρονιάς."
        }
      },
      {
        "name": "LRN B2",
        "price": "70€",
        "text": "Το LRN EXAM είναι ενιαίο. Δίνεται όλο µαζί και οι υποψήφιοι έχουν τη δυνατότητα να διαχειριστούν το χρόνο τους, να επιστρέψουν σε ένα μέρος του Test, για να διορθώσουν ή να συµπληρώσουν ό,τι χρειαστεί.",
        "duration": {
          "title": "Διάρκεια εξέτασης: ",
          "text": "2 ώρες και 30 λεπτά."
        },
        "tableTitle": "Το LRN EXAM αποτελείται από: ",
        "table": {
          "rows": [
            [" ","Skills","Parts"],
            ["1.","Listening","3 Sections"],
            ["2.","Writing","2 Sections"],
            ["3.","Reading and Use","4 Sections"],
            ["4.","Speaking","3 Sections"]                            
          ]
        },
        "listening": {
          "title": "To Listening του LRN EXAM αποτελείται από :",
          "section1": {
            "title": "Listening Section 1: (9 marks)",
            "bullets": ["Ακούγεται 2 φορές","9 µικροί διάλογοι, 9 ερωτήσεις","1 βαθµό η κάθε ερώτηση"]
          },
          "section2": {
            "title": "Listening Section 2: (6 marks)",
            "bullets": ["Ακούγεται 2 φορές","3 λίγο μεγαλύτεροι διάλογοι","Δύο ερωτήσεις ο κάθε διάλογος","1 βαθµό η κάθε ερώτηση"]
          },
          "section3": {
            "title": "Listening Section 3: (5 marks)",
            "bullets": ["Ακούγεται 2 φορές","1 κείμενο με θέμα Friendly Letter","5 ερωτήσεις","1 βαθμό η κάθε ερώτηση"]
          },
          "hint": {
            "title": "Καινοτομία",
            "text": "σε όλα τα Levels το Listening Section 3 ακούγεται και 3η φορά σε συνδυασμό με το Writing, Section 1, Task 1."
          }
        },
        "writing": {
          "title": "Το WRITING του LRN EXAM αποτελείται σε ΟΛΑ τα Επίπεδα από 2 Sections.",
          "text1": "Στο Writing Section 1, Task 1 οι υποψήφιοι γράφουν υποχρεωτικά το θέµα που άκουσαν στο  Listening Section 3. Το θέμα του Listening Section 3 και το Writing Section 1, Task 1 είναι ένα Friendly Letter.",
          "text2": "Στο Writing Section 2 υπάρχουν 3 επιλογές.",
          "bullets": ["Άλλο ένα Friendly Letter (συνοδεύεται από ιδέες)","Story","Essay (συνοδεύεται από ιδέες)"],
          "text3": "Τα 2 Writing του LRN EXAM προσφέρουν ευκαιρία στους υποψήφιους για καλύτερη βαθµολογία.",
          "table": {
            "title": "Number of words of Writing EXAM",
            "rows": [
              ["Levels","1o Task","2o Task"],
              ["B2","100-120","120-170"]
            ]
          }
        },
        "reading": {
          "title": "Το READING AND USE του LRN EXAM είναι µαζί και µοιράζονται τη βαθµολογία.",
          "table1": {
            "title": "Το Reading αποτελείται από:",
            "rows": [
              ["Section 1:","1 κείµενο, 8 ερωτήσεις ( ½ βαθµό η κάθε µια)"],
              ["Section 2:","2 µικρά κείµενα με κοινό θέμα"],
              [" ", "1ο  κείµενο, 3 – 4 ερωτήσεις. <br /> 2ο  κείµενο, 3 – 4 ερωτήσεις."],
              [" ", "Σύνολο ερωτήσεων 15, η κάθε ερώτηση παίρνει ½ βαθµό."]
            ]
          },
          "table2": {
            "title": "Tο USE  αποτελείται από:",
            "rows": [
              ["Section 3:","15 multiple choice ερωτήσεις γραµµατικής, 1/2 βαθµό η κάθε ερώτηση."],
              ["Section 4:","1 κείµενο fill in µε 10 multiple choice ερωτήσεις , ½ βαθµό η κάθε ερώτηση."],
              [" ", " "],
              [" ","Σύνολο  βαθµολογίας Reading and Use: 15 + 25 = 40 : 2 = 20 marks"]
            ]
          },
          "footer": "O συµψηφισµός του Reading and Use ευνοεί την καλύτερη βαθµολογική απόδοση των υποψηφίων."
        },
        "speaking": {
          "title": "Στο SPEAKING του LRN EXAM συµµετέχουν : 2 Candidates, 1 Examiner.",
          "duration": {
            "title": "Διάρκεια εξέτασης: ",
            "text": "14 – 16 λεπτά."
          },
          "bulletTitle": "Ανάλυση των 3 Sections: ",
          "bullets": [
            {
              "heading": "Section 1: ",
              "text": "Warm up questions"
            },
            {
              "heading": "Section 2: ",
              "text": "Candidates talk on 1 of the 5 topics they have chosen and prepared."
            },
            {
              "heading": "Section 3: ",
              "text": "Candidates respond to a question related to Section 2; points are given for them to use."
            }
          ],
          "text1": "To Speaking Section 2 του LRN EXAM αποστέλλεται για προετοιµασία 2 εβδοµάδες πριν από τις προφορικές εξετάσεις. Προϋποθέτει την προετοιµασία 1 από τα 5  θέµατα που αποστέλλονται.",
          "text2": "Οι 2 υποψήφιοι που εξετάζονται στο Speaking είναι δυνατόν να έχουν επιλέξει το ίδιο θέµα.",
          "text3": "Καινοτομία: Το Syllabus του Speaking για τις Εξετάσεις Ιανουαρίου δίνεται από την αρχή της Σχολικής Χρονιάς."
        }
      },
      {
        "name": "LRN C1",
        "price": "80€",
        "text": "Το LRN EXAM είναι ενιαίο. Δίνεται όλο µαζί και οι υποψήφιοι έχουν τη δυνατότητα να διαχειριστούν το χρόνο τους, να επιστρέψουν σε ένα μέρος του Test, για να διορθώσουν ή να συµπληρώσουν ό,τι χρειαστεί.",
        "duration": {
          "title": "Διάρκεια εξέτασης: ",
          "text": "3 ώρες."
        },
        "tableTitle": "Το LRN EXAM αποτελείται από: ",
        "table": {
          "rows": [
            [" ","Skills","Parts"],
            ["1.","Listening","3 Sections"],
            ["2.","Writing","2 Sections"],
            ["3.","Reading and Use","4 Sections"],
            ["4.","Speaking","3 Sections"]                            
          ]
        },
        "listening": {
          "title": "To Listening του LRN EXAM αποτελείται από :",
          "section1": {
            "title": "Listening Section 1: (10 marks)",
            "bullets": ["Ακούγεται 2 φορές","10 µικροί διάλογοι, 10 ερωτήσεις","1 βαθµό η κάθε ερώτηση"]
          },
          "section2": {
            "title": "Listening Section 2: (10 marks)",
            "bullets": ["Ακούγεται 2 φορές","3 λίγο μεγαλύτεροι διάλογοι","10 ερωτήσεις","Ο κάθε διάλογος 2 – 4 ερωτήσεις","1 βαθµό η κάθε ερώτηση"]
          },
          "section3": {
            "title": "Listening Section 3: (5 marks)",
            "bullets": ["Ακούγεται 2 φορές","1 κείμενο με θέμα Essay","5 ερωτήσεις","1 βαθμό η κάθε ερώτηση"]
          },
          "hint": {
            "title": "Καινοτομία",
            "text": "σε όλα τα Levels το Listening Section 3 ακούγεται και 3η φορά σε συνδυασμό με το Writing, Section 1, Task 1."
          }
        },
        "writing": {
          "title": "Το WRITING του LRN EXAM αποτελείται σε ΟΛΑ τα Επίπεδα από 2 Sections.",
          "text1": "Στο Writing Section 1, Task 1 οι υποψήφιοι γράφουν υποχρεωτικά το θέµα που άκουσαν στο  Listening Section 3. Το θέμα του Listening Section 3 και το Writing Section 1, Task 1 είναι ένα Friendly Letter.",
          "text2": "Στο Writing Section 2 υπάρχουν 3 επιλογές από 3 Essay που συνοδεύονται από ιδέες.",
          "bullets": [],
          "text3": "Τα 2 Writing του LRN EXAM προσφέρουν ευκαιρία στους υποψήφιους για καλύτερη βαθµολογία.",
          "table": {
            "title": "Number of words of Writing EXAM",
            "rows": [
              ["Levels","1o Task","2o Task"],
              ["C1","150-200","250-300"]
            ]
          }
        },
        "reading": {
          "title": "Το READING AND USE του LRN EXAM είναι µαζί και µοιράζονται τη βαθµολογία.",
          "table1": {
            "title": "Το Reading αποτελείται από:",
            "rows": [
              ["Section 1:","1 κείµενο, 9 ερωτήσεις ( ½ βαθµό η κάθε µια)"],
              ["Section 2:","2 µικρά κείµενα"],
              [" ", "1ο  κείµενο, 3 – 4 ερωτήσεις. <br /> 2ο  κείµενο, 3 – 4 ερωτήσεις. <br /> τέλος 2-3 ερωτήσεις και για τα 2 κείµενα."],
              [" ", "Σύνολο ερωτήσεων 20, η κάθε ερώτηση παίρνει ½ βαθµό."]
            ]
          },
          "table2": {
            "title": "Tο USE  αποτελείται από:",
            "rows": [
              ["Section 3:","20 multiple choice ερωτήσεις γραµµατικής, 1/2 βαθµό η κάθε ερώτηση."],
              ["Section 4:","1 κείµενο fill in µε 10 gaps, ½ βαθµό η κάθε ερώτηση."],
              [" ", " "],
              [" ","Σύνολο  βαθµολογίας Reading and Use: 20 + 30 = 50 : 2 = 25 marks"]
            ]
          },
          "footer": "O συµψηφισµός του Reading and Use ευνοεί την καλύτερη βαθµολογική απόδοση των υποψηφίων."
        },
        "speaking": {
          "title": "Στο SPEAKING του LRN EXAM συµµετέχουν : 2 Candidates, 1 Examiner.",
          "duration": {
            "title": "Διάρκεια εξέτασης: ",
            "text": "16 – 18 λεπτά."
          },
          "bulletTitle": "Ανάλυση των 3 Sections: ",
          "bullets": [
            {
              "heading": "Section 1: ",
              "text": "Warm up questions"
            },
            {
              "heading": "Section 2: ",
              "text": "Candidates talk on 1 of the 5 topics they have chosen and prepared."
            },
            {
              "heading": "Section 3: ",
              "text": "Candidates respond to a question related to Section 2; points are given for them to use."
            }
          ],
          "text1": "To Speaking Section 2 του LRN EXAM αποστέλλεται για προετοιµασία 2 εβδοµάδες πριν από τις προφορικές εξετάσεις. Προϋποθέτει την προετοιµασία 1 από τα 5  θέµατα που αποστέλλονται.",
          "text2": "Οι 2 υποψήφιοι που εξετάζονται στο Speaking είναι δυνατόν να έχουν επιλέξει το ίδιο θέµα.",
          "text3": "Καινοτομία: Το Syllabus του Speaking για τις Εξετάσεις Ιανουαρίου δίνεται από την αρχή της Σχολικής Χρονιάς."
        }
      },
      {
        "name": "LRN C2",
        "price": "100€",
        "text": "Το LRN EXAM είναι ενιαίο. Δίνεται όλο µαζί και οι υποψήφιοι έχουν τη δυνατότητα να διαχειριστούν το χρόνο τους, να επιστρέψουν σε ένα μέρος του Test, για να διορθώσουν ή να συµπληρώσουν ό,τι χρειαστεί.",
        "duration": {
          "title": "Διάρκεια εξέτασης: ",
          "text": "3 ώρες."
        },
        "tableTitle": "Το LRN EXAM αποτελείται από: ",
        "table": {
          "rows": [
            [" ","Skills","Parts"],
            ["1.","Listening","3 Sections"],
            ["2.","Writing","2 Sections"],
            ["3.","Reading and Use","4 Sections"],
            ["4.","Speaking","3 Sections"]                            
          ]
        },
        "listening": {
          "title": "To Listening του LRN EXAM αποτελείται από :",
          "section1": {
            "title": "Listening Section 1: (10 marks)",
            "bullets": ["Ακούγεται 2 φορές","10 µικροί διάλογοι, 10 ερωτήσεις","1 βαθµό η κάθε ερώτηση"]
          },
          "section2": {
            "title": "Listening Section 2: (10 marks)",
            "bullets": ["Ακούγεται 2 φορές","3 λίγο μεγαλύτεροι διάλογοι","10 ερωτήσεις","Ο κάθε διάλογος 2 – 4 ερωτήσεις","1 βαθµό η κάθε ερώτηση"]
          },
          "section3": {
            "title": "Listening Section 3: (5 marks)",
            "bullets": ["Ακούγεται 2 φορές","1 κείμενο με θέμα Essay","5 ερωτήσεις","1 βαθμό η κάθε ερώτηση"]
          },
          "hint": {
            "title": "Καινοτομία",
            "text": "σε όλα τα Levels το Listening Section 3 ακούγεται και 3η φορά σε συνδυασμό με το Writing, Section 1, Task 1."
          }
        },
        "writing": {
          "title": "Το WRITING του LRN EXAM αποτελείται σε ΟΛΑ τα Επίπεδα από 2 Sections.",
          "text1": "Στο Writing Section 1, Task 1 οι υποψήφιοι γράφουν υποχρεωτικά το θέµα που άκουσαν στο  Listening Section 3. Το θέμα του Listening Section 3 και το Writing Section 1, Task 1 είναι ένα Friendly Letter.",
          "text2": "Στο Writing Section 2 υπάρχουν 3 επιλογές από 3 Essay που συνοδεύονται από ιδέες.",
          "bullets": [],
          "text3": "Τα 2 Writing του LRN EXAM προσφέρουν ευκαιρία στους υποψήφιους για καλύτερη βαθµολογία.",
          "table": {
            "title": "Number of words of Writing EXAM",
            "rows": [
              ["Levels","1o Task","2o Task"],
              ["C2","200-250","250-300"]
            ]
          }
        },
        "reading": {
          "title": "Το READING AND USE του LRN EXAM είναι µαζί και µοιράζονται τη βαθµολογία.",
          "table1": {
            "title": "Το Reading αποτελείται από:",
            "rows": [
              ["Section 1:","1 κείµενο, 9 ερωτήσεις ( ½ βαθµό η κάθε µια)"],
              ["Section 2:","2 µικρά κείµενα"],
              [" ", "1ο  κείµενο, 3 – 4 ερωτήσεις. <br /> 2ο  κείµενο, 3 – 4 ερωτήσεις. <br /> τέλος 2-3 ερωτήσεις και για τα 2 κείµενα."],
              [" ", "Σύνολο ερωτήσεων 20, η κάθε ερώτηση παίρνει ½ βαθµό."]
            ]
          },
          "table2": {
            "title": "Tο USE  αποτελείται από:",
            "rows": [
              ["Section 3:","20 multiple choice ερωτήσεις γραµµατικής, 1/2 βαθµό η κάθε ερώτηση."],
              ["Section 4:","1 κείµενο fill in µε 10 gaps, ½ βαθµό η κάθε ερώτηση."],
              [" ", " "],
              [" ","Σύνολο  βαθµολογίας Reading and Use: 20 + 30 = 50 : 2 = 25 marks"]
            ]
          },
          "footer": "O συµψηφισµός του Reading and Use ευνοεί την καλύτερη βαθµολογική απόδοση των υποψηφίων."
        },
        "speaking": {
          "title": "Στο SPEAKING του LRN EXAM συµµετέχουν : 2 Candidates, 1 Examiner.",
          "duration": {
            "title": "Διάρκεια εξέτασης: ",
            "text": "16 – 18 λεπτά."
          },
          "bulletTitle": "Ανάλυση των 3 Sections: ",
          "bullets": [
            {
              "heading": "Section 1: ",
              "text": "Warm up questions"
            },
            {
              "heading": "Section 2: ",
              "text": "Candidates talk on 1 of the 5 topics they have chosen and prepared."
            },
            {
              "heading": "Section 3: ",
              "text": "Candidates respond to a question related to Section 2; points are given for them to use."
            }
          ],
          "text1": "To Speaking Section 2 του LRN EXAM αποστέλλεται για προετοιµασία 2 εβδοµάδες πριν από τις προφορικές εξετάσεις. Προϋποθέτει την προετοιµασία 1 από τα 5  θέµατα που αποστέλλονται.",
          "text2": "Οι 2 υποψήφιοι που εξετάζονται στο Speaking είναι δυνατόν να έχουν επιλέξει το ίδιο θέµα.",
          "text3": "Καινοτομία: Το Syllabus του Speaking για τις Εξετάσεις Ιανουαρίου δίνεται από την αρχή της Σχολικής Χρονιάς."
        }
      }
    ]
  },
  "faqs": {
    "exams": [
      {
        'q': 'Πότε βγαίνουν τα αποτελέσματα;',
        'a': 'Τα αποτελέσματα της εξεταστικής βγαίνουν σε περίπου 10 εβδομάδες από την ημέρα διεξαγωγής των Εξετάσεων.'
      },
      {
        'q': 'Ποια είναι τα Εξεταστικά Κέντρα;',
        'a': 'Επικοινωνήστε μαζί μας για να σας ενημερώσουμε για το κοντινότερο Εξεταστικό Κέντρο της περιοχής σας.'
      },
      {
        'q': 'Πότε αποστέλλονται τα Δελτία των Υποψηφίων;',
        'a': 'Δελτία γίνονται διαθέσιμα 2 εβδομάδες πριν τις Εξετάσεις.'
      },
      {
        'q': 'Ποια είναι η ημερομηνία των Εξετάσεων;',
        'a': '20 Ιανουαρίου 2019.'
      },
      {
        'q': 'Πότε δίνονται τα θέματα προετοιμασίας των προφορικών;',
        'a': 'Το Section 2 Speaking του LRN γίνεται διαθέσιμο στους υποψηφίους περίπου 2 εβδομάδες πριν τις Εξετάσεις. Καινοτομία του LRN: To Syllabus του Speaking δίνεται από την αρχή της Σχολικής Χρονιάς.'
      },
      {
        'q': 'Τι γίνεται σε περίπτωση απουσίας;',
        'a': 'Σε περίπτωση που κάποιος Υποψήφιος απουσιάσει λόγω ασθένειας ή σοβαρού λόγου, δεν γίνονται επιστροφές στα εξέταστρα αλλά πιστώνεται το ποσό για την επόμενη Εξεταστική. Πρέπει να επικοινωνήσετε μαζί μας για νέα αίτηση.'
      },
      {
        'q': 'Τι ισχύει για τους υποψήφιους με μαθησιακές δυσκολίες;',
        'a': 'Οι Υποψήφιοι με μαθησιακές δυσκολίες χρειάζεται να υποβάλλουν την όποια γνωμάτευση έχουν στη διάθεση τους με την εγγραφή τους. Από φέτος η μετάφραση της γνωμάτευσης γίνεται από την Esolnet Hellas. Καινοτομία: Σας βοηθάμε να αξιολογήσετε τους υποψηφίους σας με μαθησιακές δυσκολίες στο Writing. Στείλτε μας δείγματα του Writing και θα σας επιστραφούν με σχόλια για την επίδοσή τους. Τα δείγματα αυτά πρέπει να είναι γραμμένα με στυλό, σε κόλλα διαγωνισμού και να έχουν παραγράφους, που να διακρίνονται ξεκάθαρα.'
      }
    ],
    "registration": [
      {
        'q': 'Ποιες είναι οι ημερομηνίες για τις προθεσμίες υποβολής αιτήσεων;',
        'a': 'Παρακαλούμε ανατρέξτε στην σελίδα ενημέρωσης της τρέχουσας Εξεταστικής Περιόδου.'
      },
      {
        'q': 'Ποια είναι τα εξέταστρα για τις Εξετάσεις LRN;',
        'a': 'LEVEL A1: 70€<br /> LEVEL A2: 70€<br /> LEVEL B1: 70€<br /> LEVEL B2: 70€<br /> LEVEL C1: 80€<br /> LEVEL C2: 100€'
      },
      {
        'q': 'ΤΡΑΠΕΖΙΚΟΙ ΛΟΓΑΡΙΑΣΜΟΙ',
        'a': ''
      }
    ],
    "material": [
      {
        'q': 'Ποια είναι η διαθέσιμη βιβλιογραφία;',
        'a': 'Οι εκδόσεις SUPERCOUSE έχουν βιβλία για τα επίπεδα B2 και C2.<br /> Οι εκδόσεις BETSIS έχουν βιβλία για τα επίπεδα B1, B2, C1 και C2.'
      },
      {
        'q': 'Που θα βρω διαθέσιμο Υποστηρικτικό Υλικό;',
        'a': 'Επιπλέον διαθέσιμο υλικό μπορείτε να βρείτε στην ενότητα της σελιδας μας Υποστηρικτικό Υλικό.<br /> Για την προετοιμασία των Υποψηφίων αποστέλλεται ηλεκτρονικά και επιπλέον υλικό για LRN.'
      }
    ],
    'retake': [
      {
        'q': 'Σε περίπτωση επανεξέτασης πρέπει να κάνω νέα αίτηση;',
        'a': 'Πρέπει να γίνει νέα αίτηση με όλα τα στοιχεία του Υποψηφίου και υποσημείωση ότι είναι επανεξέταση.'
      },
    ],
    'recognizition': [
      {
        'q': 'Ποια είναι η αναγνώριση των Εξετάσεων στο εξωτερικό;',
        'a': 'Οι Εξετάσεις LRN είναι αναγνωρισμένες από Ofqual, ALTE, UCAS.'
      }
    ]
  },
  "faqsTitles": ['1. Εξετάσεις', '2. Εγγραφές','3. Υλικό','4. Δωρεάν Επανεξέταση','5. Αναγνώριση'],
  "company": {
    "headerTitle": 'ESOLNET',
    "headerSubtitle": 'Πιστοποίηση Αγγλικής Γλώσσας & Επαγγελματικών Δεξιοτήτων',
    "title1": 'Οι Επαγγελματίες <br /> Στον χώρο της Πιστοποίησης',
    "text1": [
      'H ESOLNET ιδρύθηκε στην Κύπρο από επαγγελματίες στο χώρο της Πιστοποίησης της Αγγλικής Γλώσσας και της Πιστοποίησης Επαγγελματικών Δεξιοτήτων. Ο Διευθύνων Σύμβουλος της ESOLNET, κ. Γιώργος Αντωνιάδης, είναι γνωστός στην Ιδιωτική Εκπαίδευση της Ελλάδας για τις επιτυχίες του στη σύναψη συνεργασιών μεγάλων Βρετανικών Πανεπιστημίων και Φορέων Πιστοποίησης με Ελληνικά Ιδιωτικά Κολλέγια και μεγάλους Οργανισμούς Εκπαίδευσης.',
      'Στα 25 χρόνια που δραστηριοποιείται η ESOLNET στην Ελλάδα, ο κ. Γιώργος Αντωνιάδης έχει φέρει κατά καιρούς Awarding Bodies και Universities που διεξάγουν Εξετάσεις όπως τα: University of East London σε συνεργασία με το Anglia, University of Central Lancashire (UCLAN), ESB, OCNW (αργότερα Ascentis) σε συνεργασία με το Anglia, Ascentis, City and Guilds, LCCI (αργότερα EDI), NCFE, TQUK, Lifetime Awarding, ABC Awards, Anglia και άλλα.',
      'Πανεπιστήμια όπως το University of East London και το University of Central Lancashire, με τη συμμετοχή της ESOLNET, έχουν εισάγει Πανεπιστημιακά μαθήματα και συνεργάζονται με franchising με μεγάλους Ιδιωτικούς Εκπαιδευτικούς Οργανισμούς.',
      'Η ESOLNET ασχολείται ακόμη με την εισαγωγή στα Βρετανικά Πανεπιστήμια Ελλήνων και Κυπρίων Σπουδαστών. Με τη βοήθεια της ESOLNET, μεγάλος αριθμός Σπουδαστών έχει γίνει δεκτός στα Πανεπιστήμια Essex, Derby, Greenwich, East London, Central Lancashire, Lancaster, East Anglia, Winchester, Bucks, London και πολλά άλλα.',
      'H ESOLNET παρουσιάζει τις Εξετάσεις του Awarding Body LRN (Learning Resource Network) τις οποίες και εκπροσωπεί. Το LRN είναι ένας Οργανισμός Πιστοποίησης της Αγγλικής Γλώσσας και Πιστοποίησης Επαγγελματικών Δεξιοτήτων. Αναγνωρίζεται από το Ofqual (Βρετανικός Οργανισμός Πιστοποίησης) και το ΑΣΕΠ (Αντίστοιχος Οργανισμός στην Ελλάδα) και είναι Μέλος του ALTE ( Ένωση Εξεταστικών Φορέων Ξένης Γλώσσας της Ευρώπης), στο οποίο ανήκουν όλοι οι μεγάλοι Εξεταστικοί Φορείς Πιστοποίησης Ξένης Γλώσσας.',
      'Το LRN ξεχωρίζει γιατί μετά από διεξοδική έρευνα δημιούργησε Εξετάσεις που δίνουν προτεραιότητα στο σύγχρονο Σπουδαστή. Το LRN έχει λάβει υπόψη του όλες τις ανησυχίες των Καθηγητών της Αγγλικής Γλώσσας και κυρίως των Ιδιοκτητών ινστιτούτων Ξένων Γλωσσών και είναι σε θέση να προσφέρει Εξετάσεις απόλυτα προσαρμοσμένες στις ανάγκες των Ευρωπαίων Υποψηφίων στη σύγχρονη εποχή. Οι Εξετάσεις LRN είναι προσιτές και σε Υποψηφίους με μαθησιακές δυσκολίες, φιλικές, χωρίς παγίδες',
      'Η ESOLNET HELLAS εκπροσωπείται και υποστηρίζεται, από έμπειρους επαγγελματίες της Ξενόγλωσσης Εκπαίδευσης που δραστηριοποιούνται ως Προσωπικοί Σύμβουλοι Εξετάσεων και προωθούν τις Εξετάσεις LRN σε κάθε περιοχή της Ελλάδας.',
      'Ο θεσμός του Προσωπικού Συμβούλου Εξετάσεων, που για πρώτη φορά παρουσίασε η ESOLNET, σκοπό έχει να στηρίξει ΟΥΣΙΑΣΤΙΚΑ Ιδιοκτήτες φροντιστηρίων και Καθηγητές ώστε να επιβραβευτούν οι ίδιοι και οι Υποψήφιοί τους για τον κόπο και τον χρόνο που έχουν επενδύσει στην προετοιμασία των Εξετάσεων.'     
    ],
    "title2": 'Learning <br /> Recource Network',
    "subtitle2": 'ESOL INTERNATIONAL QUALIFICATIONS',
    "text2": [
      '– LRN – is an awarding organisation that offers qualifications to candidates, educational institutes, training providers, schools and employers who can access qualifications through registered educational institutions. <br /> It was founded by a group of educators and business people and specialises in ESOL and management qualifications. LRN London head office is supported by a team of representatives around the world.',
      'ESOL International qualifications are designed for candidates who are not native speakers of English and who wish to achieve a high quality, internationally recognised qualification in English that is both available and recognised worldwide and covers the whole range (NQF Entry 3 / CEF B1), (NQF level 1 / CEF B2), (NQF level 2 / CEF C1), (NQF level 3 / CEF C2). They are suitable for candidates who are preparing for entry to higher education or professional employment in the UK or elsewhere. ESOL International qualifications are designed to reference the descriptions of language proficiency in the Common European Framework Reference for Languages (CEF). The levels in the CEF have been mapped to the levels in the National Qualifications Framework for England, Wales and Northern Ireland.'
    ],
    "footerTitle": "Ακολουθήστε μας",
    "footerText": 'Ακολουθήστε μας στα Social Media και ενημερωθείτε άμεσα.'
  },
  "material": {
    "title1": "Past Papers LRN",
    "sections1": ['Past Papers LRN C2','Past Papers LRN C1','Past Papers LRN B2','Past Papers LRN B1'],
    "title2": "MOCK INTERVIEW VIDEOS",
    "sections2": ['LRN Mock Interview for Level C2','LRN Mock Interview for Level B2'],
    "pastPapers": {
      "b1": {
        "title": "LRN ENTRY LEVEL CERTIFICATE IN ESOL INTERNATIONAL (ENTRY 3) (CEF B1)",
        "section1": {
          "title": "LRN B1 JANUARY 2016",
          "pastPapers": [
            {
              "text": "LRN Level B1 January 2016 Exam Paper",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B1-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Level B1 January 2016 Speaking Examiner Instructions",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B1-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Level B1 January 2016 Answer Key",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B1-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B1 January 2016 Listening Section 1",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B1-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN B1 January 2016 Listening Section 2",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B1-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN B1 January 2016 Listening Section 3",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B1-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Level B1 Sample Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B1-Certificate-in-ESOL-International-Entry-3-CEF-B1-Listening-Writing-Reading-and-Use_Sample_paper.pdf"
            },
            {
              "title": "LRN Level B1 Sample Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B1-Certificate-in-ESOL-International-Entry-3-CEF-B1-Speaking_Sample_paper.pdf.pdf"
            },
            {
              "title": "LRN Level B1 Sample Writing Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Entry-Level-Certificate-in-ESOL-International-Entry-3-CEF-B1_WRITING_BOOKLET_Sample_paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B1 Listening Section 1",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B1__Listening_Section1.mp3"
            },
            {
              "text": "LRN B1 Listening Section 2",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B1__Listening_Section2.mp3"
            },
            {
              "text": "LRN B1 Listening Section 3",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B1__Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for B1 B2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-B1-B2.pdf"
        }
      },
      "b2": {
        "title": "LRN LEVEL 1 CERTIFICATE IN ESOL INTERNATIONAL (CEF B2)",
        "section1": {
          "title": "LRN B2 JANUARY 2016",
          "pastPapers": [
            {
              "text": "LRN Level B2 January 2016 Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B2-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Level B2 January 2016 Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B2-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Level B2 January 2016 Answer Key",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B2-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B2 January 2016 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B2-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN B2 January 2016 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B2-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN B2 January 2016 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B2-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Level B2 Sample Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B2-Certificate-in-ESOL-International-Entry-3-CEF-B2-Listening-Writing-Reading-and-Use_Sample_paper.pdf"
            },
            {
              "title": "LRN Level B2 Sample Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B2-Certificate-in-ESOL-International-Entry-3-CEF-B2-Speaking_Sample_paper.pdf.pdf"
            },
            {
              "title": "LRN Level B2 Sample Writing Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Entry-Level-Certificate-in-ESOL-International-Entry-3-CEF-B2_WRITING_BOOKLET_Sample_paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B2 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B2_Listening_Section2.mp3"
            },
            {
              "text": "LRN B2 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B2_Listening__Section2.mp3"
            },
            {
              "text": "LRN B2 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B2_Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for B1 B2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-B1-B2.pdf"
        }
      },
      "c1": {
        "title": "LRN LEVEL 1 CERTIFICATE IN ESOL INTERNATIONAL (CEF C1)",
        "section1": {
          "title": "LRN C1 JANUARY 2016",
          "pastPapers": [
            {
              "text": "LRN Level C1 January 2016 Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C1-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Level C1 January 2016 Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C1-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Level C1 January 2016 Answer Key",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C1-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C1 January 2016 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C1-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN C1 January 2016 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C1-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN C1 January 2016 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C1-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Level C1 Sample Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C1-Certificate-in-ESOL-International-Entry-3-CEF-C1-Listening-Writing-Reading-and-Use_Sample_paper.pdf"
            },
            {
              "title": "LRN Level C1 Sample Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C1-Certificate-in-ESOL-International-Entry-3-CEF-C1-Speaking_Sample_paper.pdf.pdf"
            },
            {
              "title": "LRN Level C1 Sample Writing Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Entry-Level-Certificate-in-ESOL-International-Entry-3-CEF-C1_WRITING_BOOKLET_Sample_paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C1 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C1_Listening_Section1.mp3"
            },
            {
              "text": "LRN C1 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C1_Listening_Section2.mp3"
            },
            {
              "text": "LRN C1 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C1_Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for C1 C2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-C1-C2.pdf"
        }
      },
      "c2": {
        "title": "LRN LEVEL 1 CERTIFICATE IN ESOL INTERNATIONAL (CEF C2)",
        "section1": {
          "title": "LRN C2 JANUARY 2016",
          "pastPapers": [
            {
              "text": "LRN Level C2 January 2016 Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C2-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Level C2 January 2016 Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C2-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Level C2 January 2016 Answer Key",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C2-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C2 January 2016 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C2-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN C2 January 2016 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C2-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN C2 January 2016 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C2-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Level C2 Sample Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C2-Certificate-in-ESOL-International-Entry-3-CEF-C2-Listening-Writing-Reading-and-Use_Sample_paper.pdf"
            },
            {
              "title": "LRN Level C2 Sample Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C2-Certificate-in-ESOL-International-Entry-3-CEF-C2-Speaking_Sample_paper.pdf.pdf"
            },
            {
              "title": "LRN Level C2 Sample Writing Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Entry-Level-Certificate-in-ESOL-International-Entry-3-CEF-C2_WRITING_BOOKLET_Sample_paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C2 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C2_Listening_Section1.mp3"
            },
            {
              "text": "LRN C2 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C2_Listening_Section2.mp3"
            },
            {
              "text": "LRN C2 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C2_Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for C1 C2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-C1-C2.pdf"
        }
      }
    },
    "mockVideos": {
      "b2": {
        "title": "LRN Mock Interview for Level B2",
        "subtitle": "A full LRN speaking test practice. For all speaking sample tests please follow the link: lrnhellas.gr",
        "link": "https://www.youtube.com/embed/LFJnqUxMSZk?feature=oembed",
        "directLink": "https://www.youtube.com/watch?v=LFJnqUxMSZk",
      },
      "c2": {
        "title": "LRN Mock Interview for Level C2",
        "subtitle": "A full LRN speaking test practice. For all speaking sample tests please follow the link: lrnhellas.gr",
        "link": "https://www.youtube.com/embed/WYhuXhIarJ4?feature=oembed",
        "directLink": "https://www.youtube.com/watch?v=WYhuXhIarJ4",
      }
    }
  },
  "registration": {
    "title": "ΕΓΓΡΑΦΕΣ",
    "subtitle": "Οι Εγγραφές για την Εξεταστική στις 20 Ιανουαρίου 2019 ξεκινούν στις 5 Νοεμβρίου 2018.",
    "duration": "<b>Διάρκεια Εγγραφών:</b><br /> 5 Νοεμβρίου έως και 9 Δεκεμβρίου 2018.",
    "guide": {
      "title": "Οδηγός ONLINE Εγγραφών 2018",
      "subtitle": "Ολοκληρώστε την εγγραφή σας με 3 βήματα για τις Εξετάσεις LRN",
      "text": "Κατεβάστε τον οδηγό"
    },
    "costsLrn": {
      "title": "Εξέταστρα LRN <br /> για την Εξεταστική Ιανουαρίου 2019",
      "costs": ['<b>LEVEL A1:</b> 70 €','<b>LEVEL A2:</b> 70 €','<b>LEVEL B1:</b> 70 €','<b>LEVEL B2:</b> 70 €','<b>LEVEL C1:</b> 80 €','<b>LEVEL C2:</b> 100 €']
    },
    "nameChange": {
      "title": "Διορθώσεις Ονομάτων",
      "text1": "Τα λάθη στα First Name και Last Name αναγνωρίζονται μόνο εάν έχει σταλεί η ταυτότητα ή το διαβατήριο που είναι και υποχρεωτικά.",
      "text2": "Λάθος στο όνομα πατρός σε περίπτωση που δεν αναγράφεται στο διαβατήριο του υποψηφίου και σε περίπτωση μη αποστολής ταυτότητας ή διαβατηρίου του πατρός, δεν αναγνωρίζεται.",
      "text3": "Για τη διόρθωση λάθους στο όνομα υποψηφίου, στο πιστοποιητικό, ισχύει χρέωση επανέκδοσης η οποία ανέρχεται σε 50 λίρες Αγγλίας (GBP), συν τα μεταφορικά έξοδα."
    }
  }
}

export default text