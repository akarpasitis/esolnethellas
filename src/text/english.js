const text = {
  "header": {
    "linkCompany": "COMPANY",
    "linkFaq": "FAQ",
    "emailLabel": "Contact:",
    "phone": "+34-675-59-53-79 / +357-96-643041",
    "emailAddress": "info@esolnetespana.es",
    "registrationInfo": "Registration Information",
    "language": "Language: ",
    "languages": ['Greek', 'Spanish', 'English'],
    "createAccount": "Sign Up / Login",
    "buttons": {
      "lrn": "LRN EXAMS",
      "material": "SUPPORTING MATERIAL",
      "news": "NEWS",
      "contact": "CONTACT"
    },
    "lrnButtons": ['LRN B1 Level', 'LRN B2 Level', 'LRN C1 Level', 'LRN C2 Level'],
    "materialButtons": ['LRN Past Papers', 'Mock Interview Videos']    
  },
  "homeText": {
    "description": "– LRN – is an awarding organisation that offers qualifications to candidates, educational institutes, training providers, schools and employers who can access qualifications through registered educational institutions. <br /> It was founded by a group of educators and business people and specialises in ESOL and management qualifications. LRN London head office is supported by a team of representatives around the world.",
    "title": "Why LRN Exams?",
    "more": "More...", 
    "carousel": [
      "Learning Resource Network",
      "LRN examination on the 20th of January 2019.",
      "Registrations from the 5th of November until the 9th of December 2018."
    ],
    "sections": [
      {
          "title": "SMART",
          "bullets": [
            {
              "main": "Base at all Levels is 50%.",
              "sub1": null
            },
            {
              "main": "All skills offset without any conditions.",
              "sub1": null
            },
            {
              "main": "FREE RETAKE",
              "sub1": null
            }
          ]
      },
      {
        "title": "INNOVATIVE",
        "bullets": [
          {
            "main": "The Listening part is easy and repeats 2 times.",
            "sub1": "LRN Innovation: Section 3 of Listening repeats for a 3rd time. LNM Innovation: Section 3 subject of Listening is similar to Task 1 subject, of Writing Section 1."
          },
          {
            "main": "LRN Innovation: The Syllabus of Speaking for the January and June Exams is provided from the start of the Shool Year."
          }
        ]
      },
      {
          "title": "FRIENDLY",
          "bullets": [
            {
              "main": "Affordable Exams: they give opportunities, without a catch, without particularities.",
              "sub1": null
            },
            {
              "main": "Goal, the decrease of stress of the Examined and his/hers higher performance.",
              "sub1": null
            },
            {
              "main": "Favorable for students with learning difficulties.",
              "sub1": null
            }
          ]
      }
    ],
    recognition: 'Recognition',
    bodies: [
      {
        title: '<b>ALTE</b> The Association of Language testers in Europe.',
        text1: 'ALTE: is an association of language test providers who work together to promote the fair and accurate assessment of linguistic ability across Europe and beyond.',
        text2: 'It was founded in 1989 by the University of Cambridge (UK) and the Universidad de Salamanca (Spain). Today, ALTE has 33 Full Members representing 25 European languages, as well as 58 Institutional Affiliates and over 500 Individual Affiliates from all around the world. ALTE is a non-profit organisation, and has Participatory Status as an NGO with the Council of Europe.'
      },
      {
        title: '<b>UCAS:</b> The Universities and Colleges Admissions Service.',
        text1: 'A UK-based organisation whose main role is to operate the application process for British universities.',
        text2: ''
      },
      {
        title: '<b>OFQUAL:</b> The Office of Qualifications and Examinations Regulation.',
        text1: 'A non-ministerial government department that regulates qualifications, exams and tests in England and, until May 2016, vocational qualifications in Northern Ireland. Colloquially and publicly, Ofqual is often referred to as the exam "watchdog"',
        text2: ''
      }
    ]
  },
  "contact": {
    "text": "Contact us for more information.",
    "addressLabel": "Address",
    "address": "Thesallonikis 46, Flat 1, 6035, Larnaca, Cyprus",
    "scheduleLabel": "Call Center Schedule",
    "schedule": "Monday – Friday 9:00 – 20:00",
    "phoneLabel": "Phone",
    "phone": "ES: +34 675 595 379 <br /> CY: +357 96 643041 <br /> UK: +44 7446 069628",
    "emailLabel": "Email",
    "email": "info@esolnetespana.es"
  },
  "footer": {
    "button1": "COMPANY",
    "button2": "FAQ",
    "button3": "REGISTRATIONS",
    "button4": "PRIVACY POLICY",
    "phoneLabel": "Tel.",
    "telephone": "ES: +34 675 595 379 / CY: +357 96 643041 / UK: +44 7446 069628",
    "emailLabel": "Email",
    "emailAddress": "info@esolnetespana.es",
    "address": "Thesallonikis 46, Flat 1, 6035, Larnaca, Cyprus",
    "schedule": "Call Center: Monday – Friday 9:00 – 20:00",
    "copyright": "© 2017 Esolnet Hellas."
  },
  "cookieNotice": {
    "text": "We use third-party cookies to improve our services. You can change the configuration or obtain more information in our cookies policy here: ",
    "link": "cookie policy",
    "close": "Accept"
  },
  "lrnExams": {
    "title1": "WHY LRN?",
    "title2": "BECAUSE, LRN EXAMS:",
    "subtitle1": "Are you worried with the performance of your students?",
    "subtitle2": "With LRN Exams, your students have the maximum percent of success. Declare them with confidence.",
    "bullets": [
      {
        "main": "Recognized by Ofqual and ASEP.",
        "sub1": null
      },
      {
        "main": "They are affordable, without a catch, without peculiarities, WITHOUT STRESS.",
        "sub1": null
      },
      {
        "main": "They have a base of 50% in all Levels.",
        "sub1": null
      },
      {
        "main": "Offset ALL the Skills without conditions.",
        "sub1": null
      },
      {
        "main": "The Listening part is easy and repeats twice.",
        "sub1": "Innocation: The Listening part, section 3, in all Levels repeats for a 3rd time.",
        "sub2": "Innovation: The Writing part, Section 1, Task 1, has the same subject with the Listening part, Section 3."
      },
      {
        "main": "The Tasks in the Writing part are accompanied by ideas.",
        "sub1": null
      },
      {
        "main": "The 2 Writing Tasks, that the candidates write, favor the best performance and grading.",
        "sub1": null
      },
      {
        "main": "The Candidates project their best abilities, that normally have in Listening, Writing, Speaking.",
        "sub1": null
      },
      {
        "main": "The Reading and Use offset in a common grade.",
        "sub1": null
      },
      {
        "main": "Favorable for Level C2 too.",
        "sub1": null
      },
      {
        "main": "They are easy and they drive students to success much easier.",
        "sub1": null
      },
      {
        "main": "The duration of the Exams is united, without breaks and more than enough.",
        "sub1": null
      },
      {
        "main": "The Candidates of ALL Levels, are tested on all the skills of written and oral speech.",
        "sub1": null
      },
      {
        "main": "Also Favorable for Candidates with learning difficulties.",
        "sub1": null
      },
      {
        "main": "Innovation: We have established the FREE RETAKE.",
        "sub1": null
      },
      {
        "main": "Exams take place in January and June.",
        "sub1": null
      },
      {
        "main": "FREE, Rich Supporting Material.",
        "sub1": null
      },
      {
        "main": "Innovation: We have established the Personal Exam Consultant, for the support and continuous Communication.",
        "sub1": null
      },
      {
        "main": "The Foreign Language Centers do not need to include the addresses of their students in application forms or give personal and professional data.",
        "sub1": null
      }
    ],
    "examsButtons": ['Listening','Writing','Reading and Use','Speaking'],
    "exams": [
      {
        "name": "LRN B1",
        "price": "70€",
        "text": "The <b> LRN EXAM </b> is <b> uniform </b>. It is taken all together and the candidates have the possibility to manage their time and return back to their answers and correct or complete what is needed.",
        "duration": {
          "title": "Exam Duration: ",
          "text": "2 hours and 30 minutes."
        },
        "tableTitle": "The <b> LRN EXAM </b> consists of: ",
        "table": {
          "rows": [
            [" ","Skills","Parts"],
            ["1.","Listening","3 Sections"],
            ["2.","Writing","2 Sections"],
            ["3.","Reading and Use","4 Sections"],
            ["4.","Speaking","4 Sections"]                            
          ]
        },
        "listening": {
          "title": "The <b> Listening </b> part of <b> LRN EXAM </b> consists of :",
          "section1": {
            "title": "Listening Section 1: (9 marks)",
            "bullets": ["Heard 2 times","9 small dialogues, 9 questions","1 mark each question"]
          },
          "section2": {
            "title": "Listening Section 2: (6 marks)",
            "bullets": ["Heard 2 times","3, slightly bigger, dialogues","2 questions every dialogue","1 mark each question"]
          },
          "section3": {
            "title": "Listening Section 3: (5 marks)",
            "bullets": ["Heard 2 times","1 text with subject Friendly Letter","5 questions","1 mark each question"]
          },
          "hint": {
            "title": "<b>Innovation</b>",
            "text": "in all levels the Listening Section 3 is heard a 3rd time in combination with the Writing, Section 1, Task 1."
          }
        },
        "writing": {
          "title": "The <b>WRITING</b> part of the <b>LRN EXAM</b> consists in all the Levels by 2 Sections.",
          "text1": "In <b>Writing Section 1, Task 1</b> the candidates <b>compulsory</b> write the subject of the subject they heard in Listening Section 3.<br /> The subject of <b>Listening Section 3</b> and the <b>Writing Section 1, Task 1</b> is a Friendly Letter.",
          "text2": "In <b>Writing Section 2</b> there are <b>3 options.</b>",
          "bullets": ["Another Friendly Letter (accompanied by ideas)","Story","Essay (accompanied by ideas)"],
          "text3": "The 2 Writing parts of the LRN EXAM offer the opportunity to the candidates for a better score.",
          "table": {
            "title": "The 2 Writing parts of the LRN EXAM offer the opportunity to the candidates for a better score.",
            "subtitle": "Number of words of Writing EXAM",              
            "rows": [
              ["Levels","1st Task","2nd Task"],
              ["B1","90-110","110-150"]
            ]
          }
        },
        "reading": {
          "title": "The READING AND USE parts of the LRN EXAM are together and share the marks.",
          "table1": {
            "title": "The Reading part consists of: ",
            "rows": [
              ["Section 1:","1 text, 8 questions ( ½ mark each)"],
              ["Section 2:","2 small texts with a common subject"],
              [" ", "1st text, 3 – 4 questions. <br /> 2nd text, 3 – 4 questions."],
              [" ", "Total questions 15, each question is worth ½ mark."]
            ]
          },
          "table2": {
            "title": "The USE part consists of: ",
            "rows": [
              ["Section 3:","15 multiple choice questions for grammar, 1/2 mark each question."],
              ["Section 4:","1 text fill in with 10 multiple choice questions , ½ mark each question."],
              [" ", " "],
              [" ","Total score Reading and Use: 15 + 25 = 40 : 2 = 20 marks"]
            ]
          },
          "footer": "The offsetting of the Reading and Use part favors the better scoring performance of the candidates."
        },
        "speaking": {
          "title": "In the SPEAKING part of the LRN EXAM take part : 2 Candidates, 1 Examiner.",
          "duration": {
            "title": "Exam duration: ",
            "text": "14 – 16 minutes."
          },
          "bulletTitle": "Breakdown of the 3 Sections: ",
          "bullets": [
            {
              "heading": "Section 1: ",
              "text": "Warm up questions"
            },
            {
              "heading": "Section 2: ",
              "text": "Candidates talk on 1 of the 5 topics they have chosen and prepared."
            },
            {
              "heading": "Section 3: ",
              "text": "Candidates respond to a question related to Section 2; points are given for them to use."
            }
          ],
          "text1": "The Speaking Section 2 part of the LRN EXAM is send for preparation 2 weeks before the oral exams. Requires the preparation of the candidates in 1 of the 5 subjects that are sent.",
          "text2": "It is possible for the 2 candidates that are examined in the Speaking part to select the same subject.",
          "text3": "Innovation: The Syllabus of the Speaking part for the January Exams is provided from the beginning of the school year."
        }
      },
      {
        "name": "LRN B2",
        "price": "70€",
        "text": "The <b> LRN EXAM </b> is <b> uniform </b>. It is taken all together and the candidates have the possibility to manage their time and return back to their answers and correct or complete what is needed.",
        "duration": {
          "title": "Exam Duration: ",
          "text": "2 hours and 30 minutes."
        },
        "tableTitle": "The <b> LRN EXAM </b> consists of: ",
        "table": {
          "rows": [
            [" ","Skills","Parts"],
            ["1.","Listening","3 Sections"],
            ["2.","Writing","2 Sections"],
            ["3.","Reading and Use","4 Sections"],
            ["4.","Speaking","3 Sections"]                            
          ]
        },
        "listening": {
          "title": "The <b> Listening </b> part of <b> LRN EXAM </b> consists of :",
          "section1": {
            "title": "Listening Section 1: (9 marks)",
            "bullets": ["Heard 2 times","9 small dialogues, 9 questions","1 mark each question"]
          },
          "section2": {
            "title": "Listening Section 2: (6 marks)",
            "bullets": ["Heard 2 times","3, slightly bigger, dialogues","2 questions every dialogue","1 mark each question"]
          },
          "section3": {
            "title": "Listening Section 3: (5 marks)",
            "bullets": ["Heard 2 times","1 text with subject Friendly Letter","5 questions","1 mark each question"]
          },
          "hint": {
            "title": "<b>Innovation</b>",
            "text": "in all levels the Listening Section 3 is heard a 3rd time in combination with the Writing, Section 1, Task 1."
          }
        },
        "writing": {
          "title": "The <b>WRITING</b> part of the <b>LRN EXAM</b> consists in all the Levels by 2 Sections.",
          "text1": "In <b>Writing Section 1, Task 1</b> the candidates <b>compulsory</b> write the subject of the subject they heard in Listening Section 3.<br /> The subject of <b>Listening Section 3</b> and the <b>Writing Section 1, Task 1</b> is a Friendly Letter.",
          "text2": "In <b>Writing Section 2</b> there are <b>3 options.</b>",
          "bullets": ["Another Friendly Letter (accompanied by ideas)","Story","Essay (accompanied by ideas)"],
          "text3": "The 2 Writing parts of the LRN EXAM offer the opportunity to the candidates for a better score.",
          "table": {
            "title": "The 2 Writing parts of the LRN EXAM offer the opportunity to the candidates for a better score.",
            "subtitle": "Number of words of Writing EXAM",              
            "rows": [
              ["Levels","1st Task","2nd Task"],
              ["B2","100-120","120-170"]
            ]
          }
        },
        "reading": {
          "title": "The READING AND USE parts of the LRN EXAM are together and share the marks.",
          "table1": {
            "title": "The Reading part consists of: ",
            "rows": [
              ["Section 1:","1 text, 8 questions ( ½ mark each)"],
              ["Section 2:","2 small texts with a common subject"],
              [" ", "1st text, 3 – 4 questions. <br /> 2nd text, 3 – 4 questions."],
              [" ", "Total questions 15, each question is worth ½ mark."]
            ]
          },
          "table2": {
            "title": "The USE part consists of: ",
            "rows": [
              ["Section 3:","15 multiple choice questions for grammar, 1/2 mark each question."],
              ["Section 4:","1 text fill in with 10 multiple choice questions , ½ mark each question."],
              [" ", " "],
              [" ","Total score Reading and Use: 15 + 25 = 40 : 2 = 20 marks"]
            ]
          },
          "footer": "The offsetting of the Reading and Use part favors the better scoring performance of the candidates."
        },
        "speaking": {
          "title": "In the SPEAKING part of the LRN EXAM take part : 2 Candidates, 1 Examiner.",
          "duration": {
            "title": "Exam duration: ",
            "text": "14 – 16 minutes."
          },
          "bulletTitle": "Breakdown of the 3 Sections: ",
          "bullets": [
            {
              "heading": "Section 1: ",
              "text": "Warm up questions"
            },
            {
              "heading": "Section 2: ",
              "text": "Candidates talk on 1 of the 5 topics they have chosen and prepared."
            },
            {
              "heading": "Section 3: ",
              "text": "Candidates respond to a question related to Section 2; points are given for them to use."
            }
          ],
          "text1": "The Speaking Section 2 part of the LRN EXAM is send for preparation 2 weeks before the oral exams. Requires the preparation of the candidates in 1 of the 5 subjects that are sent.",
          "text2": "It is possible for the 2 candidates that are examined in the Speaking part to select the same subject.",
          "text3": "Innovation: The Syllabus of the Speaking part for the January Exams is provided from the beginning of the school year."
        }
      },
      {

        "name": "LRN C1",
        "price": "80€",
        "text": "The <b> LRN EXAM </b> is <b> uniform </b>. It is taken all together and the candidates have the possibility to manage their time and return back to their answers and correct or complete what is needed.",
        "duration": {
          "title": "Exam Duration: ",
          "text": "3 hours."
        },
        "tableTitle": "The <b> LRN EXAM </b> consists of: ",
        "table": {
          "rows": [
            [" ","Skills","Parts"],
            ["1.","Listening","3 Sections"],
            ["2.","Writing","2 Sections"],
            ["3.","Reading and Use","4 Sections"],
            ["4.","Speaking","3 Sections"]                            
          ]
        },
        "listening": {
          "title": "The Listening part of the LRN EXAM consists of:",
          "section1": {
            "title": "Listening Section 1: (10 marks)",
            "bullets": ["Heard 2 times","10 small dialogues, 10 questions","1 mark each question"]
          },
          "section2": {
            "title": "Listening Section 2: (10 marks)",
            "bullets": ["Heard 2 times","3 slightly bigger dialogues","10 questions","Each dialogue, 2 - 4 questions","1 mark each question"]
          },
          "section3": {
            "title": "Listening Section 3: (5 marks)",
            "bullets": ["Heard 2 times","1 text with subject 'Essay'","5 questions","1 mark each question"]
          },
          "hint": {
            "title": "Innovation",
            "text": "in all levels the Listening Section 3 is heard a 3rd time in combination with the Writing, Section 1, Task 1."
          }
        },
        "writing": {
          "title": "The <b>WRITING</b> part of the <b>LRN EXAM</b> consists in all the Levels by 2 Sections.",
          "text1": "In <b>Writing Section 1, Task 1</b> the candidates <b>compulsory</b> write the subject of the subject they heard in Listening Section 3.<br /> The subject of <b>Listening Section 3</b> and the <b>Writing Section 1, Task 1</b> is a Friendly Letter.",
          "text2": "In <b>Writing Section 2</b> there are <b>3 options from 3 Essays which are accompanied by ideas.</b>",
          "bullets": [],
          "text3": "The 2 Writing parts of the LRN EXAM offer the opportunity to the candidates for a better score.",
          "table": {
            "title": "Number of words of Writing EXAM",              
            "rows": [
              ["Levels","1st Task","2nd Task"],
              ["C1","150-200","250-300"]
            ]
          }
        },
        "reading": {
          "title": "The READING AND USE parts of the LRN EXAM are together and share the marks.",
          "table1": {
            "title": "The Reading part consists of: ",
            "rows": [
              ["Section 1:","1 text, 8 questions ( ½ mark each)"],
              ["Section 2:","2 small texts with a common subject"],
              [" ", "1st text, 3 – 4 questions. <br /> 2nd text, 3 – 4 questions.","finally, 2-3 questions for both texts."],
              [" ", "Total questions 20, each question is worth ½ mark."]
            ]
          },
          "table2": {
            "title": "The USE part consists of: ",
            "rows": [
              ["Section 3:","20 multiple choice questions for grammar, 1/2 mark each question."],
              ["Section 4:","1 text fill in with 10 gaps, ½ mark each question."],
              [" ", " "],
              [" ","Total score Reading and Use: 20 + 30 = 50 : 2 = 25 marks"]
            ]
          },
          "footer": "The offsetting of the Reading and Use part favors the better scoring performance of the candidates."
        },
        "speaking": {
          "title": "In the SPEAKING part of the LRN EXAM take part : 2 Candidates, 1 Examiner.",
          "duration": {
            "title": "Exam duration: ",
            "text": "16 – 18 minutes."
          },
          "bulletTitle": "Breakdown of the 3 Sections: ",
          "bullets": [
            {
              "heading": "Section 1: ",
              "text": "Warm up questions"
            },
            {
              "heading": "Section 2: ",
              "text": "Candidates talk on 1 of the 5 topics they have chosen and prepared."
            },
            {
              "heading": "Section 3: ",
              "text": "Candidates respond to a question related to Section 2; points are given for them to use."
            }
          ],
          "text1": "The Speaking Section 2 part of the LRN EXAM is send for preparation 2 weeks before the oral exams. Requires the preparation of the candidates in 1 of the 5 subjects that are sent.",
          "text2": "It is possible for the 2 candidates that are examined in the Speaking part to select the same subject.",
          "text3": "Innovation: The Syllabus of the Speaking part for the January Exams is provided from the beginning of the school year."
        }
      },
      {
        "name": "LRN C2",
        "price": "100€",
        "text": "The <b> LRN EXAM </b> is <b> uniform </b>. It is taken all together and the candidates have the possibility to manage their time and return back to their answers and correct or complete what is needed.",
        "duration": {
          "title": "Exam Duration: ",
          "text": "3 hours."
        },
        "tableTitle": "The <b> LRN EXAM </b> consists of: ",
        "table": {
          "rows": [
            [" ","Skills","Parts"],
            ["1.","Listening","3 Sections"],
            ["2.","Writing","2 Sections"],
            ["3.","Reading and Use","4 Sections"],
            ["4.","Speaking","3 Sections"]                            
          ]
        },

        "listening": {
          "title": "The Listening part of the LRN EXAM consists of:",
          "section1": {
            "title": "Listening Section 1: (10 marks)",
            "bullets": ["Heard 2 times","10 small dialogues, 10 questions","1 mark each question"]
          },
          "section2": {
            "title": "Listening Section 2: (10 marks)",
            "bullets": ["Heard 2 times","3 slightly bigger dialogues","10 questions","Each dialogue, 2 - 4 questions","1 mark each question"]
          },
          "section3": {
            "title": "Listening Section 3: (5 marks)",
            "bullets": ["Heard 2 times","1 text with subject 'Essay'","5 questions","1 mark each question"]
          },
          "hint": {
            "title": "Innovation",
            "text": "in all levels the Listening Section 3 is heard a 3rd time in combination with the Writing, Section 1, Task 1."
          }
        },
        "writing": {
          "title": "The <b>WRITING</b> part of the <b>LRN EXAM</b> consists in all the Levels by 2 Sections.",
          "text1": "In <b>Writing Section 1, Task 1</b> the candidates <b>compulsory</b> write the subject of the subject they heard in Listening Section 3.<br /> The subject of <b>Listening Section 3</b> and the <b>Writing Section 1, Task 1</b> is a Friendly Letter.",
          "text2": "In <b>Writing Section 2</b> there are <b>3 options from 3 Essays which are accompanied by ideas.</b>",
          "bullets": [],
          "text3": "The 2 Writing parts of the LRN EXAM offer the opportunity to the candidates for a better score.",
          "table": {
            "title": "Number of words of Writing EXAM",              
            "rows": [
              ["Levels","1st Task","2nd Task"],
              ["C2","200-250","250-300"]
            ]
          }
        },
        "reading": {
          "title": "The READING AND USE parts of the LRN EXAM are together and share the marks.",
          "table1": {
            "title": "The Reading part consists of: ",
            "rows": [
              ["Section 1:","1 text, 9 questions ( ½ mark each question)"],
              ["Section 2:","2 small texts"],
              [" ", "1st text, 3 – 4 questions. <br /> 2nd text, 3 – 4 questions. <br /> finally, 2-3 questions for both texts."],
              [" ", "Total questions 20, each question is worth ½ mark."]
            ]
          },
          "table2": {
            "title": "The USE part consists of: ",
            "rows": [
              ["Section 3:","20 multiple choice questions for grammar, 1/2 mark each question."],
              ["Section 4:","1 text fill in with 10 gaps, ½ mark each question."],
              [" ", " "],
              [" ","Total score Reading and Use: 20 + 30 = 50 : 2 = 25 marks"]
            ]
          },
          "footer": "The offsetting of the Reading and Use part favors the better scoring performance of the candidates."
        },
        "speaking": {
          "title": "In the SPEAKING part of the LRN EXAM take part : 2 Candidates, 1 Examiner.",
          "duration": {
            "title": "Exam duration: ",
            "text": "16 – 18 minutes."
          },
          "bulletTitle": "Breakdown of the 3 Sections: ",
          "bullets": [
            {
              "heading": "Section 1: ",
              "text": "Warm up questions"
            },
            {
              "heading": "Section 2: ",
              "text": "Candidates talk on 1 of the 5 topics they have chosen and prepared."
            },
            {
              "heading": "Section 3: ",
              "text": "Candidates respond to a question related to Section 2; points are given for them to use."
            }
          ],
          "text1": "The Speaking Section 2 part of the LRN EXAM is send for preparation 2 weeks before the oral exams. Requires the preparation of the candidates in 1 of the 5 subjects that are sent.",
          "text2": "It is possible for the 2 candidates that are examined in the Speaking part to select the same subject.",
          "text3": "Innovation: The Syllabus of the Speaking part for the January Exams is provided from the beginning of the school year."
        }
      }
    ]
  },
  "faqs": {
    "exams": [
      {
        'q': 'When do the results come out?',
        'a': 'The results of the exams are available about 10 weeks from the day of the Exams.'
      },
      {
        'q': 'Which are the Exam Centers?',
        'a': 'Contact us to provide your with information for the Exam Centers closest to your area.'
      },
      {
        'q': 'When are Candidates\' Bulletins sent?',
        'a': 'Bulletins are available 2 weeks before the Exams.'
      },
      {
        'q': 'When is the Exam Date?',
        'a': '20th January 2019.'
      },
      {
        'q': 'When are the oral preparations available?',
        'a': 'Section 2 Speaking of LRN is available to the candidates around 2 weeks before the exams.'
      },
      {
        'q': 'What happens in the case of an absence?',
        'a': 'In the event that a Candidate is absent due to illness or serious reason, no refunds are made to the examiner but instead the amount for the upcoming Exam is credited. In this case, you must contact us for a new application.'
      },
      {
        'q': 'What about candidates with learning difficulties?',
        'a': 'Candidates with learning difficulties need to submit any opinion/report available to them at registration. Since this year, the translation of the report is been made by Esolnet Hellas. Innovation: We help you evaluate your Candidates with learning difficulties in Writing. Please send us samples of Writing and which will then be returned with comments on their performance. These samples must be written in pen, in competition form and have paragraphs that are clearly distinguishable.'
      }
    ],
    "registration": [
      {
        'q': 'What are the deadlines for application?',
        'a': 'Please refer to the current Exam Period Update page.'
      },
      {
        'q': 'What are the fees for the LRN Exams?',
        'a': 'LEVEL A1: 70€<br /> LEVEL A2: 70€<br /> LEVEL B1: 70€<br /> LEVEL B2: 70€<br /> LEVEL C1: 80€<br /> LEVEL C2: 100€'
      },
      {
        'q': 'Bank Accounts',
        'a': ''
      }
    ],
    "material": [
      {
        'q': 'What studying material is available?',
        'a': 'SUPERCOUSE Editions have books available for levels B2 and C2.<br /> BETSIS Editions have books available for Levels B1, B2, C1 και C2.'
      },
      {
        'q': 'Where can I find available Supporting Material?',
        'a': 'Additional material can be found in the section of our Supporting Material\'s page. <br /> For the preparation of the Candidates, electronic and additional LRN material is provided.'
      }
    ],
    'retake': [
      {
        'q': 'In the case of a retake, should I reapply?',
        'a': 'A new application must be submitted with all the Candidate\'s details and a footnote that this is a retake.'
      },
    ],
    'recognizition': [
      {
        'q': 'What is the recognition of the LRN Exams abroad?',
        'a': 'The LRN Exams are recognized by Ofqual, ALTE, UCAS.'
      }
    ]
  },
  "faqsTitles": ['1. Exams', '2. Registration','3. Supporting Material','4. Free Retake','5. Recognizing Bodies'],
  "company": {
    "headerTitle": 'ESOLNET',
    "headerSubtitle": 'English Language & Professional Skills Certification',
    "title1": 'The Professionals In Certification',
    "text1": [
      'ESOLNET was founded in Cyprus from professionals in the Certification of the English Language and the Certification of Professional Skills. The Managing Director of ESOLNET, Mr. George Antoniadis, is well known in Greek Private Education for his successful partnerships with major British Universities, Certification Bodies, with Greek Private Colleges and important Educational Organisations.',
      'In the 25 years that ESOLNET has been active in Greece, Mr. George Antoniadis has managed to bring in Awarding Bodies and Universities that hold the Exams. The Awarding Bodies include: University of East London in cooperation with Anglia, University of Central Lancashire (UCLAN), ESB, OCNW (now Ascentis) in cooperation with Anglia Ascentis, City and Guilds, LCCI (now EDI), NCFE, TQUK, Lifetime Awarding, ABC Awards, Anglia and more.',
      'Universities like the University of East London and the University of Central Lancashire, with the participation of ESOLNET, have introduced University courses and are cooperating with franchising of major Private Educational Organisations.',
      'ESOLNET, also deals with the entrance of Greek and Cypriot Students to British Universities. With the help of ESOLNET, a significant  number of University Students has been accepted in the Universities of Essex, Derby, Greenwich, East London, Central Lancashire, Lancaster, East Anglia, Winchester, Bucks, London and many more.',
      'ESOLNET presents the LRN (Learning Resource Network) Awarding Body Exams which also represents. LRN is Organisation for the Certification of the English Language and the Certification of Professional Skills. It’s recognised by Ofqual, and is a member of ALTE (Association of Language Testers in Europe), which all of the major Examination Certification Bodies for Foreign Languages belong to.',
      'LRN stands out from the rest because after thorough research it has produced Exams that give priority to the modern University Student. LRN has taken into account all the issues that English Language Professors face and mainly the issues that Private Institutes of Foreign Languages face, and are in a position to offer exams fully complying to the needs of European candidates in the modern age. LRN Exams are also affordable to candidates with learning difficulties, friendly with no pitfalls.',
      'ESOLNET España is represented and supported by experienced professionals of the Foreign Languages Education that are active as Personal Exam Consultants and promote the LRN Exams in every part of Spain.',
      'The institution of the Personal Exam Consultant, which was introduced by ESOLNET, has the aim to support SUBSTANTIALLY institution owners and professors, so that they and their students can be rewarded for their efforts and time that they invested for the Exam preparation.'     
    ],
    "title2": 'Learning <br /> Recource Network',
    "subtitle2": 'ESOL INTERNATIONAL QUALIFICATIONS',
    "text2": [
      '– LRN – is an awarding organisation that offers qualifications to candidates, educational institutes, training providers, schools and employers who can access qualifications through registered educational institutions. <br /> It was founded by a group of educators and business people and specialises in ESOL and management qualifications. LRN London head office is supported by a team of representatives around the world.',
      'ESOL International qualifications are designed for candidates who are not native speakers of English and who wish to achieve a high quality, internationally recognised qualification in English that is both available and recognised worldwide and covers the whole range (NQF Entry 3 / CEF B1), (NQF level 1 / CEF B2), (NQF level 2 / CEF C1), (NQF level 3 / CEF C2). They are suitable for candidates who are preparing for entry to higher education or professional employment in the UK or elsewhere. ESOL International qualifications are designed to reference the descriptions of language proficiency in the Common European Framework Reference for Languages (CEF). The levels in the CEF have been mapped to the levels in the National Qualifications Framework for England, Wales and Northern Ireland.'
    ],
    "footerTitle": "Follow Us",
    "footerText": 'Follow us on Social Media and get instant updates.'
  },
  "material": {
    "title1": "Past Papers LRN",
    "sections1": ['Past Papers LRN C2','Past Papers LRN C1','Past Papers LRN B2','Past Papers LRN B1'],
    "title2": "MOCK INTERVIEW VIDEOS",
    "sections2": ['LRN Mock Interview for Level C2','LRN Mock Interview for Level B2'],
    "pastPapers": {
      "b1": {
        "title": "LRN ENTRY LEVEL CERTIFICATE IN ESOL INTERNATIONAL (ENTRY 3) (CEF B1)",
        "section1": {
          "title": "LRN B1 JANUARY 2016",
          "pastPapers": [
            {
              "text": "LRN Level B1 January 2016 Exam Paper",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B1-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Level B1 January 2016 Speaking Examiner Instructions",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B1-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Level B1 January 2016 Answer Key",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B1-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B1 January 2016 Listening Section 1",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B1-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN B1 January 2016 Listening Section 2",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B1-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN B1 January 2016 Listening Section 3",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B1-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Level B1 Sample Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B1-Certificate-in-ESOL-International-Entry-3-CEF-B1-Listening-Writing-Reading-and-Use_Sample_paper.pdf"
            },
            {
              "title": "LRN Level B1 Sample Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B1-Certificate-in-ESOL-International-Entry-3-CEF-B1-Speaking_Sample_paper.pdf.pdf"
            },
            {
              "title": "LRN Level B1 Sample Writing Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Entry-Level-Certificate-in-ESOL-International-Entry-3-CEF-B1_WRITING_BOOKLET_Sample_paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B1 Listening Section 1",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B1__Listening_Section1.mp3"
            },
            {
              "text": "LRN B1 Listening Section 2",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B1__Listening_Section2.mp3"
            },
            {
              "text": "LRN B1 Listening Section 3",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B1__Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for B1 B2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-B1-B2.pdf"
        }
      },
      "b2": {
        "title": "LRN LEVEL 1 CERTIFICATE IN ESOL INTERNATIONAL (CEF B2)",
        "section1": {
          "title": "LRN B2 JANUARY 2016",
          "pastPapers": [
            {
              "text": "LRN Level B2 January 2016 Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B2-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Level B2 January 2016 Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B2-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Level B2 January 2016 Answer Key",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B2-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B2 January 2016 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B2-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN B2 January 2016 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B2-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN B2 January 2016 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B2-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Level B2 Sample Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B2-Certificate-in-ESOL-International-Entry-3-CEF-B2-Listening-Writing-Reading-and-Use_Sample_paper.pdf"
            },
            {
              "title": "LRN Level B2 Sample Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B2-Certificate-in-ESOL-International-Entry-3-CEF-B2-Speaking_Sample_paper.pdf.pdf"
            },
            {
              "title": "LRN Level B2 Sample Writing Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Entry-Level-Certificate-in-ESOL-International-Entry-3-CEF-B2_WRITING_BOOKLET_Sample_paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B2 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B2_Listening_Section2.mp3"
            },
            {
              "text": "LRN B2 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B2_Listening__Section2.mp3"
            },
            {
              "text": "LRN B2 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B2_Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for B1 B2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-B1-B2.pdf"
        }
      },
      "c1": {
        "title": "LRN LEVEL 1 CERTIFICATE IN ESOL INTERNATIONAL (CEF C1)",
        "section1": {
          "title": "LRN C1 JANUARY 2016",
          "pastPapers": [
            {
              "text": "LRN Level C1 January 2016 Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C1-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Level C1 January 2016 Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C1-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Level C1 January 2016 Answer Key",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C1-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C1 January 2016 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C1-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN C1 January 2016 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C1-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN C1 January 2016 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C1-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Level C1 Sample Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C1-Certificate-in-ESOL-International-Entry-3-CEF-C1-Listening-Writing-Reading-and-Use_Sample_paper.pdf"
            },
            {
              "title": "LRN Level C1 Sample Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C1-Certificate-in-ESOL-International-Entry-3-CEF-C1-Speaking_Sample_paper.pdf.pdf"
            },
            {
              "title": "LRN Level C1 Sample Writing Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Entry-Level-Certificate-in-ESOL-International-Entry-3-CEF-C1_WRITING_BOOKLET_Sample_paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C1 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C1_Listening_Section1.mp3"
            },
            {
              "text": "LRN C1 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C1_Listening_Section2.mp3"
            },
            {
              "text": "LRN C1 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C1_Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for C1 C2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-C1-C2.pdf"
        }
      },
      "c2": {
        "title": "LRN LEVEL 1 CERTIFICATE IN ESOL INTERNATIONAL (CEF C2)",
        "section1": {
          "title": "LRN C2 JANUARY 2016",
          "pastPapers": [
            {
              "text": "LRN Level C2 January 2016 Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C2-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Level C2 January 2016 Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C2-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Level C2 January 2016 Answer Key",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C2-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C2 January 2016 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C2-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN C2 January 2016 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C2-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN C2 January 2016 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C2-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Level C2 Sample Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C2-Certificate-in-ESOL-International-Entry-3-CEF-C2-Listening-Writing-Reading-and-Use_Sample_paper.pdf"
            },
            {
              "title": "LRN Level C2 Sample Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-C2-Certificate-in-ESOL-International-Entry-3-CEF-C2-Speaking_Sample_paper.pdf.pdf"
            },
            {
              "title": "LRN Level C2 Sample Writing Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Entry-Level-Certificate-in-ESOL-International-Entry-3-CEF-C2_WRITING_BOOKLET_Sample_paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C2 Listening Section 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C2_Listening_Section1.mp3"
            },
            {
              "text": "LRN C2 Listening Section 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C2_Listening_Section2.mp3"
            },
            {
              "text": "LRN C2 Listening Section 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C2_Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for C1 C2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-C1-C2.pdf"
        }
      }
    },
    "mockVideos": {
      "b2": {
        "title": "LRN Mock Interview for Level B2",
        "subtitle": "A full LRN speaking test practice. For all speaking sample tests please follow the link: lrnhellas.gr",
        "link": "https://www.youtube.com/embed/LFJnqUxMSZk?feature=oembed",
        "directLink": "https://www.youtube.com/watch?v=LFJnqUxMSZk",
      },
      "c2": {
        "title": "LRN Mock Interview for Level C2",
        "subtitle": "A full LRN speaking test practice. For all speaking sample tests please follow the link: lrnhellas.gr",
        "link": "https://www.youtube.com/embed/WYhuXhIarJ4?feature=oembed",
        "directLink": "https://www.youtube.com/watch?v=WYhuXhIarJ4",
      }
    }
  },
  "registration": {
    "title": "REGISTRATION",
    "subtitle": "Registration for the Exam Period on 20th of January 2019 are open.",
    "duration": "<b>Registration Duration:</b><br /> 5th of November until 9th of December 2018.",
    "guide": {
      "title": "Guide for ONLINE Registrations 2018",
      "subtitle": "Complete your registration for the LRN Exam with just 3 steps",
      "text": "Download Guide"
    },
    "costsLrn": {
      "title": "LRN Fees <br /> for the January 2019 Examination",
      "costs": ['<b>LEVEL A1:</b> 70 €','<b>LEVEL A2:</b> 70 €','<b>LEVEL B1:</b> 70 €','<b>LEVEL B2:</b> 70 €','<b>LEVEL C1:</b> 80 €','<b>LEVEL C2:</b> 100 €']
    },
    "nameChange": {
      "title": "Name Corrections",
      "text1": "Mistakes involving the First Name and Last Name are only accepted only if an ID or Passport has been provided.",
      "text2": "Mistakes involving the father/'s name, in the case that it is not shown on the candidate's passport, and in the case that the father's ID or Passport has not been provided, it will not be accepted.",
      "text3": "In order to correct an error in the applicant's name, a re-issuance fee of GBP50, plus correspondence and handling costs, applies."
    }
  }
}

export default text