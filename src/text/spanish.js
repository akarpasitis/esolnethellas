const text = {
  "header": {
    "linkCompany": "EMPRESA",
    "linkFaq": "PREGUNTAS MÁS FRECUENTES",
    "emailLabel": "Contáctenos:",
    "phone": "+34-675-59-53-79 / +357-96-643041",
    "emailAddress": "info@esolnetespana.es",
    "registrationInfo": "Información de Registro",
    "language": "Idioma: ",
    "languages": ['griego', 'español', 'inglés'],
    "createAccount": "Δημιουργία Λογαριασμού / Είσοδος",
    "buttons": {
      "lrn": "LRN EXÁMENES",
      "material": "MATERIAL DE APOYO",
      "news": "NOTICIAS",
      "contact": "COMUNICACIÓN"
    },
    "lrnButtons": ['LRN B1 Nivel', 'LRN B2 Nivel', 'LRN C1 Nivel', 'LRN C2 Nivel'],
    "materialButtons": ['Exámenes anteriores LRN', 'Videos simulados de la entrevista']    
  },
  "homeText": {
    "description": "- LRN - es una organización de adjudicación que ofrece calificaciones a candidatos, institutos educativos, proveedores de capacitación, escuelas y empleadores que pueden acceder a las calificaciones ya sea a través de instituciones educativas registradas. Fue fundado por un grupo de educadores y personas de negocios y se especializa en ESOL y calificaciones de gestión.",
    "title": "¿Por qué exámenes de LRN?",
    "more": "Aprende más...",
    "carousel": [
      "Learning Resource Network",
      "Exámenes LRN el 20 de enero de 2019.",
      "Registros del 5 de novembre al 9 de diciembre."
    ],
    "sections": [
      {
          "title": "INTELIGENTES",
          "bullets": [
            {
              "main": "Base en todos los niveles de 50%.",
              "sub1": null
            },
            {
              "main": "TODAS las HABILIDADES se compensan sin condiciones.",
              "sub1": null
            },
            {
              "main": "REVISIÓN GRATIS",
              "sub1": null
            }
          ]
      },
      {
        "title": "INNOVADORES",
        "bullets": [
          {
            "main": "El escuchar es simple y suena 2 veces.",
            "sub1": "Innovación de LRN: La Sección 3 de la escucha también suena por la tercera vez."
          },
          {
            "main": "Innovación de LRN: El Tema de la Sección 3 de la escucha, es similar al tema de la Tarea 1 de la sección 1 de escritura."
          }
        ]
      },
      {
          "title": "COMODOS",
          "bullets": [
            {
              "main": "Exámenes asequibles: dan oportunidades, sin trampas, sin peculiaridades.",
              "sub1": null
            },
            {
              "main": "El objetivo es reducir el estrés de la persona examinada y alcanzar su máximo rendimiento.",
              "sub1": null
            },
            {
              "main": "Ideal para estudiantes con problemas de aprendizaje.",
              "sub1": null
            }
          ]
      }
    ],
    recognition: 'Reconocimientos',    
    bodies: [
      {
        title: '<b>ALTE:</b> Asociación de Evaluadores de Idiomas en Europa.',
        text1: 'ALTE es una asociación de proveedores de exámenes de idiomas que trabajan juntos para promover una evaluación justa y precisa de la capacidad lingüística en toda Europa y más allá.',
        text2: 'Fue fundado en 1989 por la Universidad de Cambridge (Reino Unido) y la Universidad de Salamanca (España). Hoy, ALTE tiene 33 miembros plenos que representan 25 idiomas europeos, así como 58 afiliados institucionales y más de 500 afiliados individuales de todo el mundo. ALTE es una organización sin fines de lucro, y tiene estado participativo como una ONG con el Consejo de Europa.'
      },
      {
        title: '<b>UCAS:</b> El Servicio de Admisión para Universidades y Colegios.',
        text1: 'Una organización con sede en el Reino Unido cuya función principal es gestionar el proceso de solicitud para universidades británicas.',
        text2: ''
      },
      {
        title: '<b>OFQUAL:</b> Órgano regulador que supervisa exámenes y calificaciones escolares.',
        text1: 'Un departamento gubernamental no ministerial que regula las calificaciones, los exámenes y las pruebas en Inglaterra y, hasta mayo de 2016, las cualificaciones profesionales en Irlanda del Norte. Coloquialmente y públicamente, Ofqual a menudo se conoce como el examen "órgano de control".',
        text2: ''
      }
    ]
  },
  "contact": {
    "text": "Contáctenos para más información.",
    "addressLabel": "Dirección",
    "address": "Thesallonikis 46, Flat 1, 6035, Larnaca, Cyprus",
    "scheduleLabel": "Hora del centro de llamadas",
    "schedule": "Lunes – Viernes 9:00 – 20:00",
    "phoneLabel": "Número de teléfono",
    "phone": "ES: +34 675 595 379 <br /> CY: +357 96 643041 <br /> UK: +44 7446 069628",
    "phone": "ES: +34 675 595 379 <br /> CY: +357 96 643041 <br /> UK: +44 7446 069628",
    "emailLabel": "Correo Electrónico",
    "email": "info@esolnetespana.es"
  },
  "footer": {
    "button1": "EMPRESA",
    "button2": "PREGUNTAS MÁS FRECUENTES",
    "button3": "Información de Registro",
    "button4": "POLITICA DE PRIVACIDAD",
    "phoneLabel": "Tel.",
    "telephone": "ES: +34 675 595 379 / CY: +357 96 643041 / UK: +44 7446 069628",
    "emailLabel": "Email",
    "emailAddress": "info@esolnetespana.es",
    "address": "Thesallonikis 46, Flat 1, 6035, Larnaca, Cyprus",
    "schedule": "Hora del centro de llamadas: Lunes – Viernes 9:00 – 20:00",
    "copyright": "© 2017 Esolnet Hellas."
  },
  "cookieNotice": {
    "text": "Utilizamos cookies de terceros para mejorar nuestros servicios. Puede cambiar la configuración u obtener más información en nuestra política de cookies aqui: ",
    "link": "política de cookies",
    "close": "Aceptar"
  },
  "lrnExams": {
    "title1": "¿POR QUÉ LRN?",
    "title2": "PORQUE LOS EXAMENES LRN:",
    "subtitle1": "¿Te preocupa el rendimiento de tus alumnos?",
    "subtitle2": "En los exámenes de LRN, sus estudiantes tienen la mayor tasa de éxito. Regístrelos con confianza.",
    "bullets": [
      {
        "main": "Son reconocidos por Ofqual.",
        "sub1": null
      },
      {
        "main": "Son accesibles, sin trampas, sin características especiales, SIN ESTRES.",
        "sub1": null
      },
      {
        "main": "Base en todos los niveles de 50%.",
        "sub1": null
      },
      {
        "main": "Todas las HABILIDADES se suman sin condiciones.",
        "sub1": null
      },
      {
        "main": "El escuchar es simple y suena 2 veces.",
        "sub1": "Innovación: La tercera sección de la escucha se escucha por tercera vez a TODOS los niveles.",
        "sub2": "Innovación: La tarea 1 de la Sección de escritura 1 tiene el mismo tema que la sección de escucha 3."
      },
      {
        "main": "Las tareas de escritura se acompañan de ideas.",
        "sub1": null
      },
      {
        "main": "Las 2 tareas de escritura, escritas por los candidatos, favorecen un mejor rendimiento y calificación.",
        "sub1": null
      },
      {
        "main": "Los candidatos muestran su mejor rendimiento, generalmente en escuchar, escribir, hablar.",
        "sub1": null
      },
      {
        "main": "La lectura y el uso se suman a un nivel común.",
        "sub1": null
      },
      {
        "main": "Favorables también para el nivel C2.",
        "sub1": null
      },
      {
        "main": "Son simples y llevan a los estudiantes más fácilmente al éxito.",
        "sub1": null
      },
      {
        "main": "El periodo para los exámenes es sin interrumpir y es más que adecuado.",
        "sub1": null
      },
      {
        "main": "Los candidatos de TODOS los niveles se examinan en todas las habilidades del lenguaje escrito y hablado.",
        "sub1": null
      },
      {
        "main": "Ideal también para Candidatos con Dificultades de Aprendizaje.",
        "sub1": null
      },
      {
        "main": "Innovación: hemos establecido la REXAMINACION GRATUITA.",
        "sub1": null
      },
      {
        "main": "Los exámenes suceden en enero y junio.",
        "sub1": null
      },
      {
        "main": "Excelente material de apoyo GRATUITO.",
        "sub1": null
      },
      {
        "main": "Innovación: hemos colocado un Consejero Personalizado para obtener asistencia y comunicación continua para los exámenes.",
        "sub1": null
      },
      {
        "main": "Los Institutos de Idiomas Extranjeros no necesitan escribir las direcciones de sus estudiantes en los formularios de inscripción ni dar información personal ni profesional.",
        "sub1": null
      }
    ],
    "examsButtons": ['Escuchar','Escribir','Lectura y uso','Hablar'],
    "exams": [
      {
        "name": "LRN B1",
        "price": "70€",
        "text": "El <b> EXAMEN DE LRN </b> es  <b> cohesivo </b>. Se realiza sin ninguna interrupción y los candidatos tienen la oportunidad de administrar su tiempo, regresar a una parte del examen, corregir o completar lo que se necesita.",
        "duration": {
          "title": "Duración del examen: ",
          "text": "2 horas y 30 minutos."
        },
        "tableTitle": "El <b> EXAMEN DE LRN </b> consiste en:",
        "table": {
          "rows": [
            [" ","Habilidades","Partes"],
            ["1.","Escuchar ","3 Secciones"],
            ["2.","Escribir","2 Secciones"],
            ["3.","Lectura y uso","4 Secciones"],
            ["4.","Hablar","4 Secciones"]                            
          ]
        },
        "listening": {
          "title": "El <b>EXAMEN LRN</b> de <b>escucha</b> consiste en:",
          "section1": {
            "title": "Sección de Escucha 1: (9 puntos)",
            "bullets": ["Suena dos veces","9 pequeños diálogos, 9 preguntas","1 punto cada pregunta"]
          },
          "section2": {
            "title": "Sección de Escucha 2: (6 puntos)",
            "bullets": ["Suena dos veces","3 diálogos poco más largos","Dos preguntas para cada diálogo","1 punto cada pregunta"]
          },
          "section3": {
            "title": "Sección de Escucha 3: (5 puntos)",
            "bullets": ["Suena dos veces","1 texto con tema Carta Amistosa","5 preguntas","1 punto cada pregunta"]
          },
          "hint": {
            "title": "<b>Innovación: </b>",
            "text": "en todos los niveles, la Sección 3 de escucha suena por tercera vez en conjunción con la Escritura, Sección 1, Tarea 1."
          }
        },
        "writing": {
          "title": "Le <b>escritura</b> en el <b>EXAMEN LRN</b> está incluida en todos los niveles de 2 secciones.",
          "text1": "En la <b>Sección 1 de Escritura, Tarea 1</b>, se <b>requiere</b> que los candidatos escriban el tema que escucharon en la Sección 3 de Escucha. <br/> El tema de la <b>sección 3 del escuchar</b> y la <b>Sección 1 del escribir, Tarea 1</b>, son una Carta Amistosa.",
          "text2": "En la <b> sección 2 de escritura </b>, hay <b>3 opciones.</b>",
          "bullets": ["Otra carta amistosa (acompañada de ideas)","Historia","Ensayo (acompañado de ideas)"],
          "text3": "Los 2 Escritos del EXAMEN LRN ofrecen una oportunidad para que los candidatos obtengan mejores puntajes.",
          "table": {
            "title": "Los 2 Escritos del EXAMEN LRN ofrecen una oportunidad para que los candidatos obtengan mejores puntajes.",
            "subtitle": "Número de palabras del EXAMEN de escritura",              
            "rows": [
              ["Niveles","1a Tarea","2a Tarea"],
              ["B1","90-110","110-150"]
            ]
          }
        },
        "reading": {
          "title": "La LECTURA Y USO del EXAMEN LRN están juntos y comparten el puntaje.",
          "table1": {
            "title": "La lectura consiste en:",
            "rows": [
              ["Sección 1:","1 texto, 8 preguntas (½ punto cada una)"],
              ["Sección 2:","2 textos cortos con un tema común"],
              [" ", "1er texto, 3 - 4 preguntas.<br />2º texto, 3-4 preguntas."],
              [" ", "Un total de 15 preguntas, ½ punto cada pregunta."]
            ]
          },
          "table2": {
            "title": "El USO consiste de:",
            "rows": [
              ["Sección 3:","115 preguntas de gramática de opción múltiple, 1/2 punto cada pregunta."],
              ["Sección 4:","1 texto de completar con 10 preguntas de opción múltiple, ½ punto cada pregunta."],
              [" ", " "],
              [" ","Puntaje total de Lectura y uso puntaje: 15 + 25 = 40: 2 = 20 puntos"]
            ]
          },
          "footer": "El resumen de Lectura y Uso favorece el mejor desempeño de los candidatos."
        },
        "speaking": {
          "title": "El HABLAR del EXAMEN LRN incluye: 2 Candidatos, 1 Examinador.",
          "duration": {
            "title": "Duración del hablar: ",
            "text": "14 - 16 minutos."
          },
          "bulletTitle": "Análisis de las 3 Secciones: ",
          "bullets": [
            {
              "heading": "Sección 1: ",
              "text": "Preguntas de calentamiento"
            },
            {
              "heading": "Sección 2: ",
              "text": "Los candidatos hablan sobre 1 de los 5 temas que han elegido y preparado."
            },
            {
              "heading": "Sección 3: ",
              "text": "Los candidatos responden a una pregunta relacionada con la Sección 2; se les dan puntos para que usen."
            }
          ],
          "text1": "La sección 2 de hablar del EXAMEN LRN se envía para su preparación 2 semanas antes del examen oral. Requiere la preparación de 1 de los 5 artículos enviados.",
          "text2": "Los dos candidatos examinados en hablar pueden haber elegido el mismo tema.",
          "text3": "Innovación: El Plan de estudios de hablar para los exámenes de enero y junio se da desde el comienzo del año escolar."
        }
      },
      {
        "name": "LRN B2",
        "price": "70€",
        "text": "El <b> EXAMEN DE LRN </b> es  <b> cohesivo </b>. Se realiza sin ninguna interrupción y los candidatos tienen la oportunidad de administrar su tiempo, regresar a una parte del examen, corregir o completar lo que se necesita.",
        "duration": {
          "title": "Duración del examen: ",
          "text": "2 horas y 30 minutos."
        },
        "tableTitle": "El EXAMEN DE LRN consiste en: ",
        "table": {
          "rows": [
            [" ","Habilidades","Partes"],
            ["1.","Escuchar ","3 Secciones"],
            ["2.","Escribir","2 Secciones"],
            ["3.","Lectura y uso","4 Secciones"],
            ["4.","Hablar","3 Secciones"]                            
          ]
        },
        "listening": {
          "title": "El EXAMEN LRN de escucha consiste en: ",
          "section1": {
            "title": "Sección de Escucha 1: (9 puntos)",
            "bullets": ["Suena dos veces","9 pequeños diálogos, 9 preguntas","1 punto cada pregunta"]
          },
          "section2": {
            "title": "Sección de Escucha 2: (6 puntos)",
            "bullets": ["Suena dos veces","3 diálogos poco más largos","Dos preguntas para cada diálogo","1 punto cada pregunta"]
          },
          "section3": {
            "title": "Sección de Escucha 3: (5 puntos)",
            "bullets": ["Suena dos veces","1 texto con tema Carta Amistosa","5 preguntas","1 punto cada pregunta"]
          },
          "hint": {
            "title": "Innovación",
            "text": "en todos los niveles, la Sección 3 de escucha suena por tercera vez en conjunción con la Escritura, Sección 1, Tarea 1."
          }
        },
        "writing": {
          "title": "Le <b>escritura</b> en el <b>EXAMEN LRN</b> está incluida en todos los niveles de 2 secciones.",
          "text1": "En la <b>Sección 1 de Escritura, Tarea 1</b>, se <b>requiere</b> que los candidatos escriban el tema que escucharon en la Sección 3 de Escucha. <br/> El tema de la <b>sección 3 del escuchar</b> y la <b>Sección 1 del escribir, Tarea 1</b>, son una Carta Amistosa.",
          "text2": "En la <b> sección 2 de escritura </b>, hay 3 opciones.",
          "bullets": ["Otra carta amistosa (acompañada de ideas)","Historia","Ensayo (acompañado de ideas)"],
          "text3": "Los 2 Escritos del EXAMEN LRN ofrecen una oportunidad para que los candidatos obtengan mejores puntajes.",
          "table": {
            "title": "Número de palabras del EXAMEN de escritura",
            "rows": [
              ["Niveles","1a Tarea","2a Tarea"],
              ["B2","100-120","120-170"]
            ]
          }
        },
        "reading": {
          "title": "La LECTURA Y USO del EXAMEN LRN están juntos y comparten el puntaje.",
          "table1": {
            "title": "La lectura consiste en:",
            "rows": [
              ["Sección 1:","1 texto, 8 preguntas (½ punto cada una)"],
              ["Sección 2:","2 textos cortos con un tema común"],
              [" ", "1er texto, 3 - 4 preguntas. <br /> 2º texto, 3-4 preguntas."],
              [" ", "Un total de 15 preguntas, ½ punto cada pregunta."]
            ]
          },
          "table2": {
            "title": "El USO consiste de:",
            "rows": [
              ["Sección 3:","15 preguntas de gramática de opción múltiple, 1/2 punto cada pregunta."],
              ["Sección 4:","1 texto de completar con 10 preguntas de opción múltiple, ½ punto cada pregunta."],
              [" ", " "],
              [" ","Puntaje total de Lectura y uso puntaje: 15 + 25 = 40: 2 = 20 puntos"]
            ]
          },
          "footer": "El resumen de Lectura y Uso favorece el mejor desempeño de los candidatos."
        },
        "speaking": {
          "title": "El HABLAR del EXAMEN LRN incluye: 2 Candidatos, 1 Examinador.",
          "duration": {
            "title": "Duración del hablar: ",
            "text": "14 - 16 minutos."
          },
          "bulletTitle": "Análisis de las 3 Secciones: ",
          "bullets": [
            {
              "heading": "Sección 1: ",
              "text": "Preguntas de calentamiento"
            },
            {
              "heading": "Sección 2: ",
              "text": "los candidatos hablan sobre 1 de los 5 temas que han elegido y preparado."
            },
            {
              "heading": "Sección 3: ",
              "text": "los candidatos responden a una pregunta relacionada con la Sección 2; se les dan puntos para que usen."
            }
          ],
          "text1": "TLa sección 2 de hablar del EXAMEN LRN se envía para su preparación 2 semanas antes del examen oral. Requiere la preparación de 1 de los 5 artículos enviados.",
          "text2": "Los dos candidatos examinados en hablar pueden haber elegido el mismo tema.",
          "text3": "Innovación: El Plan de estudios de hablar para los exámenes de enero y junio se da desde el comienzo del año escolar."
        }
      },
      {
        "name": "LRN C1",
        "price": "80€",
        "text": "El <b> EXAMEN DE LRN </b> es  <b> cohesivo </b>. Se realiza sin ninguna interrupción y los candidatos tienen la oportunidad de administrar su tiempo, regresar a una parte del examen, corregir o completar lo que se necesita.",
        "duration": {
          "title": "Duración del examen: ",
          "text": "3 horas."
        },
        "tableTitle": "El EXAMEN DE LRN consiste en: ",
        "table": {
          "rows": [
            [" ","Habilidades","Partes"],
            ["1.","Escuchar ","3 Secciones"],
            ["2.","Escribir","2 Secciones"],
            ["3.","Lectura y uso","4 Secciones"],
            ["4.","Hablar","3 Secciones"]                            
          ]
        },
        "listening": {
          "title": "El examen de escucha de LRN consiste en: ",
          "section1": {
            "title": "Sección de escucha 1: (10 puntos)",
            "bullets": ["Suena dos veces","10 pequeños diálogos, 10 preguntas","1 punto cada pregunta"]
          },
          "section2": {
            "title": "Sección de escucha 2: (10 puntos)",
            "bullets": ["Suena dos veces","3 diálogos poco más largos","10 preguntas","Cada diálogo 2 - 4 preguntas","1 punto cada pregunta"]
          },
          "section3": {
            "title": "Sección de escucha 3: (5 puntos)",
            "bullets": ["Suena 3 veces","1 texto de ensayo","5 preguntas","1 punto cada pregunta"]
          },
          "hint": {
            "title": "Innovación",
            "text": "en todos los niveles, la sección 3 de escuchar se escucha por tercera vez junto con Escritura, Sección 1, Tarea 1."
          }
        },
        "writing": {
          "title": "El ESCRIBIR del EXAMEN LRN consta de TODOS los niveles de 2 secciones.",
          "text1": "En la Sección 1 del Escribir, Tarea 1, los candidatos deben escribir el tema que escucharon en la Sección 3 de Escuchar. ",
          "text2": "El tema de la Sección 3 del Escuchar y Sección 1 del escribir, Tarea 1 es un Ensayo. En la Sección 2 de Escribir hay 3 opciones de 3 ensayos acompañados de ideas.",
          "bullets": [],
          "text3": "Los 2 Escritos del EXAMEN LRN ofrecen una oportunidad para que los candidatos obtengan mejor puntaje.",
          "table": {
            "title": "Número de palabras del EXAMEN de escribir",
            "rows": [
              ["Niveles","1ª Tarea","2ª Tarea"],
              ["C1","150-200","250-300"]
            ]
          }
        },
        "reading": {
          "title": "La LECTURA Y USO del EXAMEN LRN están juntos y comparten el puntaje.",
          "table1": {
            "title": "La lectura consiste en:",
            "rows": [
              ["Sección 1:","1 texto, 9 preguntas (½ punto cada una)"],
              ["Sección 2:","2 textos cortos con un tema común"],
              [" ", "1er texto, 3 - 4 preguntas. <br /> 2º texto, 3-4 preguntas. <br /> Finalmente, 2-3 preguntas para ambos textos."],
              [" ", "Un total de 20 preguntas, cada pregunta obtiene ½ punto."]
            ]
          },
          "table2": {
            "title": "El USO consiste en:",
            "rows": [
              ["Sección 3:","20 preguntas de gramática de opción múltiple, 1/2 punto cada pregunta."],
              ["Sección 4:","1 texto para completar con 10 espacios, ½ punto cada pregunta."],
              [" ", " "],
              [" ","Puntuación total, lectura y uso: 20 + 30 = 50: 2 = 25 puntos"]
            ]
          },
          "footer": "El cálculo del promedio de Lectura y Uso favorece el mejor desempeño de los candidatos."
        },
        "speaking": {
          "title": "El hablar del EXAMEN LRN incluye: 2 Candidatos, 1 Examinador.",
          "duration": {
            "title": "Duración del hablar: ",
            "text": "16 – 18 minutos."
          },
          "bulletTitle": "Análisis de las 3 Secciones: ",
          "bullets": [
            {
              "heading": "Sección 1: ",
              "text": "Preguntas de calentamiento"
            },
            {
              "heading": "Sección 2: ",
              "text": "Los candidatos hablan sobre 1 de los 5 temas que han elegido y preparado."
            },
            {
              "heading": "Sección 3: ",
              "text": "Los candidatos responden a una pregunta relacionada con la Sección 2; se les dan puntos para que usen."
            }
          ],
          "text1": "La sección 2 de hablar del EXAMEN LRN se envía para su preparación 2 semanas antes del examen oral. Requiere la preparación de 1 de los 5 artículos enviados.",
          "text2": "Los dos candidatos examinados en hablar pueden haber elegido el mismo tema.",
          "text3": "Innovación: El Plan de estudios de hablar para los exámenes de enero y junio se da desde el comienzo del año escolar."
        }
      },
      {
        "name": "LRN C2",
        "price": "100€",
        "text": "El <b> EXAMEN DE LRN </b> es  <b> cohesivo </b>. Se realiza sin ninguna interrupción y los candidatos tienen la oportunidad de administrar su tiempo, regresar a una parte del examen, corregir o completar lo que se necesita.",
        "duration": {
          "title": "Duración del examen: ",
          "text": "3 horas."
        },
        "tableTitle": "El EXAMEN DE LRN consiste en: ",
        "table": {
          "rows": [
            [" ","Habilidades","Partes"],
            ["1.","Escuchar ","3 Secciones"],
            ["2.","Escribir","2 Secciones"],
            ["3.","Lectura y uso","4 Secciones"],
            ["4.","Hablar","3 Secciones"]                            
          ]
        },
        "listening": {
          "title": "El examen de escucha de LRN consiste en:",
          "section1": {
            "title": "Sección de escucha 1: (10 puntos)",
            "bullets": ["Suena dos veces","10 pequeños diálogos, 10 preguntas","1 punto cada pregunta"]
          },
          "section2": {
            "title": "Sección de escucha 2: (10 puntos)",
            "bullets": ["Suena dos veces","3 diálogos poco más largos","10 preguntas","Cada diálogo 2 - 4 preguntas","1 punto cada pregunta"]
          },
          "section3": {
            "title": "Sección de escucha 3: (5 puntos)",
            "bullets": ["Suena 3 veces","1 texto de ensayo","5 preguntas","1 punto cada pregunta"]
          },
          "hint": {
            "title": "Innovación",
            "text": "en todos los niveles, la sección 3 de escuchar se escucha por tercera vez junto con Escritura, Sección 1, Tarea 1."
          }
        },
        "writing": {
          "title": "El ESCRIBIR del EXAMEN LRN consta de TODOS los niveles de 2 secciones.",
          "text1": "En la Sección 1 del Escribir, Tarea 1, los candidatos deben escribir el tema que escucharon en la Sección 3 de Escuchar.",
          "text2": "El tema de la Sección 3 del Escuchar y Sección 1 del escribir, Tarea 1 es un Ensayo. En la Sección 2 de Escribir hay 3 opciones de 3 ensayos acompañados de ideas.",
          "bullets": [],
          "text3": "Los 2 Escritos del EXAMEN LRN ofrecen una oportunidad para que los candidatos obtengan mejor puntaje.",
          "table": {
            "title": "Número de palabras del EXAMEN de escribir",
            "rows": [
              ["Niveles","1ª Tarea","2ª Tarea"],
              ["C2","200-250","250-300"]
            ]
          }
        },
        "reading": {
          "title": "La LECTURA Y USO del EXAMEN LRN están juntos y comparten el puntaje.",
          "table1": {
            "title": "La lectura consiste en:",
            "rows": [
              ["Sección 1:","1 texto, 9 preguntas (½ punto cada una)"],
              ["Sección 2:","2 textos cortos con un tema común"],
              [" ", "1er texto, 3 - 4 preguntas. <br /> 2º texto, 3-4 preguntas. <br /> Finalmente, 2-3 preguntas para ambos textos."],
              [" ", "Un total de 20 preguntas, cada pregunta obtiene ½ punto."]
            ]
          },
          "table2": {
            "title": "El USO consiste en:",
            "rows": [
              ["Sección 3:","20 preguntas de gramática de opción múltiple, 1/2 punto cada pregunta."],
              ["Sección 4:","1 texto para completar con 10 espacios, ½ punto cada pregunta."],
              [" ", " "],
              [" ","Puntuación total, lectura y uso: 20 + 30 = 50: 2 = 25 puntos"]
            ]
          },
          "footer": "El cálculo del promedio de Lectura y Uso favorece el mejor desempeño de los candidatos."
        },
        "speaking": {
          "title": "El hablar del EXAMEN LRN incluye: 2 Candidatos, 1 Examinador.",
          "duration": {
            "title": "Duración del hablar: ",
            "text": "16 – 18 minutos."
          },
          "bulletTitle": "Análisis de las 3 Secciones: ",
          "bullets": [
            {
              "heading": "Sección 1: ",
              "text": "Preguntas de calentamiento"
            },
            {
              "heading": "Sección 2: ",
              "text": "Los candidatos hablan sobre 1 de los 5 temas que han elegido y preparado."
            },
            {
              "heading": "Sección 3: ",
              "text": "Los candidatos responden a una pregunta relacionada con la Sección 2; se les dan puntos para que usen."
            }
          ],
          "text1": "La sección 2 de hablar del EXAMEN LRN se envía para su preparación 2 semanas antes del examen oral. Requiere la preparación de 1 de los 5 artículos enviados.",
          "text2": "Los dos candidatos examinados en hablar pueden haber elegido el mismo tema.",
          "text3": "Innovación: El Plan de estudios de hablar para los exámenes de enero y junio se da desde el comienzo del año escolar."
        }
      }
    ]
  },
  "faqs": {
    "exams": [
      {
        'q': '¿Cuándo salen los resultados?',
        'a': 'Los resultados del examen salen aproximadamente 10 semanas después del día de los exámenes.'
      },
      {
        'q': '¿Cuáles son los centros de prueba?',
        'a': 'Póngase en contacto con nosotros para informarle sobre el centro de pruebas más cercano en su área.'
      },
      {
        'q': '¿Cuándo se envían los boletines de los candidatos?',
        'a': 'Los boletines están disponibles 2 semanas antes de los exámenes.'
      },
      {
        'q': '¿Cuál es la fecha del examen?',
        'a': 'Los períodos de prueba de 2019 son: 20 de enero.'
      },
      {
        'q': '¿Cuándo se administran las preparaciones orales?',
        'a': 'LRN Sección 2 Habla, está disponible para los candidatos unas 2 semanas antes de los exámenes. Innovación LRN: el programa de enseñanza oral es desde el comienzo del año escolar.'
      },
      {
        'q': '¿Qué pasa si no lo haces?',
        'a': 'En el caso de que un Candidato se ausente debido a una enfermedad o mala conducta, los reembolsos no se reembolsan al examinador, pero el monto se acredita para el próximo Examen. Debe contactarnos para una nueva aplicación.'
      },
      {
        'q': '¿Qué pasa con los candidatos con dificultades de aprendizaje?',
        'a': 'Los candidatos con dificultades de aprendizaje deben enviar cualquier opinión disponible registrándose. Desde este año, la traducción del informe ha sido hecha por Esolnet Hellas. Innovación: Te ayudamos a evaluar a tus candidatos con dificultades de aprendizaje en la escritura. Envíenos muestras de Escritura y se le devolverán comentarios sobre su desempeño. Estas muestras deben escribirse en pluma, en papel de competición y tener párrafos que sean claramente distinguibles.'
      }
    ],
    "registration": [
      {
        'q': '¿Cuáles son las fechas para las fechas límites para la Aplicación?',
        'a': 'Por favor, consulte la página actual de actualización del período de examen.'
      },
      {
        'q': '¿Cuál es la tarifa de los exámenes LRN?',
        'a': 'NIVEL A1: 70€<br /> NIVEL A2: 70€<br />NIVEL B1: 70€<br /> NIVEL B2: 70€<br /> NIVEL C1: 80€<br /> NIVEL C2: 100€'
      },
      {
        'q': 'CUENTAS BANCARIAS',
        'a': ''
      }
    ],
    "material": [
      {
        'q': '¿Cuál es la bibliografía disponible?',
        'a': 'Las publicaciones SUPERCOUSE tienen libros para los niveles B2 y C2. <br />Las publicaciones de BETSIS tienen los libros B1, B2, C1 y C2.'
      },
      {
        'q': '¿Dónde encontraré material de apoyo disponible?',
        'a': 'Se puede encontrar material adicional disponible en la página de Materiales de soporte. <br /> Para la preparación de los candidatos, el material adicional para los exámenes LRN también se envía electrónicamente.'
      }
    ],
    'retake': [
      {
        'q': 'En caso de que se realice un nuevo examen, ¿debo volver a presentar la solicitud?',
        'a': 'Debe hacerse una nueva solicitud con todos los detalles del Candidato y una nota a pie de página que es un reexamen.'
      },
    ],
    'recognizition': [
      {
        'q': '¿Cuál es el reconocimiento de los exámenes en el extranjero?',
        'a': 'Los exámenes LRN son reconocidos por Ofqual, ALTE, UCAS.'
      }
    ]
  },
  "faqsTitles": ['1. Exámenes', '2. Registros','3. Material','4. Reexaminación gratuita','5. Reconocimiento'],
  "company": {
    "headerTitle": 'ESOLNET',
    "headerSubtitle": 'Certificación de inglés y habilidades profesionales',
    "title1": 'Profesionales <br /> en el área de certificación',
    "text1": [
      'ESOLNET fue fundada en Chipre por profesionales en el campo de la Certificación del Idioma Inglés y la Certificación de Habilidades Profesionales. El CEO de ESOLNET, el Sr. George Antoniadis, es muy conocido en la Educación Privada de Grecia por sus éxitos en colaborar con las principales Universidades Británicas y Organismos de Certificación con Colegios Privados Griegos y Grandes Organizaciones de Educación.',
      'En los 25 años que ESOLNET ha estado activa en Grecia, el Sr. Giorgos Antoniadis ha sido galardonado ocasionalmente con Órganos de Adjudicación y Universidades que realizan reconocimientos tales como: Universidad de East London en colaboración con Anglia, Universidad de Central Lancashire (UCLAN), ESB, OCNW (más tarde Ascentis) en colaboración con Anglia, Ascentis, City y Guilds, LCCI (posterior EDI), NCFE, TQUK, Premio a la Trayectoria, Premios ABC, Anglia y otros.',
      'Universidades como la Universidad de East London y la Universidad de Central Lancashire, con la participación de ESOLNET, han introducido cursos universitarios y cooperan con franquicias con importantes organizaciones educativas privadas.',
      'ESOLNET también participa en la presentación de estudiantes griegos y chipriotas en las universidades británicas. Con la ayuda de ESOLNET, un gran número de estudiantes han sido admitidos en las universidades de Essex, Derby, Greenwich, Londres Este, Lancashire Central, Lancaster, East Anglia, Winchester, Bucks, Londres y más.',
      'ESOLNET presenta los Exámenes al Órgano de Adjudicación LRN (Learning Resource Network), las que representan. LRN es una organización para la Certificación de Inglés y la Certificación de Habilidades Profesionales. Es reconocida por Ofqual (organismo de certificación británico) y ASEP (organización correspondiente en Grecia) y es miembro de ALTE (Asociación Europea de Organismos de Evaluación de Lenguas Extranjeras), al que pertenecen todos los principales organismos de certificación de lenguas extranjeras.',
      'LRN se destaca porque, después de una investigación exhaustiva, ha creado exámenes que le dan prioridad al estudiante moderno. LRN ha tenido en cuenta todas las preocupaciones de los profesores de inglés y, en particular, los ppropietarios de institutos de idiomas extranjeras y es capaz de ofrecer exámenes adaptados a las necesidades de los candidatos europeos en los tiempos modernos. Los exámenes LRN también están disponibles para candidatos con dificultades de aprendizaje, sin trampas.',
      'ESOLNET HELLAS está representada y respaldada por practicantes de idiomas extranjeros con experiencia que actúan como examinadores personales y promueven declaraciones electrónicas de LRN en cada región de Grecia.',
      'El propósito del Consultor de Exámenes Personales de ESOLNET es apoyar ESENCIALMENTE a los Propietarios de escuelas y Maestros para recompensarse a sí mismos y a sus Candidatos por el esfuerzo y el tiempo que han invertido en la preparación de los Exámenes.'
    ],
    "title2": 'Red de Recursos de Aprendizaje (LRN)',
    "subtitle2": 'ESOL INTERNATIONAL QUALIFICATIONS',
    "text2": [
      '- LRN - es una organización de adjudicación que ofrece calificaciones a candidatos, institutos educativos, proveedores de capacitación, escuelas y empleadores que pueden acceder a las calificaciones ya sea a través de instituciones educativas registradas. <br /> Fue fundado por un grupo de educadores y personas de negocios y se especializa en ESOL y calificaciones de gestión. La sede central de LRN en Londres cuenta con el respaldo de un equipo de representantes en todo el mundo.',
      'Las cualificaciones de ESOL International están diseñadas para candidatos que no son hablantes nativos de inglés y desean obtener una calificación de alta calidad reconocida internacionalmente en inglés que esté disponible y sea reconocida en todo el mundo y cubra toda la gama (NQF Registro 3 / CEF B1), (NQF nivel 1 / CEF B2), (NQF nivel 2 / CEF C1), (NQF nivel 3 / CEF C2). Son adecuados para candidatos que se preparan para ingresar a la educación superior o al empleo profesional en el Reino Unido o en cualquier otro lugar. Las cualificaciones de ESOL International están diseñadas para hacer referencia a las descripciones de competencia lingüística en el Marco Común Europeo de Referencia para las Lenguas (CEF). Los niveles en el CEF se han asignado a los niveles en el Marco Nacional de Cualificaciones para Inglaterra, Gales e Irlanda del Norte.'
    ],
    "footerTitle": "Síguenos",
    "footerText": 'Síguenos en las redes sociales y obtén actualizaciones instantáneas.'
  },
  "material": {
    "title1": "EXAMENES DE AÑOS ANTERIORES",
    "sections1": ['EXAMENES DE AÑOS ANTERIORES LRN C2','EXAMENES DE AÑOS ANTERIORES LRN C1','EXAMENES DE AÑOS ANTERIORES LRN B2','EXAMENES DE AÑOS ANTERIORES LRN B1'],
    "title2": "VIDEO SIMULADO DE LA ENTREVISTA",
    "sections2": ['LRN simulacros de entrevista de Nivel C2','LRN simulacros de entrevista de Nivel B2'],
    "pastPapers": {
      "b1": {
        "title": "LRN ENTRY CERTIFICADO DE NIVEL DE ENTRADA EN ESOL INTERNATIONAL (ENTRY 3) (CEF B1)",
        "section1": {
          "title": "LRN B1 ENERO 2016",
          "pastPapers": [
            {
              "text": "LRN Nivel B1 enero 2016 Exam Paper",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B1-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Nivel B1 enero 2016 Speaking Examiner Instructions",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B1-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Nivel B1 enero 2016 Answer Key",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B1-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B1 enero 2016 Escuchar Sección 1",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B1-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN B1 enero 2016 Escuchar Sección 2",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B1-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN B1 enero 2016 Escuchar Sección 3",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B1-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Nivel B1 Espécimen Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B1-Certificate-in-ESOL-International-Entry-3-CEF-B1-Listening-Writing-Reading-and-Use_Sample_paper.pdf"
            },
            {
              "title": "LRN Nivel B1 Espécimen Hablar Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Level-B1-Certificate-in-ESOL-International-Entry-3-CEF-B1-Speaking_Sample_paper.pdf.pdf"
            },
            {
              "title": "LRN Nivel B1 Espécimen Escribir Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-Entry-Level-Certificate-in-ESOL-International-Entry-3-CEF-B1_WRITING_BOOKLET_Sample_paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B1 Escuchar Sección 1",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B1__Listening_Section1.mp3"
            },
            {
              "text": "LRN B1 Escuchar Sección 2",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B1__Listening_Section2.mp3"
            },
            {
              "text": "LRN B1 Escuchar Sección 3",
              "link": "https://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B1__Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for B1 B2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-B1-B2.pdf"
        }
      },
      "b2": {
        "title": "LRN NIVEL 1 CERTIFICADO EN ESOL INTERNATIONAL (CEF B2)",
        "section1": {
          "title": "LRN B2 ENERO 2016",
          "pastPapers": [
            {
              "text": "LRN Nivel B2 enero 2016 Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B2-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Nivel B2 enero 2016 Speaking Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B2-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Nivel B2 enero 2016 Answer Key",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-B2-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B2 enero 2016 Escuchar Sección 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B2-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN B2 enero 2016 Escuchar Sección 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B2-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN B2 enero 2016 Escuchar Sección 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-B2-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Nivel B2 Espécimen Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-_LEVEL-1-CERTIFICATE-IN-ESOL-INTERNATIONAL-CEF-B2_LISTENING-WRITING-READING-AND-USE_Sample_Paper.pdf"
            },
            {
              "title": "LRN Nivel B2 Espécimen Hablar Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-_LEVEL-1-CERTIFICATE-IN-ESOL-INTERNATIONAL-CEF-B2_SPEAKING_Sample_Paper.pdf"
            },
            {
              "title": "LRN Nivel B2 Espécimen Escribir Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-_LEVEL-2-CERTIFICATE-IN-ESOL-INTERNATIONAL-CEF-B2_WRITING_BOOKLET_Sample_Paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN B2 Escuchar Sección 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B2_Listening_Section2.mp3"
            },
            {
              "text": "LRN B2 Escuchar Sección 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B2_Listening_Section2.mp3"
            },
            {
              "text": "LRN B2 Escuchar Sección 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_B2_Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for B1 B2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-B1-B2.pdf"
        }
      },
      "c1": {
        "title": "LRN NIVEL 1 CERTIFICADO EN ESOL INTERNATIONAL (CEF C1)",
        "section1": {
          "title": "LRN C1 ENERO 2016",
          "pastPapers": [
            {
              "text": "LRN Nivel C1 Enero 2016 Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C1-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Nivel C1 Enero 2016 Hablar Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C1-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Nivel C1 Enero 2016 Answer Key",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C1-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C1 Enero 2016 Escuchar Sección 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C1-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN C1 Enero 2016 Escuchar Sección 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C1-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN C1 Enero 2016 Escuchar Sección 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C1-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Nivel C1 Espécimen Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-_LEVEL-2-CERTIFICATE-IN-ESOL-INTERNATIONAL-CEF-C1_LISTENING-WRITING-READING-AND-USE_Sample_Paper-1.pdf"
            },
            {
              "title": "LRN Nivel C1 Espécimen Hablar Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-_LEVEL-2-CERTIFICATE-IN-ESOL-INTERNATIONAL-CEF-C1_SPEAKING_Sample_Paper-1.pdf"
            },
            {
              "title": "LRN Nivel C1 Espécimen Escribir Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-_LEVEL-2-CERTIFICATE-IN-ESOL-INTERNATIONAL-CEF-C1_WRITING_BOOKLET_Sample_Paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C1 Escuchar Sección 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C1_Listening_Section1.mp3"
            },
            {
              "text": "LRN C1 Escuchar Sección 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C1_Listening_Section2.mp3"
            },
            {
              "text": "LRN C1 Escuchar Sección 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C1_Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for C1 C2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-C1-C2.pdf"
        }
      },
      "c2": {
        "title": "LRN NIVEL 1 CERTIFICADO EN ESOL INTERNATIONAL (CEF C2)",
        "section1": {
          "title": "LRN C2 Enero 2016",
          "pastPapers": [
            {
              "text": "LRN Nivel C2 Enero 2016 Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C2-January-2016-Exam-Paper.pdf"
            },
            {
              "text": "LRN Nivel C2 Enero 2016 Hablar Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C2-January-2016-Speaking-Examiner-Instructions.pdf"
            },
            {
              "text": "LRN Nivel C2 Enero 2016 Answer Key",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-Level-C2-January-2016-Answer-Key.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C2 Enero 2016 Escuchar Sección 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C2-January-2016-Listening-Part-1.mp3"
            },
            {
              "text": "LRN C2 Enero 2016 Escuchar Sección 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C2-January-2016-Listening-Part-2.mp3"
            },
            {
              "text": "LRN C2 Enero 2016 Escuchar Sección 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-C2-January-2016-Listening-Part-3.mp3"
            }
          ]
        },
        "section2": {
          "title": "SAMPLE PAPER",
          "samplePapers": [
            {
              "title": "LRN Nivel C2 Espécimen Exam Paper",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-_LEVEL-3-CERTIFICATE-IN-ESOL-INTERNATIONAL-CEF-C2_LISTENING-WRITING-READING-AND-USE_Sample_Paper.pdf"
            },
            {
              "title": "LRN Nivel C2 Espécimen Hablar Examiner Instructions",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN-_LEVEL-3-CERTIFICATE-IN-ESOL-INTERNATIONAL-CEF-C2_SPEAKING_Sample_Paper.pdf"
            },
            {
              "title": "LRN Nivel C2 Espécimen Escribir Booklet",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/09/LRN-_LEVEL-3-CERTIFICATE-IN-ESOL-INTERNATIONAL-CEF-C2_WRITING_BOOKLET_Sample_Paper.pdf"
            }
          ],
          "listeningAudio": [
            {
              "text": "LRN C2 Escuchar Sección 1",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C2_Listening_Section1.mp3"
            },
            {
              "text": "LRN C2 Escuchar Sección 2",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C2_Listening_Section2.mp3"
            },
            {
              "text": "LRN C2 Escuchar Sección 3",
              "link": "http://esolnethellas.gr/wp-content/uploads/2016/07/LRN_C2_Listening_Section3.mp3"
            }
          ]
        },
        "section3": {
          "title": "ANSWER SHEET",
          "subtitle": "LRN Answer Sheet for C1 C2",
          "link": "http://esolnethellas.gr/wp-content/uploads/2017/02/LRN-Computerised-Answer-Sheet-C1-C2.pdf"
        }
      }
    },
    "mockVideos": {
      "b2": {
        "title": "LRN simulacros de entrevista de Nivel B2",
        "subtitle": "A full LRN speaking test practice. For all speaking sample tests please follow the link: lrnhellas.gr",
        "link": "https://www.youtube.com/embed/LFJnqUxMSZk?feature=oembed",
        "directLink": "https://www.youtube.com/watch?v=LFJnqUxMSZk",
      },
      "c2": {
        "title": "LRN simulacros de entrevista de Nivel C2",
        "subtitle": "A full LRN speaking test practice. For all speaking sample tests please follow the link: lrnhellas.gr",
        "link": "https://www.youtube.com/embed/WYhuXhIarJ4?feature=oembed",
        "directLink": "https://www.youtube.com/watch?v=WYhuXhIarJ4",
      }
    }
  },
  "registration": {
    "title": "REGISTRO",
    "subtitle": "Las inscripciones para el Período de examen el 20 de JANUARY de 2019 empezar en 5 de noviembre de 2018.",
    "duration": "<b>Duración de registro:</b><br /> 5 de novembre a 9 de diciembre de 2018.",
    "guide": {
      "title": "Guía de inscripción en línea 2018",
      "subtitle": "Complete su registro de 3 pasos para los exámenes del LRN",
      "text": "Descargar la guía"
    },
    "costsLrn": {
      "title": "Tarifa del LRN <br /> para el examen de enero de 2019",
      "costs": ['<b>NIVEL A1:</b> 70 €','<b>NIVEL A2:</b> 70 €','<b>NIVEL B1:</b> 70 €','<b>NIVEL B2:</b> 70 €','<b>NIVEL C1:</b> 80 €','<b>NIVEL C2:</b> 100 €']
    },
    "nameChange": {
      "title": "Correcciones de nombre",
      "text1": "Los errores en Nombre y Apellido solo se reconocen si se envía la identidad o el pasaporte que también se requieren.",
      "text2": "Un error en el nombre de un padre, si no está incluido en el pasaporte del candidato, y en ausencia de identidad o pasaporte del padre, no se reconoce.",
      "text3": "Para corregir un error en el nombre del solicitante, se aplica una tarifa de reemisión de £ 50, más los costos de transporte."
    }
  }
}

export default text