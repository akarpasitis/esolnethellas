import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../utils/setLanguage"
import Link from 'gatsby-link'
import greek from '../text/greek.js'
import spanish from '../text/spanish.js'
import english from '../text/english.js'
import lightBulbIcon from '../assets/icons/PNG/lightbulb.png'
import trophyIcon from '../assets/icons/PNG/trophy.png'
import heartIcon from '../assets/icons/PNG/heart.png'
import checkIcon from '../assets/icons/PNG/check.png'
import nextIcon from '../assets/icons/PNG/next.png'
import mapIcon from '../assets/icons/PNG/map.png'
import phoneIcon from '../assets/icons/PNG/phone.png'
import emailIcon from '../assets/icons/PNG/email.png'
import clockIcon from '../assets/icons/PNG/clock.png'
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import _ from 'lodash'
import MetaTags from 'react-meta-tags'
import './contact.css'

class ContactPage extends Component {
  state = {}

  render() {
    const language = setLanguage(this.props.state)
    const text = language.contact

    return (
      <div className='contactcontainer'>
        <MetaTags>
          <title>Esolnet España - Comunicación</title>
          <meta name="description" content="LRN Comunicación, España / Contact Us, Spain." />
        </MetaTags>
        <div className='contactcontent'>
          <div className='contactheader'>{ReactHtmlParser(_.get(text, 'text'))}</div>
          <div className='contactheader' style={{ fontSize: '24px' }}>{ReactHtmlParser(_.get(text, 'phoneEmail'))}</div>
        </div>
        <div className='contactdetailssection'>
          <div className='contactdetailscell'>
            <div className='iconholder'>
              <img src={mapIcon}/>
            </div>
            <div className='contactdetailstext'>
            <div style={{ textAlign: 'center' }}><b>{_.get(text, 'addressLabel')}</b></div>
            <div style={{ textAlign: 'center' }}>{ReactHtmlParser(_.get(text, 'address'))}</div>
            </div>
          </div>
          <div className='contactdetailscell'>
            <div className='iconholder'>
              <img src={clockIcon}/>
            </div>
            <div className='contactdetailstext'>
              <div style={{ textAlign: 'center' }}><b>{_.get(text, 'scheduleLabel')}</b></div>
              <div style={{ textAlign: 'center' }}>{ReactHtmlParser(_.get(text, 'schedule'))}</div>
            </div>  
          </div>
          <div className='contactdetailscell'>
            <div className='iconholder'>
              <img src={phoneIcon}/>
            </div>
            <div className='contactdetailstext'>
              <div style={{ textAlign: 'center' }}><b>{_.get(text, 'phoneLabel')}</b></div>
              <div style={{ textAlign: 'center' }}>{ReactHtmlParser(_.get(text, 'phone'))}</div>
            </div>  
          </div>
          <div className='contactdetailscell'>
            <div className='iconholder'>
              <img style={{height: '75px' }} src={emailIcon}/>
            </div>
            <div className='contactdetailstext'>
              <div style={{ textAlign: 'center' }}><b>{_.get(text, 'emailLabel')}</b></div>
              <div style={{ textAlign: 'center' }}>{ReactHtmlParser(_.get(text, 'email'))}</div>
            </div>  
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(ContactPage)