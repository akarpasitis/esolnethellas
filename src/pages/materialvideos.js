import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../utils/setLanguage"
import Link from 'gatsby-link'
import './materialvideos.css'
import pdfIcon from '../assets/icons/PNG/pdf.png'
import headphonesIcon from '../assets/icons/PNG/headphones.png'
import _ from 'lodash'

import greek from '../text/greek'
import spanish from '../text/spanish.js'
import english from '../text/english.js'

class MaterialVideos extends Component {
  state = {
    exam: _.get(this.props.location, 'query') || 'b2'
  }

  render() {
    const language = setLanguage(this.props.state)
    const text = language.material.mockVideos[this.state.exam]

    return (
      <div className='materialvideoscontainer'>
        <div className='materialvideosbanner'>
          <h2 style={{ fontWeight: '500'}}>{_.get(text, 'title')}</h2>
          <h3 style={{ fontWeight: '300'}}>{_.get(text, 'subtitle')}</h3>
        </div>
        <div className='materialvideoscontent'>
          <iframe className='materialvideosplayer' src={text.link}/>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(MaterialVideos)