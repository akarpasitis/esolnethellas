import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../utils/setLanguage"
import Link from 'gatsby-link'
import './materialpapers.css'
import pdfIcon from '../assets/icons/PNG/pdf.png'
import headphonesIcon from '../assets/icons/PNG/headphones.png'
import _ from 'lodash'


import greek from '../text/greek'
import spanish from '../text/spanish.js'
import english from '../text/english.js'

class MaterialPapers extends Component {
  state = {
    exam: _.get(this.props.location, 'query') || 'b1'
  }

  render() {
    const language = setLanguage(this.props.state)
    const text = language.material.pastPapers[this.state.exam]

    return (
      <div className='materialpaperscontainer'>
        <div className='materialpaperscontent'>
          <h2>{_.get(text, 'section1.title')}</h2>
          <div className='materialpapersrow'>
            {_.get(text, 'section1.pastPapers').map((item, index) => {
              return (
                <a key={`${index}a`} style={{ color: 'black' }} href={item.link} target='_blank'>
                  <div key={`${index}b`} className='materialpaperscell'>
                    <img key={`${index}c`} className='pdficon' src={pdfIcon}/>
                    <div key={`${index}d`} className='materialpaperscelltext'>{item.text}</div>
                  </div>
                </a>
              )
            })}
          </div>
          <div className='materialpapersrow'>
            {_.get(text, 'section1.listeningAudio').map((item, index) => {
              return (
                <div key={`${index}e`} className='materialpaperscell'>
                  <img key={`${index}f`} className='pdficon' src={headphonesIcon}/>
                  <div key={`${index}g`} className='materialpaperscelltext'>{item.text}</div>
                  <audio key={`${index}h`} controls style={{width: '240px', margin: '0 auto', alignSelf: 'center'}}>
                    <source key={`${index}i`} src={item.link} type="audio/mpeg"/>
                    Your browser does not support the audio tag.
                  </audio>
                </div>
              )
            })}
          </div>
          <h2>{_.get(text, 'section2.title')}</h2>
          <div className='materialpapersrow'>
            {_.get(text, 'section2.samplePapers').map((item, index) => {
              return (
                <a key={`${index}j`} tyle={{ color: 'black' }} href={item.link} target='_blank'>
                  <div key={`${index}k`} className='materialpaperscell'>
                    <img key={`${index}l`} className='pdficon' src={pdfIcon}/>
                    <div key={`${index}m`} className='materialpaperscelltext'>{item.title}</div>
                  </div>
                </a>
              )
            })}
          </div>
          <div className='materialpapersrow'>
            {_.get(text, 'section2.listeningAudio').map((item, index) => {
              return (
                <div key={`${index}n`} className='materialpaperscell'>
                  <img key={`${index}o`} className='pdficon' src={headphonesIcon}/>
                  <div key={`${index}p`} className='materialpaperscelltext'>{item.text}</div>
                  <audio key={`${index}q`} controls style={{width: '240px', margin: '0 auto', alignSelf: 'center'}}>
                    <source key={`${index}r`} src={item.link} type="audio/mpeg"/>
                    Your browser does not support the audio tag.
                  </audio>
                </div>
              )
            })}
          </div>
          <div className='materialpapersrow'>
            <a style={{ color: 'black' }} href={_.get(text, 'section3.link')} target='_blank'>
              <div className='materialpaperscell'>
                <h2 className='materialpaperscelltext'>{_.get(text, 'section3.title')}</h2>
                <img className='pdficon' src={pdfIcon}/>
                <div className='materialpaperscelltext'>{_.get(text, 'section3.subtitle')}</div>            
              </div>
            </a>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(MaterialPapers)