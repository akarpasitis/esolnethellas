import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../utils/setLanguage"
import Link from 'gatsby-link'
import './lrnexams.css'
import b1icon from '../assets/b1.jpg'
import b2icon from '../assets/b2.jpg'
import c1icon from '../assets/c1.jpg'
import c2icon from '../assets/c2.jpg'
import { Router, Route, Switch } from 'react-router'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import Zoom from 'react-reveal/Zoom'
import MetaTags from 'react-meta-tags'
import _ from 'lodash'

import greek from '../text/greek'
import spanish from '../text/spanish.js'
import english from '../text/english.js'

class LrnExams extends Component {

  renderListItem = (item, i) => {
    if (item.sub2 !== null & item.sub1 !== null) {
      return (
        <div key={`${i}-1`}>
          <li key={i}>{item.main}</li>
          <li className='lrnsubtitle' key={`${i}+1`}>{ReactHtmlParser(item.sub1)}</li>
          <li className='lrnsubtitle' key={`${i}+2`}>{ReactHtmlParser(item.sub2)}</li>    
        </div>
      ) 
    } else if (item.sub1 !== null && item.sub2 === null) {
      return (
        <div key={`${i}-1`}>
          <li key={i}>{ReactHtmlParser(item.main)}</li>
          <li className='lrnsubtitle' key={`${i}+1`}>{ReactHtmlParser(item.sub1)}</li>
        </div>
      )
    } else {
      return (
        <li key={i}>{ReactHtmlParser(item.main)}</li>
      )
    }
  }

  render() {
    const language = setLanguage(this.props.state)
    const text = language.lrnExams
    const listLeft =  text.bullets.slice(0,9)
    const listRight =  text.bullets.slice(9-18)   

    return (
      <div className='lrncontainer'>
        <MetaTags>
          <title>Esolnet España - LRN Exámenes</title>
          <meta name="description" content="LRN - Learning Resource Network Examinations, Spain / Exámenes, España." />
        </MetaTags>
        <h2>{text.title1}</h2>
        <p>{text.subtitle1}<br />{ReactHtmlParser(text.subtitle2)}</p>
        <h2 style={{marginTop: '50px'}}>{ReactHtmlParser(text.title2)}</h2>
        <div className='lrnlistcontainer'>
          <ul className='lrnlistleft'> 
            {listLeft.map((item, i) => {
              return this.renderListItem(item, i)
            })}
          </ul>
          <ul className='lrnlistright'>
            {listRight.map((item, i) => {
              return this.renderListItem(item, i)
            })}
          </ul>
        </div>
        <div className='examlinks'>
          <div>
            <Zoom duration={1500}><Link to={{'pathname': '/lrnb1/', 'query': 'b1'}}><img className='lrnIcon' src={b1icon}/></Link></Zoom>
          </div>
          <div>
            <Zoom duration={1500} delay={200}><Link to={{'pathname': '/lrnb2/', 'query': 'b2'}}><img className='lrnIcon' src={b2icon}/></Link></Zoom>
          </div>
          <div>
            <Zoom duration={1500} delay={400}><Link to={{'pathname': '/lrnc1/', 'query': 'c1'}}><img className='lrnIcon' src={c1icon}/></Link></Zoom>
          </div>
          <div>
            <Zoom duration={1500} delay={600}><Link to={{'pathname': '/lrnc2/', 'query': 'c2'}}><img className='lrnIcon' src={c2icon}/></Link></Zoom>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(LrnExams)