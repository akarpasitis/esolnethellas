import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../utils/setLanguage"
import Link from 'gatsby-link'
import './registration.css'
import headphonesIcon from '../assets/icons/PNG/headphones.png'
import _ from 'lodash'
import pdfIcon from '../assets/icons/PNG/pdf.png'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import MetaTags from 'react-meta-tags'

import greek from '../text/greek'
import spanish from '../text/spanish.js'
import english from '../text/english.js'

class RegistrationPage extends Component {
  state = {}

  render() {
    const language = setLanguage(this.props.state)
    const text = language.registration

    return (
      <div className='registrationcontainer'>
        <MetaTags>
          <title>Esolnet España - LRN Información de Registro</title>
          <meta name="description" content="LRN Información de Registro, España / Registratio Information, Spain." />
        </MetaTags>
        <div className='registrationcontent'>
          <h2>{ReactHtmlParser(text.title)}</h2>
          <h4>{ReactHtmlParser(text.subtitle)}</h4>
          <p style={{ textAlign: 'center'}}>{ReactHtmlParser(text.duration)}</p>
          <div className='registrationbox'>
            <p><b>{ReactHtmlParser(text.costsLrn.title)}</b></p>
            {text.costsLrn.costs.map((item, index) => {
              return (
                <div key={`${index}cost`}>{ReactHtmlParser(item)}</div>
              )
            })}
          </div>
          <h4>{ReactHtmlParser(text.nameChange.title)}</h4>
          <div className='registrationnamechange'>
            <p>{ReactHtmlParser(text.nameChange.text1)}</p>
            <p>{ReactHtmlParser(text.nameChange.text2)}</p>
            <p>{ReactHtmlParser(text.nameChange.text3)}</p> 
          </div>         
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(RegistrationPage)