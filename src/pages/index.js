import React, { Component } from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import setLanguage from "../utils/setLanguage"
import Link from "gatsby-link"
import GatsbyLink from "gatsby-link"
import greek from "../text/greek.js"
import spanish from "../text/spanish.js"
import lightBulbIcon from "../assets/icons/PNG/lightbulb.png"
import trophyIcon from "../assets/icons/PNG/trophy.png"
import heartIcon from "../assets/icons/PNG/heart.png"
import checkIcon from "../assets/icons/PNG/check.png"
import nextIcon from "../assets/icons/PNG/next.png"
import previousIcon from "../assets/icons/PNG/previous.png"
import recognitionImageSpanish from "../assets/anagnoriseis.jpg"
import recognitionImageGreek from "../assets/anagnoriseis-greek.jpg"
import cleverTestImage from "../assets/exetaseis-lrn.png"
import carouselImage1 from "../assets/carousel1.jpg"
import carouselImage2 from "../assets/carousel2.jpg"
import carouselImage3 from "../assets/carousel3.jpg"
import carouselImage4 from "../assets/carousel4.jpg"
import imageButton1Spanish from "../assets/homepage-info.jpg"
import imageButton2Spanish from "../assets/homepage-material.jpg"
import imageButton3Spanish from "../assets/lrn-exams-1.jpg"
import imageButton1Greek from "../assets/homepage-info-greek.jpg"
import imageButton2Greek from "../assets/homepage-material-greek.jpg"
import imageButton3Greek from "../assets/lrn-exams-1-greek.jpg"
import imageButton1English from "../assets/homepage-info-english.jpg"
import imageButton2English from "../assets/homepage-material-english.jpg"
import imageButton3English from "../assets/lrn-exams-1-english.jpg"
import lrnIcon from "../assets/lrn-icon.png"
import Fade from "react-reveal/Fade"
import Zoom from "react-reveal/Zoom"
import ErrorBoundary from '../utils/ErrorBoundary'
import _ from "lodash"

import style from "./index.module.css"

class IndexPage extends Component {
  state = {
    isMobile: false,
    isLowRes: false,
    carouselPosition: 0,
    carouselImage: carouselImage1
  }

  componentDidMount() {
    if (screen.width < 650) {
      this.setState({ isMobile: true })
    } else {
      this.setState({ isMobile: false })
    }
    setInterval(() => {
      this.goToNext()
    },7000)
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions = () => {
    if (window.innerWidth < 1200) {
      this.setState({ isLowRes: true })
    } else if (window.innerWidth >= 1200) {
      this.setState({ isLowRes: false })
    }
    if (screen.width < 650) {
      this.setState({ isMobile: true })
    } else if (screen.width >= 650) {
      this.setState({ isMobile: false })
    }
  }

  renderSection(section, index) {
    let icon
    switch (index) {
      case 0:
        icon = lightBulbIcon
        break
      case 1:
        icon = trophyIcon
        break
      case 2:
        icon = heartIcon
        break
    }

    return (
      <div key={`${index}a`} className={style.homesection}>
        <div key={`${index}b`} className={style.homesectionleft}>
          <img key={`${index}c`} className={style.homemainicon} src={icon} />
          {section.title}
        </div>
        <div key={`${index}d`} className={style.homesectionright}>
          {section.bullets.map((bullet, subIndex) => {
            if (bullet.sub1) {
              return (
                <div key={`${index}e`}>
                  <div key={`${index}f`} className={style.homebulletmain}>
                    <img key={`${index}g`} className={style.homecheckicon} src={checkIcon} />
                    {bullet.main}
                  </div>
                  <div key={`${index}h`} className={style.homebulletsub}>{bullet.sub1}</div>
                </div>
              )
            } else {
              return (
                <div key={`${index}i${subIndex}`} className={style.homebulletmain}>
                  <img key={`${index}j${subIndex}`} className={style.homecheckicon} src={checkIcon} />
                  {bullet.main}
                </div>
              )
            }
          })}
        </div>
      </div>
    )
  }

  goToNext = () => {
    const position =
      this.state.carouselPosition >= 2 ? 0 : this.state.carouselPosition + 1
    this.setState({ carouselPosition: position })
  }

  goToPrevious = () => {
    const position =
      this.state.carouselPosition <= 0 ? 2 : this.state.carouselPosition - 1
    this.setState({ carouselPosition: position })
  }

  getCarouselContent = (position, text) => {
        return (
          <div className={style.homecarouselcontent}>
            <div className={style.homecarouselone}>
              <div className={style.homelrntitle}>
                <img className={style.homelrnimage} src={lrnIcon} />
                {text.carousel[0]}
              </div>
              <div className={style.homecarouselonetextcontainer}>
                <div className={style.homecarouselonetext}>
                  {text.carousel[1]}<br /><br />
                  {text.carousel[2]}
                </div>
              </div>
            </div>
          </div>
        )
  }

  render() {
    let carouselImage
    switch (this.state.carouselPosition) {
      case 0:
        carouselImage = carouselImage1
        break
      case 1:
        carouselImage = carouselImage2
        break
      case 2:
        carouselImage = carouselImage4
        break
      default:
        carouselImage = carouselImage1
        break
    }

    const language = setLanguage(this.props.state)
    const text = language.homeText

    let imageButton1 = imageButton1Spanish
    let imageButton2 = imageButton2Spanish
    let imageButton3 = imageButton3Spanish

    switch(this.props.state) {
      case 'SPANISH':
        imageButton1 = imageButton1Spanish
        imageButton2 = imageButton2Spanish
        imageButton3 = imageButton3Spanish
        break
      case 'GREEK':
        imageButton1 = imageButton1Greek
        imageButton2 = imageButton2Greek
        imageButton3 = imageButton3Greek
        break
      case 'ENGLISH':
        imageButton1 = imageButton1Greek
        imageButton2 = imageButton2Greek
        imageButton3 = imageButton3Greek
        break
      default:
        break
    }

    return (
      <div className={style.homecontainer}>
        <div className={style.homecontent}>
          <ErrorBoundary>        
          <div className={style.homeinfobar}>
            {ReactHtmlParser(text.description)}
            <Link to={{ pathname: "/company/", query: "scroll" }} className={style.homelinkmore}>
              {" "}
              {text.more}{" "}
            </Link>
          </div>
          <div className={style.hometop}>
            <div className={style.hometopleft}>
              <h2 className={style.recognitionTitle}>{text.recognition}</h2>
            {text.bodies.map((item, index) => {
              return (
                <div key={`${index}bdiv`} className={style.recognitionText}>
                  <p key={`${index}bpar1`} className={style.recognitionSubtitle}>{ReactHtmlParser(item.title)}</p>
                  <p key={`${index}bpar2`} style={{ width: '100%' }}>{ReactHtmlParser(item.text1)}</p>
                  <p key={`${index}bpar3`} style={{ width: '100%' }}>{ReactHtmlParser(item.text2)}</p>
                </div>
              )
            })}
            </div>
            {!this.state.isMobile && (
              <div className={style.hometopright}>
              <Fade>
                <div
                  className={style.homecarouselcontainer}
                  style={{ backgroundImage: `url(${carouselImage})` }}
                >
                  <div
                    className={style.homecarouselleft}
                    onClick={this.goToPrevious}
                  >
                    <img
                      className={style.homenavigationbuttons}
                      src={previousIcon}
                    />
                  </div>
                  {this.getCarouselContent(this.state.carouselPosition, text)}
                  <div
                    className={style.homecarouselright}
                    onClick={this.goToNext}
                  >
                    <img
                      className={style.homenavigationbuttons}
                      src={nextIcon}
                    />
                  </div>
                </div>
                </Fade>                
              </div>
            )}
          </div>
          <div className={style.homebottom}>
            <h1>{text.title}</h1>
            <div className={style.homesections}>
              {text.sections.map((section, index) => {
                return this.renderSection(section, index)
              })}
            </div>
            <div className={style.homenavbar}>
            <Link to="/registration/">
              <img src={imageButton1} className={style.homenavbarimages} />
            </Link>
            <Link to={{ pathname: "/material/", query: "showall" }}>
              <img src={imageButton2} className={style.homenavbarimages} />
            </Link>
            <Link to="/lrnexams/">
              <img src={imageButton3} className={style.homenavbarimages} />
            </Link>
          </div>
          </div>
          </ErrorBoundary>          
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default withRouter(connect(mapStateToProps)(IndexPage))