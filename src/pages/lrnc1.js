import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../utils/setLanguage"
import Link from 'gatsby-link'
import LrnExam from '../components/LrnExam'
import LrnExamInfo from '../components/LrnExamInfo'
import _ from 'lodash'

import style from './lrn.css'
import greek from '../text/greek'
import spanish from '../text/spanish.js'
import english from '../text/english.js'

class LrnC1 extends Component {
  state = {
    exam: _.get(this.props.location, 'query')
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ exam:  _.get(nextProps.location, 'query') })
  }

  render() {
    const language = setLanguage(this.props.state)
    const text = language.lrnExams
    
    return (
      
      <div className={style.lrnexamcontainer}>
        <LrnExam exam={'c1'} />
        <LrnExamInfo exam={2}/>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(LrnC1)