import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../utils/setLanguage"
import Link from 'gatsby-link'
import './faq.css'
import _ from 'lodash'
import addIcon from '../assets/icons/PNG/add.png'
import closeIcon from '../assets/icons/PNG/close.png'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import Flip from 'react-reveal/Flip';

import greek from '../text/greek'
import spanish from '../text/spanish.js'
import english from '../text/english.js'

class Faq extends Component {
  state = {}

  handleClick = (item) => {
    if (this.state[item]) {
      if (this.state[item] === 'block') {
        this.setState({ [item]: 'none'})
      } else {
        this.setState({ [item]: 'block'})
      }
    } else {
      this.setState({ [item]: 'block'})
    }
  }

  renderFaq = (item, i, section) => {
    const answerStyle = this.state[`${i}a${section}`] ? this.state[`${i}a${section}`] : 'none'
    const image = this.state[`${i}a${section}`] === 'block' ? closeIcon : addIcon

    return (
      <div key={`${i}faq`}>
        <div key={`${i}q${section}`} onClick={() => this.handleClick(`${i}a${section}`)} className="faqquestion">
          <img className='addicon' src={image}/>{ReactHtmlParser(item.q)}
        </div>
        <Flip top>
          <div key={`${i}a${section}`} style={{ display: answerStyle }} className="faqanswer">{ReactHtmlParser(item.a)}</div>
        </Flip>  
      </div>
    )
  }

  render() {
    const language = setLanguage(this.props.state)
    const text = language.faqs
    const titles = language.faqsTitles

    return (
      <div className='faqcontainer'>
        <div className='faqcontent'>
        <div className='faqtitle'>{titles[0]}</div>
        {_.get(text,'exams').map((item, i) => {
          return this.renderFaq(item,i, 1)
        })}
        <div className='faqtitle'>{titles[1]}</div>
        {_.get(text,'registration').map((item, i) => {
          return this.renderFaq(item,i, 2)
        })}
        <div className='faqtitle'>{titles[2]}</div>
        {_.get(text,'material').map((item, i) => {
          return this.renderFaq(item,i, 3)
        })}
        <div className='faqtitle'>{titles[3]}</div>
        {_.get(text,'retake').map((item, i) => {
          return this.renderFaq(item,i, 4)
        })}
        <div className='faqtitle'>{titles[4]}</div>
        {_.get(text,'recognizition').map((item, i) => {
          return this.renderFaq(item,i, 5)
        })}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(Faq)