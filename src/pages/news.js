import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../utils/setLanguage"
import Link from 'gatsby-link'
import greek from '../text/greek.js'
import spanish from '../text/spanish.js'
import english from '../text/english.js'
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import facebookBlackIcon from '../assets/icons/PNG/facebookblack.png'
import youtubeBlackIcon from '../assets/icons/PNG/youtubeblack.png'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import _ from 'lodash'
import './news.css'

class News extends Component {
  state = {}

  render() {
    const language = setLanguage(this.props.state)
    const text = language.company
    const title = language.header.buttons.news

    return (
      <div className='newscontainer'>
        <div className='newscontent'>
          <h2>{title}</h2>
          <p>{_.get(text, 'footerText')}</p>
          <div className='newslinks'>
            <a className='footersociallink' href='https://www.facebook.com/Esolnet-Espa%C3%B1a-2156276291269633/' target='_blank'><img className='newssociallogo' src={facebookBlackIcon}/></a><br/>
            <a className='footersociallink' href='https://www.youtube.com/channel/UCjCwAVbDlgUusWt-wulqVbA' target='_blank'><img className='newssociallogo' src={youtubeBlackIcon}/></a>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(News)