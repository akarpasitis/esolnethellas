import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../utils/setLanguage"
import Link from 'gatsby-link'
import greek from '../text/greek.js'
import spanish from '../text/spanish.js'
import english from '../text/english.js'
import facebookIcon from '../assets/icons/PNG/facebook.png'
import youtubeIcon from '../assets/icons/PNG/youtube.png'
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import ScrollableAnchor, { goToAnchor, configureAnchors } from 'react-scrollable-anchor'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import _ from 'lodash'

import './company.css'

configureAnchors({offset: -100})

class CompanyPage extends Component {
  state = {
    scrollHandler: 1
  }

  componentDidMount() {
    document.addEventListener('scroll', () => {
      this.setState({ yScroll: window.scrollY })
    })


    if (this.state.scrollHandler > 0) {
      this.setState({ scrollHandler: -1 })
      setTimeout(() => {
        if (
          _.get(this.props.location, 'query') === 'scroll') {
          goToAnchor('lrn')
        }
      }, 500)
    }
  }

  render() {
    const language = setLanguage(this.props.state)
    const text = language.company

    return (
      <div className='companycontainer'>
        <div className='companybanner'>
          <h2 style={{ fontWeight: '500' }}>{ReactHtmlParser(_.get(text, 'headerTitle'))}</h2>
          <h3 style={{ fontWeight: '300' }}>{ReactHtmlParser(_.get(text, 'headerSubtitle'))}</h3>
        </div>
        <Fade duration={1500}>
        <div className='companycontent'>
          <h2 className='companytitle'>{ReactHtmlParser(_.get(text, 'title1'))}</h2>
          {_.get(text, 'text1').map((paragraph, index) => {
            return (
              <p key={`${index}a`}>{ReactHtmlParser(paragraph)}</p>
            )
          })}
          <ScrollableAnchor id={'lrn'}>
            <h2 className='companytitle'>{ReactHtmlParser(_.get(text, 'title2'))}</h2>
          </ScrollableAnchor>
          <p>{ReactHtmlParser(_.get(text, 'text2')[0])}</p>
          <p style={{ textAlign: 'left', fontWeight: '600' }}>{ReactHtmlParser(_.get(text, 'subtitle2'))}</p>
          <p>{ReactHtmlParser(_.get(text, 'text2')[1])}</p>
        </div>
        </Fade>
        <Fade top>
          <div className='companybannerbottom'>
            <h2 style={{ fontWeight: '500' }}>{ReactHtmlParser(_.get(text, 'footerTitle'))}</h2>
            <h3 style={{ fontWeight: '300' }}>{ReactHtmlParser(_.get(text, 'footerText'))}</h3>
            <div className='companysociallinks'>
              <a href='https://www.facebook.com/Esolnet-Espa%C3%B1a-2156276291269633/' target='_blank'><img className='companysociallogo' src={facebookIcon}/></a>
              <a href='https://www.youtube.com/channel/UCjCwAVbDlgUusWt-wulqVbA' target='_blank'><img className='companysociallogo' src={youtubeIcon}/></a>
            </div>
          </div>
        </Fade>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(CompanyPage)