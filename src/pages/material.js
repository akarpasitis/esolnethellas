import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../utils/setLanguage"
import Link from 'gatsby-link'
import './material.css'
import b1image from '../assets/pplrnb1.jpg'
import b2image from '../assets/pplrnb2.jpg'
import c1image from '../assets/pplrnc1.jpg'
import c2image from '../assets/pplrnc2.jpg'
import c2VideoImage from '../assets/mvc2.jpg'
import b2VideoImage from '../assets/mvb2.jpg'
import _ from 'lodash'
import MetaTags from 'react-meta-tags'


import greek from '../text/greek'
import spanish from '../text/spanish.js'
import english from '../text/english.js'

// ORIGINAL VERSION FOR REDIRECTING TO IFRAME PAGE FOR VIDEO
// {_.get(text, 'sections2').map((item, index) => {
//   const text = language.material.mockVideos
//   console.warn(text)
//   return (
//     <Link key={`${index}f`} className='materialcelllink' to={{'pathname': '/materialvideos/', 'query': mockVideosQueries[index]}}>
//       <img key={`${index}g`} className='materiallinkimage' src={mockVideosImages[index]}/>
//       <div key={`${index}h`} style={{ textAlign: 'center', color: 'black' }}>{item}</div>
//     </Link>
//   )
// })}


class Material extends Component {
  state = {
    exam: _.get(this.props.location, 'query')
  }

  render() {
    const language = setLanguage(this.props.state)
    const text = language.material

    const pastPapersImages = [c2image, c1image, b2image, b1image]
    const mockVideosImages = [c2VideoImage, b2VideoImage]
    const pastPaperQueries = ['c2','c1','b2','b1']
    const mockVideosQueries = ['c2','b2']
    const showBoth = 
      _.get(this.props.location, 'query') === 'showall' ||
      _.get(this.props.location, 'query') === undefined
    const showPastPapers = showBoth ? true : _.get(this.props.location, 'query') === 'pastpapers'
    const showMockVideos = showBoth ? true : _.get(this.props.location, 'query') === 'mockvideos'    

    const videoLinkC2 = language.material.mockVideos.c2.directLink;
    const videoLinkB2 = language.material.mockVideos.b2.directLink;

    const textVideos = _.get(text, 'sections2');

    return (
      <div className='materialcontainer'>
        <MetaTags>
          <title>Esolnet España - Material de Apoyo</title>
          <meta name="description" content="LRN Material de Apoyo, Spain / Supporting Material Spain, España." />
        </MetaTags>
        <div className='materialcontent'>
          {showPastPapers &&
            (
            <div className="materialbox">
              <h2 style={{ marginTop: '30px', borderBottom: '2px solid green' }}>{_.get(text, 'title1')}</h2>
              <div className='materialsection'>
                {_.get(text, 'sections1').map((item, index) => {
                  return (
                    <div key={`${index}a`} className='materialcell'>
                      <Link key={`${index}b`} className='materialcelllink' to={{'pathname': '/materialpapers/', 'query': pastPaperQueries[index]}}>
                        <img key={`${index}c`} className='materiallinkimage' src={pastPapersImages[index]}/>
                        <div key={`${index}d`} style={{ textAlign: 'center', color: 'black' }}>{item}</div>
                      </Link>
                    </div>
                  )
                })}
                </div>
            </div>
          )}
        </div>

        {showMockVideos &&
          (<div className="materialbox">
            <h2 style={{ marginTop: '50px', borderBottom: '2px solid green' }}>{_.get(text, 'title2')}</h2>
            <div className='materialsection'>
              <div className='materialcell'>
                <a className='materialcelllink' href={videoLinkC2} target='_blank'>
                  <img className='materiallinkimage' src={mockVideosImages[0]}/>
                  <div style={{ textAlign: 'center', color: 'black' }}>{textVideos[0]}</div>
                </a>
              </div>
              <div className='materialcell'>
                <a className='materialcelllink' href={videoLinkB2} target='_blank'>
                  <img className='materiallinkimage' src={mockVideosImages[1]}/>
                  <div style={{ textAlign: 'center', color: 'black' }}>{textVideos[1]}</div>
                </a>
              </div>
            </div>
          </div>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(Material)