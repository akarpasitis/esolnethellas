import React, { Component } from "react"

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      return (
        <div>
          <h2 style={{ textAlign: 'center' }}>Something went wrong, please refresh your browser.</h2>
          <h3 style={{ textAlign: 'center' }}>If the problem persists please contact us.</h3>
        </div>
      )
    }
    return this.props.children;
  }
}

export default ErrorBoundary