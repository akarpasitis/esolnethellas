import greek from '../text/greek'
import spanish from '../text/spanish'
import english from '../text/english'

function changeLanguage (type) {
  switch (type) {
    case 'SPANISH':
      return spanish
    case 'GREEK':
      return greek
    case 'ENGLISH':
      return english
    default:
      return spanish
  }
}


export default changeLanguage