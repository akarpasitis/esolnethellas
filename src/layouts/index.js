import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import _ from 'lodash'

import Header from '../components/Header'
import Footer from '../components/Footer'
import CookieNotice from '../components/CookieNotice'
import './index.css'

class TemplateWrapper extends Component {
  constructor() {
    super()
    this.state = {
      cookiesAccepted: false,
      cookiesHaveBeenAcceptedBefore: false,
    }
  }

  componentDidMount() {
    this.setState({ cookiesHaveBeenAcceptedBefore: localStorage.getItem( 'cookiesAccepted' ) || false });
    const cookiesHaveBeenAcceptedBefore = window.localStorage.getItem( 'cookiesAccepted' ) || false;
    if (!cookiesHaveBeenAcceptedBefore) {
      document.cookie.split(";").forEach(c => {
        document.cookie = c.replace(/^ +/, "").replace(
          /=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"
        );
      });
    }
  }

  setCookies = () => {
    this.setState({ cookiesAccepted: true, cookiesHaveBeenAcceptedBefore: true })
  }

  render() {
    const { children } = this.props;
    const { cookiesAccepted, cookiesHaveBeenAcceptedBefore } = this.state;
    // let cookiesHaveBeenAcceptedBefore
    // if (window) {
    //   cookiesHaveBeenAcceptedBefore = window.localStorage.getItem( 'cookiesAccepted' ) || false;
    // } else {
    //   cookiesHaveBeenAcceptedBefore = false
    // }

    return (
      <div className='wrapper'>
        <Helmet
          title="Esolnet España"
          meta={[
            { property: 'og:image', content: 'http://esolnetespana.es/static/LogoWhite.b3cf7662.png'},
            { name: 'og:image', content: 'http://esolnetespana.es/static/LogoWhite.b3cf7662.png'},
            { name: 'description', content: 'LRN Exámenes de idioma inglés de Esolnet España, ALTE, UCAS, OFQUAL.' },
            { name: 'google', content: 'notranslate'}
          ]}
        />
        <Header />
        {!cookiesHaveBeenAcceptedBefore && <CookieNotice  style={cookiesAccepted ? { display: 'none'} : {}} setCookies={this.setCookies}/>}
        <div className='content'>
          {children()}
        </div>
        <Footer />
      </div>
    )
  }
}

TemplateWrapper.propTypes = {
  children: PropTypes.func
}

export default TemplateWrapper
