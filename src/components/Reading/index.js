import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../../utils/setLanguage"
import Link from 'gatsby-link'
import './style.css'
import greek from '../../text/greek'
import spanish from '../../text/spanish'
import english from '../../text/english'
import logo from '../../assets/LogoWhite.png'
import infoIcon from '../../assets/icons/PNG/info.png'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import _ from 'lodash'

class Reading extends Component {
  renderTableRow = (item, i) => {
    return (
      <div className='readingtablerow' key={i}>
        <div className='readingtablecell' style={{ width: '20%', fontWeight: '700'}} key={`${i}a`}>{ReactHtmlParser(item[0])}</div>
        <div className='readingtablecell' style={{ width: '80%'}} key={`${i}b`}>{ReactHtmlParser(item[1])}</div>
      </div>
    )
  }

  render() {
    return (
      <div className='readingcontainer'>
        <div>
          <h4>{ReactHtmlParser(_.get(this.props.text, 'title'))}</h4>
          <h5>{_.get(this.props.text, 'table1.title')}</h5>
          <div className='readingtable'>
          {_.get(this.props.text, 'table1.rows').map((item, i) => {
            return this.renderTableRow(item, i)
          })} 
          </div>
          <h5>{_.get(this.props.text, 'table2.title')}</h5>
          <div className='readingtable'>
          {_.get(this.props.text, 'table2.rows').map((item, i) => {
            return this.renderTableRow(item, i)
          })} 
          </div>
          <p>{_.get(this.props.text, 'footer')}</p>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(Reading)