import React, { Component } from "react"
import Link from "gatsby-link"
import Listening from "../Listening/"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import setLanguage from "../../utils/setLanguage"
import Writing from "../Writing/"
import Reading from "../Reading/"
import Speaking from "../Speaking/"
import "./style.css"
import listeningIcon from '../../assets/icons/PNG/listening.png'
import writingIcon from '../../assets/icons/PNG/writing.png'
import readingIcon from '../../assets/icons/PNG/reading.png'
import speakingIcon from '../../assets/icons/PNG/speaking.png'
import _ from "lodash"

import greek from "../../text/greek"
import spanish from "../../text/spanish"

class LrnExamInfo extends Component {
  constructor() {
    super()
    this.state = {
      section: 'listening',
      language: 'es'
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ language: nextProps.state })
  }

  renderInfo = (examIndex, section, text) => {
    switch (section) {
      case "listening":
        return (
          <Listening
            text={text.exams[examIndex][section]}
            examIndex={examIndex}
          />
        )
      case "writing":
        return (
          <Writing
            text={text.exams[examIndex][section]}
            examIndex={examIndex}
          />
        )
      case "reading":
        return (
          <Reading
            text={text.exams[examIndex][section]}
            examIndex={examIndex}
          />
        )
      case "speaking":
        return (
          <Speaking
            text={text.exams[examIndex][section]}
            examIndex={examIndex}
          />
        )
      default:
        console.warn("errorrrrr")
    }
  }

  checkActive = (type) => {
    if (type === this.state.section) {
      return { borderBottom: '2px solid black' }
    } else {
      return { border: 'none' }
    }
  }

  handleClick = (type) => {
    this.setState({ section: type })
  }

  render() {
    const language = setLanguage(this.props.state)
    const text = language.lrnExams

    return (
      <div className="lrnexaminfo">
        <div className="lrnbreakdown">
          <div className="lrnbreakdownleft">
            <button
              style={this.checkActive("listening")}
              className="exambutton"
              onClick={() => this.handleClick("listening")}
            >
              <img src={listeningIcon} className="lrnbuttonicons" />
              <span className="lrnbuttontext">{text.examsButtons[0]}</span>
            </button>
            <button
              style={this.checkActive("writing")}
              className="exambutton"
              onClick={() => this.handleClick("writing")}
            >
              <img src={writingIcon} className="lrnbuttonicons" />
              <span className="lrnbuttontext">{text.examsButtons[1]}</span>
            </button>
            <button
              style={this.checkActive("reading")}
              className="exambutton"
              onClick={() => this.handleClick("reading")}
            >
              <img src={readingIcon} className="lrnbuttonicons" />
              <span className="lrnbuttontext">{text.examsButtons[2]}</span>
            </button>
            <button
              style={this.checkActive("speaking")}
              className="exambutton"
              onClick={() => this.handleClick("speaking")}
            >
              <img src={speakingIcon} className="lrnbuttonicons" />
              <span className="lrnbuttontext">{text.examsButtons[3]}</span>
            </button>
          </div>
          <div className="lrnbreakdownright">
            {this.renderInfo(
              _.get(this.props, "exam"),
              this.state.section,
              text
            )}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default withRouter(connect(mapStateToProps)(LrnExamInfo))
