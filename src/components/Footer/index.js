import React, { Component } from "react"
import { connect } from "react-redux"
import Link from 'gatsby-link'
import style from './style.css'
import greek from '../../text/greek'
import spanish from '../../text/spanish'
import english from '../../text/english'
import setLanguage from "../../utils/setLanguage"
import logo from '../../assets/LogoWhite.png'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import Fade from 'react-reveal/Fade'
import facebookIcon from '../../assets/icons/PNG/facebook.png'
import youtubeIcon from '../../assets/icons/PNG/youtube.png'
import facebookBlackIcon from '../../assets/icons/PNG/facebookblack.png'
import youtubeBlackIcon from '../../assets/icons/PNG/youtubeblack.png'
import _ from 'lodash'

class Footer extends Component {
  constructor() {
    super();
    this.state = {
      isMobile: false
    }
  }

  componentDidMount() {
    if (screen.width < 650) {
      this.setState({ isMobile: true })
    } else {
      this.setState({ isMobile: false })
    }
    document.addEventListener('scroll', this.handleScroll)
    window.addEventListener('resize', this.updateDimensions.bind(this))
  }

  updateDimensions() {
    if (screen.width < 650) {
      this.setState({ isMobile: true })
    } else {
      this.setState({ isMobile: false })
    }
  }

  render() {
    const language = setLanguage(this.props.state)
    const textHeader = language.header
    const textFooter = language.footer
    const textCookies = language.cookieNotice

    return (
      <div className='footercontainer'>
        <div className='footercontent'>
          <div className='footerleft'>
            <div className='footerlinks'>
              <Link className='footeremaillink' to='/company/'>{ReactHtmlParser(textFooter.button1)} </Link>
              <br/>
              <Link className='footeremaillink' to='/faq/'> {textFooter.button2}</Link>
              <br/>
              <a className="footeremaillink" href="https://www.termsfeed.com/cookies-policy/77fc54670be114dc46255ae9309c2b56" target="_blank">{textCookies.link}</a>
              <br/>
              <a className='footeremaillink' href='https://www.termsfeed.com/privacy-policy/ae0b8920496ff5b38d7c9ae92a8777b2' target='_blank'>{textFooter.button4}</a>
              <br/>
              <a className='footeremaillink' href="mailto:director@dquals.net">{ReactHtmlParser(textHeader.emailAddress)}</a>
            </div>
          </div>
          <div className='footerright'>
            <a className='footersociallink' href='https://www.facebook.com/Esolnet-Espa%C3%B1a-2156276291269633/' target='_blank'><img className='footersociallogo' src={facebookIcon}/></a><br/>
            <a className='footersociallink' href='https://www.youtube.com/channel/UCjCwAVbDlgUusWt-wulqVbA' target='_blank'><img className='footersociallogo' src={youtubeIcon}/></a>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(Footer)