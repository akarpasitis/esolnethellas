import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../../utils/setLanguage"
import Link from 'gatsby-link'
import './style.css'
import greek from '../../text/greek'
import spanish from '../../text/spanish'
import english from '../../text/english'
import logo from '../../assets/LogoWhite.png'
import infoIcon from '../../assets/icons/PNG/info.png'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import _ from 'lodash'

class Listening extends Component {
  render() {
    return (
      <div className='listeningcontainer'>
        <div>
          <h4>{ReactHtmlParser(_.get(this.props.text, 'title'))}</h4>
          <div className='listeningsection'>
            <div className='column'>
              <div className='listeningtitle'>
                <b>{ReactHtmlParser(_.get(this.props.text, 'section1.title'))}</b>
              </div>
              <ul>
              {_.get(this.props.text, 'section1.bullets').map((item, i) => {
                return <li key={i+1}>{item}</li>
              })}
              </ul>
            </div>
            <div className='column'>
              <div className='listeningtitle'>
                <b>{ReactHtmlParser(_.get(this.props.text, 'section2.title'))}</b>
              </div>
              <ul>
              {_.get(this.props.text, 'section2.bullets').map((item, i) => {
                return <li key={i+1}>{item}</li>
              })}
              </ul>
            </div>
            <div className='column'>
              <div className='listeningtitle'>
                <b>{ReactHtmlParser(_.get(this.props.text, 'section3.title'))}</b>
              </div>
              <ul>
              {_.get(this.props.text, 'section3.bullets').map((item, i) => {
                return <li key={i+1}>{item}</li>
              })}
              </ul>
            </div>
          </div>
          <div className='listeninghint'>
            <p><b>{ReactHtmlParser(_.get(this.props.text, 'hint.title'))}</b><br />{ReactHtmlParser(_.get(this.props.text, 'hint.text'))}</p>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(Listening)