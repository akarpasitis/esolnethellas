import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../../utils/setLanguage"
import Link from 'gatsby-link'
import './style.css'
import greek from '../../text/greek'
import spanish from '../../text/spanish'
import english from '../../text/english'
import logo from '../../assets/LogoWhite.png'
import infoIcon from '../../assets/icons/PNG/info.png'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import _ from 'lodash'

class Writing extends Component {
  renderTableRow = (item, i) => {
    const divClass = i === 0 ? 'writingtableheader' : 'writingtablerow'
    return (
      <div key={`${i}wx`} className={divClass} key={i}>
        <div key={`${i}wa`} style={{ width: '33%', textAlign: 'center'}} key={`${i}a`}>{ReactHtmlParser(item[0])}</div>
        <div key={`${i}wb`} style={{ width: '33%', textAlign: 'center'}} key={`${i}b`}>{ReactHtmlParser(item[1])}</div>
        <div key={`${i}wb`} style={{ width: '33%', textAlign: 'center'}} key={`${i}c`}>{ReactHtmlParser(item[2])}</div>
      </div>
    )
  }

  render() {
    if (this.props.examIndex <= 1) {
    return (
        <div className='writingcontainer'>
          <div>
            <h4>{ReactHtmlParser(_.get(this.props.text, 'title'))}</h4>
            <p>
              {ReactHtmlParser(_.get(this.props.text, 'text1'))}
              {ReactHtmlParser(_.get(this.props.text, 'text2'))}
            </p>
            <p>
              {ReactHtmlParser(_.get(this.props.text, 'text3'))}
            </p>
            <ul>
            {_.get(this.props.text, 'bullets').map((item, i) => {
              return <li key={`${i}wl`}>{ReactHtmlParser(item)}</li>
            })}
            </ul>
            <div>{ReactHtmlParser(_.get(this.props.text, 'table.title'))}</div>
            <div className='writingtable'>
            {_.get(this.props.text, 'table.rows').map((item, i) => {
              return this.renderTableRow(item, i)
            })}
            
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div className='writingcontainer'>
          <div>
            <h4>{ReactHtmlParser(_.get(this.props.text, 'title'))}</h4>
            <p>{ReactHtmlParser(_.get(this.props.text, 'text1'))}</p>
            <p>{ReactHtmlParser(_.get(this.props.text, 'text2'))}</p>
            <p>{ReactHtmlParser(_.get(this.props.text, 'text3'))}</p>
            <div>{ReactHtmlParser(_.get(this.props.text, 'table.title'))}</div>
            <div>{ReactHtmlParser(_.get(this.props.text, 'table.subtitle'))}</div>            
            <div className='writingtable'>
            {_.get(this.props.text, 'table.rows').map((item, i) => {
              return this.renderTableRow(item, i)
            })}
            </div>
          </div>
        </div>      )
    }
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(Writing)