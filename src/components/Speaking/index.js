import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../../utils/setLanguage"
import Link from 'gatsby-link'
import './style.css'
import greek from '../../text/greek'
import spanish from '../../text/spanish'
import english from '../../text/english'
import logo from '../../assets/LogoWhite.png'
import infoIcon from '../../assets/icons/PNG/info.png'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import _ from 'lodash'

class Speaking extends Component {
  renderItem = (item, i) => {
    return (
        <div key={`${i}sx`}>
          <li key={`${i}s`}><b>{item.heading}</b>{item.text}</li>
        </div>
      )
  }

  render() {
    return (
      <div className='speakingcontainer'>
        <div>
          <h4>{ReactHtmlParser(_.get(this.props.text, 'title'))}</h4>
          <p><b>{ReactHtmlParser(_.get(this.props.text, 'duration.title'))}</b>{ReactHtmlParser(_.get(this.props.text, 'duration.text'))}</p>
          <p>{ReactHtmlParser(_.get(this.props.text, 'bulletTitle'))}</p>
          <ul>
          {_.get(this.props.text, 'bullets').map((item, i) => {
            return this.renderItem(item, i)
          })}
          </ul>
          <p>{ReactHtmlParser(_.get(this.props.text, 'text1'))}</p>
          <p>{ReactHtmlParser(_.get(this.props.text, 'text2'))}</p>
          <p>{ReactHtmlParser(_.get(this.props.text, 'text3'))}</p>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(Speaking)