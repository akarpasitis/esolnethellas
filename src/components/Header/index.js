import React, { Component, PropTypes } from 'react'
import Link from 'gatsby-link'
import { navigateTo } from "gatsby-link"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import setLanguage from "../../utils/setLanguage"
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'
import style from './style.css'
import greek from '../../text/greek'
import spanish from '../../text/spanish'
import english from '../../text/english'
import logo from '../../assets/LogoWhite.png'
import closeIcon from '../../assets/icons/PNG/close.png'
import infoIcon from '../../assets/icons/PNG/info.png'
import burgerIcon from '../../assets/icons/PNG/burger.png'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import Fade from 'react-reveal/Fade'
import facebookIcon from '../../assets/icons/PNG/facebook.png'
import youtubeIcon from '../../assets/icons/PNG/youtube.png'
import facebookBlackIcon from '../../assets/icons/PNG/facebookblack.png'
import youtubeBlackIcon from '../../assets/icons/PNG/youtubeblack.png'
import _ from 'lodash'

class Header extends Component {
  constructor() {
    super();
    this.state = {
        makeSticky: false,
        isLrnOpen: false,
        isMaterialOpen: false,
        isBurgerOn: false,
        isMobile: false,
        lrnBgButtonClicked: false,
        materialBgButtonClicked: false,
        lrnMenuOn: false,
        materialMenuOn: false,
        language: 'SPANISH',
        languageLabel: 'español'
    };
    this.toggle = this.toggle.bind(this);
    this.toggleBurger = this.toggleBurger.bind(this);
    this.close = this.close.bind(this);
    this.hideMenu = this.hideMenu.bind(this)
  }

  componentDidMount() {
    if (screen.width < 650) {
      this.setState({ isMobile: true })
    } else {
      this.setState({ isMobile: false })
    }
    document.addEventListener('scroll', this.handleScroll)
    window.addEventListener('resize', this.updateDimensions.bind(this))
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleScroll)
    window.removeEventListener('resize', this.updateDimensions.bind(this))
  }

  updateDimensions() {
    if (screen.width < 650) {
      this.setState({ isMobile: true })
    } else {
      this.setState({ isMobile: false })
    }
  }

  toggle = (type) => {
    if (this.state[type]) {
      this.setState({ [type]: true })
    } else {
      this.setState({ [type]: !this.state.isLrnOpen });
    }
  }

  close = (type) => {
    this.setState({ [type]: false })
  }

  handleDropdown = (type) => {
    this.setState({ [type]: true })
  }

  handleScroll = () => {
    const makeSticky = window.scrollY >= 160
    this.setState({ makeSticky })
  }

  toggleBurger = (isBurgerOn) => {
    if (!isBurgerOn) {
      return
    }
    this.setState({ isBurgerOn: !this.state.isBurgerOn })
  }

  changeLanguage = (e, text) => {
    this.props.changeLanguage(e.value)
    let label
    switch (e.value) {
      case 'SPANISH':
        label = 'español'
        break;
      case 'GREEK':
        label = 'Ελληνικά'
        break;
      case 'ENGLISH':
        label = 'English'
        break;
      default:
        label = 'español'
        break;
    }
    this.setState({ language: e.value, languageLabel: label })
  }

  toggleBurgerDropdowns = (button) => {
    switch (button) {
      case 'lrn':
        this.setState({
          lrnBgButtonClicked: !this.state.lrnBgButtonClicked,
          materialBgButtonClicked: false
        })
        return
      case 'material':
        this.setState({
          materialBgButtonClicked: !this.state.materialBgButtonClicked,
          lrnBgButtonClicked: false
        })
        return
      default:
        break;
    }
    this.setState({ lrnBgButtonClicked: !this.state.lrnBgButtonClicked })
  }

  getBurgerLinks = (text) => {
    return (
      <div className='burgerbuttoncontainer'>
        <div className='dropdownlinkburger' onClick={() => this.toggleBurgerDropdowns('lrn')}>
          {ReactHtmlParser(text.buttons.lrn)}
        </div>
        {this.state.lrnBgButtonClicked &&
          (<div className='lrnsublinksburger'>
            <Link to={{'pathname': '/lrnb1/', 'query': 'b1'}} onClick={() => this.toggleBurger(true)}><div className='dropdownsublinkslrn'>{text.lrnButtons[0]}</div></Link>
            <Link to={{'pathname': '/lrnb2/', 'query': 'b2'}} onClick={() => this.toggleBurger(true)}><div className='dropdownsublinkslrn'>{text.lrnButtons[1]}</div></Link>
            <Link to={{'pathname': '/lrnc1/', 'query': 'c1'}} onClick={() => this.toggleBurger(true)}><div className='dropdownsublinkslrn'>{text.lrnButtons[2]}</div></Link>
            <Link to={{'pathname': '/lrnc2/', 'query': 'c2'}} onClick={() => this.toggleBurger(true)}><div className='dropdownsublinkslrn'>{text.lrnButtons[3]}</div></Link>
          </div>)}
        <div className='dropdownlinkburger' onClick={() => this.toggleBurgerDropdowns('material')}>
          {ReactHtmlParser(text.buttons.material)}
        </div>
        {this.state.materialBgButtonClicked &&
          (<div className='lrnsublinksburger'>
            <Link to={{'pathname': '/material/', 'query': 'pastpapers'}} onClick={() => this.toggleBurger(true)}><div className='dropdownsublinkslrn'>{text.materialButtons[0]}</div></Link>
            <Link to={{'pathname': '/material/', 'query': 'mockvideos'}} onClick={() => this.toggleBurger(true)}><div className='dropdownsublinkslrn'>{text.materialButtons[1]}</div></Link>
          </div>)}
        <Link to='/news/' onClick={() => this.toggleBurger(true)}>
          <div className='dropdownlinkburger'>
            {ReactHtmlParser(text.buttons.news)}
          </div>
        </Link>
        <Link to='/faq/' onClick={() => this.toggleBurger(true)}>
          <div className='dropdownlinkburger'>
            {ReactHtmlParser(text.linkFaq)}
          </div>
        </Link>
        <Link to='/company/' onClick={() => this.toggleBurger(true)}>
          <div className='dropdownlinkburger'>
            {ReactHtmlParser(text.linkCompany)}
          </div>
        </Link>
        <Link to='/contact/' onClick={() => this.toggleBurger(true)}>
          <div className='dropdownlinkburger'>
            {ReactHtmlParser(text.buttons.contact)}
          </div>
        </Link>
      </div>
    )
  }

  getTop = (text) => {
    const options = [
      { value: 'SPANISH', label: text.languages[1]},
      { value: 'GREEK', label: text.languages[0]},
      { value: 'ENGLISH', label: text.languages[2]}      
    ]

    const defaultOption = {
      value: this.state.language,
      label: this.state.languageLabel
    } 

    return (
      <div className='top'>
        <div className='headertopleftside'>
          <span className='headerlanguagelabel'>{text.language}{' '}</span>
          <Dropdown
            className='dropdownclassstyle'
            placeholderClassName='dropdownplaceholderstyle'
            menuClassName='dropdownmenustyle'
            options={options}
            onChange={(e) => this.changeLanguage(e, text)}
            value={defaultOption}
            placeholder={defaultOption} />
          {!this.state.isMobile && ('|')}
          <div className='links'><Link className='headeremaillink' to='/faq/'>{text.linkFaq}</Link></div>
        </div>
        <div className='headertopright'>
          <div className='contact'>
            <div className='headerphones'> {ReactHtmlParser(text.emailLabel) + ' ' + ReactHtmlParser(text.phone)}</div>
            <a className='headeremaillink' href="mailto:info@esolnetespana.es">{ReactHtmlParser(text.emailAddress)}</a>
          </div>
          <div className='headersociallinks'>
            <a className='headersociallink' href='https://www.facebook.com/Esolnet-Espa%C3%B1a-2156276291269633/' target='_blank'><img className="headersociallogoblack" src={facebookBlackIcon}/></a><br/>
            <a className='headersociallink' href='https://www.youtube.com/channel/UCjCwAVbDlgUusWt-wulqVbA' target='_blank'><img className="headersociallogoblack" src={youtubeBlackIcon}/></a>
          </div>
        </div>
      </div>
    )
  }

  getMiddle = (text, isMenu) => {
    return (
      <div className='middle'>
        <div className='left'>
          {this.state.isMobile && (<img className='burgericon' src={isMenu ? closeIcon : burgerIcon} onClick={() => this.toggleBurger(true)}/>)}
          <Link to='/'><img src={logo} className='logobig' style={{ marginBottom: '0' }}/></Link>
        </div>
        {!this.state.isMobile && (
          <div className='right'>
           <img src={infoIcon} className='icon'/>
           <Link className='headeremaillink' to='/registration/'>{ReactHtmlParser(text.registrationInfo)}</Link>
         </div>
        )}
        {this.state.isMobile && (<Link to='/' onClick={() => this.toggleBurger(this.state.isBurgerOn)}><img src={logo} style={{ width: '100px', margin: '0' }}/></Link>)}
      </div>
    )
  }

  showMenu = (type) => {
    const name = `${type}MenuOn`
    this.setState({ [name]: true })
  }

  hideMenu = (type, origin) => {
    const name = `${type}MenuOn`
    this.setState({ [name]: false })
  }

  redirect = (destination) => {
    navigateTo(destination)
  }

  getNavBar = (text, stickyClass) => {
    const lrnDropdownActiveColor = this.state.isLrnOpen ? '#95BD52' : 'transparent'
    const materialDropdownActiveColor = this.state.isMaterialOpen ? '#95BD52' : 'transparent'

    const dropdownlrnDisplay = this.state.lrnMenuOn ? 'flex': 'none'
    const dropdownmaterialDisplay = this.state.materialMenuOn ? 'flex': 'none'    

    return (
    <div className={stickyClass.bottom}>
        <ul className='navLinks'>
          <li>
            <div onMouseEnter={() => this.showMenu('lrn')} onMouseLeave={() => this.hideMenu('lrn')}>
              <div className='dropdownlink'>
                <div className='headerdropdownbuttonlink' onClick={() => this.redirect('/lrnexams/')}>
                  {ReactHtmlParser(text.buttons.lrn)}
                </div>
                <div style={{ display: dropdownlrnDisplay }} className='dropdowncontentlrn' onMouseEnter={() => this.showMenu('lrn')} onMouseLeave={() => this.hideMenu('lrn')}>
                  <Link to={{'pathname': '/lrnb1/', 'query': 'b1'}} onClick={() => this.hideMenu('lrn')}><div className='dropdownsublinkslrn'>{text.lrnButtons[0]}</div></Link>
                  <Link to={{'pathname': '/lrnb2/', 'query': 'b2'}} onClick={() => this.hideMenu('lrn')}><div className='dropdownsublinkslrn'>{text.lrnButtons[1]}</div></Link>
                  <Link to={{'pathname': '/lrnc1/', 'query': 'c1'}} onClick={() => this.hideMenu('lrn')}><div className='dropdownsublinkslrn'>{text.lrnButtons[2]}</div></Link>
                  <Link to={{'pathname': '/lrnc2/', 'query': 'c2'}} onClick={() => this.hideMenu('lrn')}><div className='dropdownsublinkslrn'>{text.lrnButtons[3]}</div></Link>        
                </div>    
              </div>
            </div>
          </li>
          <li>
            <div onMouseEnter={() => this.showMenu('material')} onMouseLeave={() => this.hideMenu('material')}>
              <div className='dropdownlink'>
                <div className='headerdropdownbuttonlink' onClick={() => this.redirect('/material/')}>
                    {ReactHtmlParser(text.buttons.material)}
                </div>
                <div style={{ display: dropdownmaterialDisplay }} className='dropdowncontentmaterial' onMouseEnter={() => this.showMenu('material')} onMouseLeave={() => this.hideMenu('material')}>
                  <Link to={{'pathname': '/material/', 'query': 'pastpapers'}} onClick={() => this.hideMenu('material')}><div className='dropdownsublinksmaterial'>{text.materialButtons[0]}</div></Link>
                  <Link to={{'pathname': '/material/', 'query': 'mockvideos'}} onClick={() => this.hideMenu('material')}><div className='dropdownsublinksmaterial'>{text.materialButtons[1]}</div></Link>
                </div>
              </div>
            </div>
          </li>
          <li>
            <Link to='/news/'>
              <div className='dropdownlink'>
                <div className='headerdropdownbuttonlink'>
                  {ReactHtmlParser(text.buttons.news)}
                </div>
              </div>
            </Link>
          </li>
          <li>
            <Link to='/contact/'>
              <div className='dropdownlink'>
                <div className='headerdropdownbuttonlink'>
                  {ReactHtmlParser(text.buttons.contact)}
                </div>
              </div>
            </Link>
          </li>
          <li>
            <Link to='/company/'>
              <div className='dropdownlink'>
                <div className='headerdropdownbuttonlink'>
                  {ReactHtmlParser(text.linkCompany)}
                </div>
              </div>
            </Link>
          </li>
        </ul>
      </div>
    )
  }

  render() {
    const language = setLanguage(this.state.language)
    const text = language.header

    const containerClass = 'container'
    const bottomClass = 'bottom'  

    const stickyClass = {
      'container': this.state.makeSticky ? 'containerSticky' : 'container',
      'bottom': this.state.makeSticky ? 'bottomSticky' : 'bottom'
    }



    return (
      <div className={this.state.isMobile ? 'container ' : stickyClass.container}>
        {this.state.isBurgerOn && (
          <Fade left duration={300}>
            <div className='burgercontainer'>
              {this.getMiddle(text, true)}
              {this.getBurgerLinks(text)}
            </div>
          </Fade>
        )}
        {this.getTop(text)}
        {this.getMiddle(text, false)}
        {this.getNavBar(text,stickyClass)}
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changeLanguage: (language) => dispatch({ type: language })
  }
}

export default withRouter(connect(null, mapDispatchToProps)(Header))