import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../../utils/setLanguage"
import Link from 'gatsby-link'
import './style.css'
import b1icon from '../../assets/b1.jpg'
import b2icon from '../../assets/b2.jpg'
import c1icon from '../../assets/c1.jpg'
import c2icon from '../../assets/c2.jpg'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
import _ from 'lodash'

import greek from '../../text/greek'
import spanish from '../../text/spanish'
import english from '../../text/english'

class LrnExam extends Component {
  constructor() {
    super()
    this.state = {}
  }

  renderTableRow = (item, i) => {
    const color = i === 0 ? '#2F75A1' : ((i === 1 || i ===3) ? 'white' : 'rgba(250, 219, 109, 0.4)')
    const fontStyle = i === 0 ? 'bold' : ((i === 1 || i ===3) ? 'regular' : 'regular')
    return (
      <div key={`${i}trx`} className='tablerow'>
        <div key={`${i}tra`} style={{ backgroundColor: color, width: '20%', fontWeight: fontStyle }}>{ReactHtmlParser(item[0])}</div>
        <div key={`${i}trb`} style={{ backgroundColor: color, width: '40%', fontWeight: fontStyle }}>{ReactHtmlParser(item[1])}</div>
        <div key={`${i}trc`} style={{ backgroundColor: color, width: '40%', fontWeight: fontStyle }}>{ReactHtmlParser(item[2])}</div>
      </div>
    )
  }

  render() {
    const language = setLanguage(this.props.state)
    const text = language.lrnExams

    let image, exam
    if (this.props.exam) {
      switch (this.props.exam) {
        case 'b1':
          exam = 0
          image = b1icon
          break;
        case 'b2':  
          exam = 1            
          image = b2icon
          break;
        case 'c1':  
          exam = 2           
          image = c1icon
          break;
        case 'c2':
          exam = 3          
          image = c2icon  
          break; 
        default:
          exam = 0
          image = b1icon
          break;
      }
    } 

    const table = _.get(text.exams[exam],'table')

    return (
      <div className='lrndescription'>
        <div className='lrndescriptionleft'>
          <h2>{ReactHtmlParser(_.get(text.exams[exam], 'name'))}</h2>
          <div className='line'/>
          <h2>{ReactHtmlParser(_.get(text.exams[exam], 'price'))}</h2>
          <p>{ReactHtmlParser(_.get(text.exams[exam], 'text'))}</p>  
          <p><b>{ReactHtmlParser(_.get(text.exams[exam], 'duration.title'))}</b> {ReactHtmlParser(_.get(text.exams[exam], 'duration.text'))}</p> 
          <p>{ReactHtmlParser(_.get(text.exams[exam], 'tableTitle'))}</p>
          <div className='tablecontainer'>
            {table.rows.map((item, i) => {
              return this.renderTableRow(item, i)
            })}
          </div>                
        </div>
        <div className='lrndescriptionright'>
          <img className='lrnimage' src={image}/>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(LrnExam)