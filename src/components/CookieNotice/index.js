import React, { Component } from "react"
import { connect } from "react-redux"
import setLanguage from "../../utils/setLanguage"
import _ from 'lodash'
import ReactGA from 'react-ga';

import './style.css';

class CookieNotice extends Component {
  acceptCookies(e, setCookies) {
    e.preventDefault()
    ReactGA.initialize('UA-116321660-1', {
      trackingId: "UA-116321660-1",
      // Puts tracking script in the head instead of the body
      head: false,
      // Setting this parameter is optional
      anonymize: true,
      // Setting this parameter is also optional
      respectDNT: true,
    });
    ReactGA.pageview('/');
    window.localStorage.setItem( 'cookiesAccepted', true );
    setCookies()
  }

  render() {
    const { cookiesAccepted, setCookies } = this.props;
    const language = setLanguage(this.props.state)
    const textCookies = language.cookieNotice.text
    const linkCookies = language.cookieNotice.link
    const close = language.cookieNotice.close

    return (
      <div className='cookiecontainer' style={cookiesAccepted ? { display: 'none'} : { display: 'flex'}}>
        {textCookies}&nbsp;<a className="cookielink" href="https://www.termsfeed.com/cookies-policy/77fc54670be114dc46255ae9309c2b56" target="_blank">{linkCookies}</a>
        <button className='closecookie' type="button" onClick={e => this.acceptCookies(e, setCookies)}>{close}</button>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { state}
}

export default connect(mapStateToProps)(CookieNotice)