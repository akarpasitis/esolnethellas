export default function changeLanguage(state, action) {
  switch (action.type) {
    case 'ENGLISH':
      return 'ENGLISH'
    case 'SPANISH':
      return 'SPANISH'
    case 'GREEK':
      return 'GREEK'
    default:
      return 'SPANISH'
    }
}